package com.wolfpire.system.test;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wolfpire.system.model.Authority;
import com.wolfpire.system.service.AuthorityService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:spring-*.xml"})
public class TestService {
	
	@Resource private AuthorityService authorityService;
	
	@Test
	public void testSave() {
//		Authority a = new Authority();
//		this.authorityService.saveOrUpdate(a);
//		this.authorityService.get(1L);
		List<Authority> as = this.authorityService.findByUserId(1L);
		for (Authority a : as) {
			System.out.println(ToStringBuilder.reflectionToString(a));;
		}
	}
	
	@Test
	public void testBuildTree() throws JsonProcessingException {
		long startTime = System.currentTimeMillis();
		Authority authority = new Authority();
		List<Authority> authoritys = this.authorityService.listTree(authority);
		ObjectMapper mapper = new ObjectMapper();
		System.out.println(mapper.writeValueAsString(authoritys));
		long endTime = System.currentTimeMillis();
		System.out.println(endTime - startTime);
	}

}
