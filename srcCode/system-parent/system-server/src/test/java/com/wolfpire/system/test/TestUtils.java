package com.wolfpire.system.test;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Test;

import com.wolfpire.system.common.Constants;
import com.wolfpire.system.common.excel.ExcelHelper;
import com.wolfpire.system.common.excel.JxlExcelHelper;
import com.wolfpire.system.common.util.BeanUtils;
import com.wolfpire.system.model.User;

public class TestUtils {
	
	@Test
	public void join () {
		Long[] ids = new Long[] {1L, 2L, 3L};
		String idStr = StringUtils.join(ids, ",");
		System.out.println(ToStringBuilder.reflectionToString(idStr, ToStringStyle.SIMPLE_STYLE));
		System.out.println(System.currentTimeMillis()/1000);
	}
	
	@Test
	public void testGlobalUtils() {
		System.out.println(Constants.FILE_ROOT_PATH);
	}
	
	@Test
	public void testMergeUtils() {
		// springMVC
		/*SkillsCompetitionInfo dest = new SkillsCompetitionInfo();
		dest.setId(2L);
		dest.setIndustrySkillFir(1);*/
		User dest = new User();
		dest.setId(2L);
		dest.setNickName("dest");
		dest.setGender(1);
		
		// form database
		/*SkillsCompetitionInfo orig = new SkillsCompetitionInfo();
		orig.setId(2L);
		orig.setIndustrySkillFir(2);
		orig.setIndustrySkillSec(2);
		orig.setCreateTime(new Date());*/
		User orig = new User();
		orig.setId(2L);
		orig.setNickName("orig");
		orig.setGender(0);
		orig.setAccount("orig");
		orig.setCreateTime(new Date());
		orig.setAuthorityIds(new Long[]{1L, 2L, 3L});
		
		try {
			System.out.println(ToStringBuilder.reflectionToString(dest));
			BeanUtils.mergeObject(dest, orig);
			System.out.println(ToStringBuilder.reflectionToString(dest));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testExcelUtilsForBean() {
		List<User> users = new ArrayList<User>();
		User u1 = new User();
		u1.setId(1L);
		u1.setNickName("u1");
		u1.setAccount("account1");
		u1.setAge(18);
		users.add(u1);
		User u2 = new User();
		u2.setId(2L);
		u2.setNickName("u2");
		u2.setAccount("account2");
		u2.setAge(19);
		users.add(u2);
		User u3 = new User();
		u3.setId(3L);
		u3.setNickName("u3");
		u3.setAccount("account3");
		u3.setAge(20);
		users.add(u3);
		
        String[] titles = new String[]{"工号", "姓名", "账号", "年龄", "入职时间"};
        String[] fieldNames = new String[]{"id", "nickName", "account", "age", "createTime"};
        try {
        	String fileName = System.currentTimeMillis() + ".xls"; 
            File file1 = new File("E:\\" + fileName);
            ExcelHelper eh1 = JxlExcelHelper.getInstance(file1);
//            eh1.writeExcel(Employee.class, employees);
            eh1.writeExcel(User.class, users, fieldNames, titles);
            /*List<Employee> list1 = eh1.readExcel(Employee.class, fieldNames,
                    true);
            System.out.println("-----------------JXL2003.xls-----------------");
            for (Employee user : list1) {
                System.out.println(user);
            }
            File file2 = new File("E:\\POI2003.xls");
            ExcelHelper eh2 = HssfExcelHelper.getInstance(file2);
            eh2.writeExcel(Employee.class, employees);
            eh2.writeExcel(Employee.class, employees, fieldNames, titles);
            List<Employee> list2 = eh2.readExcel(Employee.class, fieldNames,
                    true);
            System.out.println("-----------------POI2003.xls-----------------");
            for (Employee user : list2) {
                System.out.println(user);
            }
            File file3 = new File("E:\\POI2007.xlsx");
            ExcelHelper eh3 = XssfExcelHelper.getInstance(file3);
            eh3.writeExcel(Employee.class, employees);
            eh3.writeExcel(Employee.class, employees, fieldNames, titles);
            List<Employee> list3 = eh3.readExcel(Employee.class, fieldNames,
                    true);
            System.out.println("-----------------POI2007.xls-----------------");
            for (Employee user : list3) {
                System.out.println(user);
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	@Test
	public void testExcelUtilsForMap() {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", 1L);
		map.put("name", "u1");
		map.put("account", "account1");
		map.put("age", 18);
		map.put("createTime", new Date());
		list.add(map);
		map.put("id", 2L);
		map.put("name", "u12");
		map.put("account", "account2");
		map.put("age", 19);
		map.put("createTime", new Date());
		list.add(map);
		map.put("id", 3L);
		map.put("name", "u3");
		map.put("account", "account3");
		map.put("age", 20);
		map.put("createTime", new Date());
		list.add(map);
		String[] titles = new String[]{"ID", "姓名", "账号", "年龄", "入职时间"};
        String[] fieldNames = new String[]{"id", "name", "account", "age", "createTime"};
        try {
        	String fileName = System.currentTimeMillis() + ".xls"; 
            File file1 = new File("E:\\" + fileName);
            ExcelHelper eh1 = JxlExcelHelper.getInstance(file1);
            eh1.writeExcel(list, fieldNames, titles);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	@Test
	public void testReflectUtils() {
        /*try {
            Class<User> clazz = User.class;
            User user = ReflectUtil.invokeConstructor(clazz,
                    new Class<?>[]{long.class, String.class, String.class,
                            int.class}, new Object[]{1001L,
                            "u1", "account1", "18"});
            System.out.println(user);
            ReflectUtil.invokeSetter(user, "age", 19);
            System.out.println(user);
            Object ret = ReflectUtil.invokeGetter(user, "age");
            System.out.println(ret);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }
	
}
