package com.wolfpire.system.common.base.service.impl;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.wolfpire.system.common.base.dao.IBaseHibernateDao;
import com.wolfpire.system.common.base.service.IBaseService;

/**
 * 基于hibernate的领域对象业务管理类抽象类
 * @author lihd
 *
 * @param <T>
 * @param <PK>
 */

@Transactional
public abstract class BaseHibernateService<T, PK extends Serializable> implements IBaseService<T, PK> {
	
	protected Logger logger = Logger.getLogger(getClass());
	
	protected abstract IBaseHibernateDao<T, PK> getEntityDao();
	
	@Transactional(readOnly = true)
	@Override
	public T get(PK id) {
		return getEntityDao().get(id);
	}

	@Override
	public PK save(T entity) {
		return getEntityDao().save(entity);
	}

	@Override
	public void update(T entity) {
		getEntityDao().update(entity);
	}

	@Override
	public void saveOrUpdate(T entity) {
		getEntityDao().saveOrUpdate(entity);
	}

	@Override
	public void delete(PK id) {
		getEntityDao().delete(id);
	}

	@Override
	public void delete(T entity) {
		getEntityDao().delete(entity);
	}

	@Override
	public void delete(PK... ids) {
		getEntityDao().delete(ids);
	}
	
	@Transactional(readOnly = true)
	@Override
	public boolean exists(PK id) {
		return getEntityDao().exists(id);
	}
	
	@Transactional(readOnly = true)
	@Override
	public boolean isPropertyUnique(String propertyName, Object propertyValue,
			PK id) {
		return getEntityDao().isPropertyUnique(propertyName, propertyValue, id);
	}

}
