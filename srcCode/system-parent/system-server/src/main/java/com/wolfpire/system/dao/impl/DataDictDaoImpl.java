/**
 * 
 */
package com.wolfpire.system.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wolfpire.system.common.Constants;
import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.base.dao.impl.BaseHibernateDao;
import com.wolfpire.system.dao.DataDictDao;
import com.wolfpire.system.model.DataDict;
import com.wolfpire.system.model.Org;

/**
 * @author lihd
 *
 */
@Repository("dataDictDao")		 
public class DataDictDaoImpl extends BaseHibernateDao<DataDict, Long> implements
		DataDictDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<DataDict> lists(DataDict dataDict) {
		Criteria criteria = this.createCriteria(this.createFilter(dataDict));
		return criteria.list();
	}

	@Override
	public Page<DataDict> findDataDicts(Page<DataDict> page,
			DataDict filterDataDict) {
		Criterion[] criterions = this.createFilter(filterDataDict);
		int totalCount = this.findIntByCriteria(criterions);
		page.setTotalCount(totalCount);
		if (0 < totalCount) {
			Criteria c = createCriteria(criterions);
			c.addOrder(Order.asc("seq"));
			c.addOrder(Order.asc("id"));
			c = setPageParameter(c, page);
			@SuppressWarnings("unchecked")
			List<DataDict> dataDicts = c.list();
			page.setDataList(dataDicts);
		}
		return page;
	}

	@Override
	public boolean isUniqueName(DataDict dataDict) {
		Criteria criteria = null;
		List<Criterion> criterions = new ArrayList<Criterion>();
		criterions.add(Restrictions.eq("name", dataDict.getName()));
		criterions.add(Restrictions.eq("delFlag", Constants.NORMAL_FLAG));
		Long parentId = dataDict.getParentId();
		if (null != dataDict.getParentId()) {
			criterions.add(Restrictions.eq("parentId", parentId));
		}
		Long id = dataDict.getId();
		if (null != id) {
			criterions.add(Restrictions.ne("id", id));
		}
		criteria = createCriteria(criterions.toArray(new Criterion[criterions.size()]));
		@SuppressWarnings("unchecked")
		List<Org> orgs = criteria.list();
		return orgs == null || orgs.size() < 1;
	}
	
	private Criterion[] createFilter(DataDict dataDict) {
		if (null == dataDict) {
			return null;
		}
		List<Criterion> criterions = new ArrayList<Criterion>();
		if (null != dataDict.getId()) {
			criterions.add(Restrictions.eq("id", dataDict.getId()));
		}
		if (null != dataDict.getParentId()) {
			criterions.add(Restrictions.eq("parentId", dataDict.getParentId()));
		} else {
			criterions.add(Restrictions.isNull("parentId"));
		}
		if (!StringUtils.isBlank(dataDict.getName())) {
			criterions.add(Restrictions.like("name", dataDict.getName(), MatchMode.ANYWHERE));
		}
		if (!StringUtils.isBlank(dataDict.getCode())) {
//			criterions.add(Restrictions.like("code", dataDict.getCode(), MatchMode.START));
			criterions.add(Restrictions.eq("code", dataDict.getCode()));
		}
		if (null != dataDict.getDelFlag()) {
			criterions.add(Restrictions.eq("delFlag", dataDict.getDelFlag()));
		}
		return criterions.toArray(new Criterion[criterions.size()]);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DataDict> findChildrenByCode(String code) {
		StringBuffer querySql = new StringBuffer();
		querySql.append(" select * from t_sm_data_dict where parent_id = (")
		.append(" select id from t_sm_data_dict where code = ? ")
		.append(") ");
		SQLQuery query = (SQLQuery) this.createSqlQuery(querySql.toString(), new Object[]{code});
		query.addEntity(DataDict.class);
		return query.list();
	}

	@Override
	public DataDict findUniqueByCode(String code) {
		Criteria criteria = createCriteria(Restrictions.eq("code", code));
		return (DataDict) criteria.uniqueResult();
	}
}
