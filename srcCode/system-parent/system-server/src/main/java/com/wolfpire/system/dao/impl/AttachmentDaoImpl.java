/**
 * 
 */
package com.wolfpire.system.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.base.dao.impl.BaseHibernateDao;
import com.wolfpire.system.dao.AttachmentDao;
import com.wolfpire.system.model.Attachment;

/**
 * @author lihd
 *
 */

@Repository("attachmentDao")
public class AttachmentDaoImpl extends BaseHibernateDao<Attachment, Long>
		implements AttachmentDao {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Attachment> getByBiz(Long bizId, String attaType) {
		Criteria c = createCriteria(Restrictions.eq("bizId", bizId),
				Restrictions.eq("attaType", attaType));
		return c.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Attachment> getById(Long[] attId) {
		Criteria c = createCriteria(Restrictions.in("attId", attId));
		return c.list();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Page<Map<String, Object>> findTaskDetails(Page<Map<String, Object>> page,
			Attachment filterAttachment, Long taskId, List<Long> feedBackIds) {
		ArrayList<String> propertyNames = new ArrayList<String>();
		ArrayList propertyValues = new ArrayList();
		propertyNames.add("taskId");
		propertyValues.add(taskId);
		propertyNames.add("taskType");
		propertyValues.add("task");
		StringBuffer querySqlBuffer = new StringBuffer();
		StringBuffer countSqlBuffer = new StringBuffer();
		querySqlBuffer.append("SELECT a.att_id, a.atta_type, a.biz_id, a.file_name, a.file_path, a.title, ")
			.append(" DATE_FORMAT( a.create_time, '%Y-%m-%d %H:%i:%s' ) as create_time ")
			.append("FROM t_sm_attachment a ")
			.append(" WHERE ")
			.append(" (a.biz_id=:taskId and a.atta_type=:taskType) ");
		if (!CollectionUtils.isEmpty(feedBackIds)) {
			querySqlBuffer.append(" OR ")
				.append(" ( a.biz_id IN (:feedBackIds) and a.atta_type like :feedBackType) ");
			propertyNames.add("feedBackIds");
			propertyValues.add(feedBackIds);
			propertyNames.add("feedBackType");
			propertyValues.add("feedBack%");
		}
		countSqlBuffer.append(" SELECT count(1) from (").append(querySqlBuffer).append(") AS TOTAL ");
		int totalCount = this.findSqlInt(countSqlBuffer.toString(), propertyNames, propertyValues);
		page.setTotalCount(totalCount);
		querySqlBuffer.append(" ORDER BY a.att_id ");
		if (0 < totalCount) {
			List<Map<String, Object>> list = this.setPageParameter(page, querySqlBuffer.toString(), propertyNames, propertyValues);
			page.setDataList(list);
		}
		return page;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Page<Map<String, Object>> findAttachmentsByPiId(Page<Map<String, Object>> page, Long projectIndexId, Attachment filterAttachment) {
		ArrayList<String> propertyNames = new ArrayList<String>();
		ArrayList propertyValues = new ArrayList();
		StringBuffer querySqlBuffer = new StringBuffer();
		StringBuffer countSqlBuffer = new StringBuffer();
		querySqlBuffer.append("SELECT a.att_id, a.atta_type, a.biz_id, a.file_name, a.file_path, a.title, ")
			.append(" IFNULL(t.id, f1.id) as task_id, IFNULL(t.name, f1.name) as task_name, IFNULL(t.remark, f1.remark) as task_remark, ")
			.append(" DATE_FORMAT( a.create_time, '%Y-%m-%d %H:%i:%s' ) as create_time ")
			.append("FROM t_sm_attachment a ")
			.append(" LEFT JOIN t_task t on t.id = a.biz_id AND a.atta_type = 'task' ")
			.append(" LEFT JOIN ( ")
			.append(" SELECT t1.id , t1.name, t1.remark, f.id as f_id FROM t_task t1, t_feedback f WHERE t1.id = f.task_id) f1 ")
			.append(" ON f1.f_id = a.biz_id AND a.atta_type IN ('feedBack_funds', 'feedBack_result') ")
			.append(" LEFT JOIN t_project_index pi ON pi.id = a.biz_id AND a.atta_type in('projectIndex_funds', 'projectIndex_result') ")
			.append(" WHERE 1=1");
		if (null != filterAttachment) {
			if (!StringUtils.isBlank(filterAttachment.getFileName())) {
				querySqlBuffer.append(" AND a.file_name LIKE :fileName ");
				propertyNames.add("fileName");
				propertyValues.add("%" + filterAttachment.getFileName() + "%");
			}
		}
		if (null == projectIndexId) {
			querySqlBuffer.append(" AND a.atta_type in (:atta_type) and a.biz_id is not null ");
			propertyNames.add("atta_type");
			List<String> types = new ArrayList<String>();
			if(null != filterAttachment && filterAttachment.getAttaType()!=null &&filterAttachment.getAttaType().trim().length()>0){
				types.add(filterAttachment.getAttaType().trim());
			}else{
				types.add("task");
				types.add("feedBack_funds");
				types.add("feedBack_result");
				types.add("projectIndex_funds");
				types.add("projectIndex_result");
			}
			propertyValues.add(types);
		} else {
			querySqlBuffer.append(" AND (")
							.append("(a.atta_type='task' AND a.biz_id IN ( ")
							.append(" SELECT t.id FROM t_task t WHERE t.project_index_id in ( ")
							.append(" SELECT id FROM t_project_index WHERE FIND_IN_SET(id, queryChildrenProjectIndex(:projectIndexId)) ")
							.append(" ) ")
							.append(" )) ")
							.append(" OR ")
							.append(" ( a.atta_type LIKE 'feedBack%' AND a.biz_id IN ( ")
							.append(" SELECT fb.id FROM t_feedback fb WHERE fb.task_id in ( ")
							.append(" SELECT t.id FROM t_task t WHERE t.project_index_id in ( ")
							.append(" SELECT id FROM t_project_index WHERE FIND_IN_SET(id, queryChildrenProjectIndex(:projectIndexId_)) ")
							.append(" ) ")
							.append(" ) ")
							.append(" )) ")
							.append(" OR (")
							.append(" a.atta_type LIKE 'projectIndex%' AND a.biz_id IN(")
							.append(" SELECT id FROM t_project_index WHERE FIND_IN_SET(id, queryChildrenProjectIndex(:projectIndexId3)) ")
							.append(") ")
							.append(")")
							.append(" ) ");
			propertyNames.add("projectIndexId");
			propertyValues.add(projectIndexId);
			propertyNames.add("projectIndexId_");
			propertyValues.add(projectIndexId);
			propertyNames.add("projectIndexId3");
			propertyValues.add(projectIndexId);
		}
		
		countSqlBuffer.append(" SELECT count(1) from (").append(querySqlBuffer).append(") AS TOTAL ");
		int totalCount = this.findSqlInt(countSqlBuffer.toString(), propertyNames, propertyValues);
		page.setTotalCount(totalCount);
		querySqlBuffer.append(" ORDER BY a.att_id ");
		if (0 < totalCount) {
			List<Map<String, Object>> list = this.setPageParameter(page, querySqlBuffer.toString(), propertyNames, propertyValues);
			page.setDataList(list);
		}
		return page;
	}

}

