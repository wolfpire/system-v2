/**
 * 
 */
package com.wolfpire.system.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.base.dao.IBaseHibernateDao;
import com.wolfpire.system.common.base.service.impl.BaseHibernateService;
import com.wolfpire.system.dao.AttachmentDao;
import com.wolfpire.system.model.Attachment;
import com.wolfpire.system.service.AttachmentService;

/**
 * @author lihd
 *
 */

@Service("attachmentService")
public class AttachmentServiceImpl extends
		BaseHibernateService<Attachment, Long> implements AttachmentService {
	
	@Resource private AttachmentDao attachmentDao;
	
	@Override
	public List<Attachment> list(Attachment entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Attachment> getByBiz(Long bizId, String attaType) {
		return this.attachmentDao.getByBiz(bizId, attaType);
	}

	@Override
	public List<Attachment> getById(Long[] attId) {
		return this.attachmentDao.getById(attId);
	}

	@Override
	protected IBaseHibernateDao<Attachment, Long> getEntityDao() {
		return this.attachmentDao;
	}

	@Override
	public void del(Long attId) {
		// TODO Auto-generated method stub
		Attachment attachment = this.attachmentDao.get(attId);
		if (null != attachment) {
			attachment.setBizId(0L);
			attachment.setAttaType(StringUtils.EMPTY);
			this.attachmentDao.save(attachment);
		}
	}

	@Override
	public void batchRelateAttachment(Long[] attId, Long bizId, String attaType) {
		if (ArrayUtils.isNotEmpty(attId) && null != bizId) {
			for (Long id : attId) {
				Attachment attament = this.attachmentDao.get(id);
				attament.setBizId(bizId);
				attament.setAttaType(attaType);
				this.attachmentDao.saveOrUpdate(attament);
			}
		}
	}

	@Override
	public Page<Map<String, Object>> setAttachmentsByPiId(
			Page<Map<String, Object>> page, Long projectIndexId, Attachment filterAttachment) {
		return this.attachmentDao.findAttachmentsByPiId(page, projectIndexId, filterAttachment);
	}

}
