package com.wolfpire.system.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wolfpire.system.common.Constants;
import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.base.dao.impl.BaseHibernateDao;
import com.wolfpire.system.dao.RoleDao;
import com.wolfpire.system.model.Role;

@Repository("roleDao")
public class RoleDaoImpl extends BaseHibernateDao<Role, Long> implements RoleDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Role> list(Role role) {
		Criteria c = createCriteria(createFilter(role));
		return c.list();
	}

	@Override
	public Page<Role> findRoles(Page<Role> page, Role filterRole) {
		Criterion[] criterions = createFilter(filterRole);
		int totalCount = this.findIntByCriteria(criterions);
		page.setTotalCount(totalCount);
		
		if (0 < totalCount) {
			Criteria c = createCriteria(criterions);
			c = this.setPageParameter(c, page);
			@SuppressWarnings({ "unchecked" })
			List<Role> result = c.list();
			page.setDataList(result);
		}
		return page;
	}
	
	private Criterion[] createFilter(Role role) {
		List<Criterion> criterions = new ArrayList<Criterion>();
		if (null == role) {
			criterions.add(Restrictions.eq("delFlag", Constants.NORMAL_FLAG));
			return criterions.toArray(new Criterion[criterions.size()]);
		}
		if (null != role.getId()) {
			criterions.add(Restrictions.eq("id", role.getId()));
		}
		if (!StringUtils.isBlank(role.getName())) {
			criterions.add(Restrictions.like("name", role.getName(), MatchMode.ANYWHERE));
		}
		if (null != role.getDelFlag()) {
			criterions.add(Restrictions.eq("delFlag", role.getDelFlag()));
		}
		return criterions.toArray(new Criterion[criterions.size()]);
	}

	@SuppressWarnings("unchecked")
	@Override
	public String findRoleNames(List<Long> rolesId) {
		if (CollectionUtils.isEmpty(rolesId)) {
			return StringUtils.EMPTY;
		}
		String querySql = "SELECT GROUP_CONCAT(name) FROM t_sm_role WHERE id in (:roleIds) and del_flag = 1";
		List<String> propertyNames = new ArrayList<String>();
		propertyNames.add("roleIds");
		@SuppressWarnings("rawtypes")
		List propertyValues = new ArrayList();
		propertyValues.add(rolesId);
		SQLQuery query = this.createSqlQuery(querySql, propertyNames , propertyValues);
		Object obj = query.uniqueResult();
		if (null != obj) {
			return obj.toString();
		}
		return StringUtils.EMPTY;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public String getUserIdsByRoleId(Long roleId) {
		if (null == roleId) {
			return StringUtils.EMPTY;
		}
		StringBuffer querySql = new StringBuffer();
		querySql.append(" SELECT GROUP_CONCAT(u.id) FROM t_sm_role r, t_sm_user_role ur, t_sm_user u ")
				.append(" WHERE r.id = ur.role_id AND ur.user_id = u.id ")
				.append(" AND u.del_flag = 1 AND u.del_flag = 1 ")
				.append(" AND r.id =:roleId ");
		List<String> propertyNames = new ArrayList<String>();
		propertyNames.add("roleId");
		List propertyValues = new ArrayList();
		propertyValues.add(roleId);
		SQLQuery query = this.createSqlQuery(querySql.toString(), propertyNames , propertyValues);
		Object obj = query.uniqueResult();
		if (null != obj) {
			return obj.toString();
		}
		return StringUtils.EMPTY;
	}

}
