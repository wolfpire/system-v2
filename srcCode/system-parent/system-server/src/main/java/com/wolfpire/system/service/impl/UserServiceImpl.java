package com.wolfpire.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.wolfpire.system.common.Constants;
import com.wolfpire.system.common.MD5;
import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.base.dao.IBaseHibernateDao;
import com.wolfpire.system.common.base.service.impl.BaseHibernateService;
import com.wolfpire.system.common.util.StringUtils;
import com.wolfpire.system.dao.AuthorityDao;
import com.wolfpire.system.dao.OrgDao;
import com.wolfpire.system.dao.OrgUserRelationDao;
import com.wolfpire.system.dao.RoleDao;
import com.wolfpire.system.dao.UserDao;
import com.wolfpire.system.dao.UserRoleRelationDao;
import com.wolfpire.system.model.Authority;
import com.wolfpire.system.model.Org;
import com.wolfpire.system.model.OrgUserRelation;
import com.wolfpire.system.model.User;
import com.wolfpire.system.model.UserRoleRelation;
import com.wolfpire.system.service.UserService;

@Service("userService")
public class UserServiceImpl extends BaseHibernateService<User, Long> implements
		UserService {
	
	@Resource private UserDao userDao;
	
	@Resource private UserRoleRelationDao userRoleRelationDao;
	
	@Resource private OrgUserRelationDao orgUserRelationDao;
	
	@Resource private AuthorityDao authorityDao;
	
	@Resource private RoleDao roleDao;
	
	@Resource private OrgDao orgDao;
	
	@Override
	protected IBaseHibernateDao<User, Long> getEntityDao() {
		return this.userDao;
	}

	@Override
	public User getByAccount(String account, String password) {
		return this.userDao.getByAccount(account, password);
	}
	
	public List<User> getUsers() {
		return null;
	}

	@Override
	public List<User> list(User queryUser) {
		return this.userDao.list(queryUser);
	}

	@Override
	public Page<User> setPageDataList(Page<User> page, User filterUser) {
		return this.userDao.findUsers(page, filterUser);
	}

	@Override
	public void delete(Long id) {
		User user = this.userDao.get(id);
		if (null != user) {
			user.setDelFlag(Constants.DELETE_FLAG);
			this.userDao.update(user);
		}
	}

	@Override
	public void saveOrUpdate(User user) {
		if (null == user.getId()) {
			MD5 md5 = new MD5();
			user.setPassword(md5.getMD5ofStr("123456"));
			this.userDao.saveOrUpdate(user);
		} else {
			this.userDao.merge(user);
		} 
		
		Long userId = user.getId();
		Long[] roleIds = user.getRoleIds();
		this.userRoleRelationDao.relate(userId, roleIds);
		Long orgId = user.getOrgId();
		this.orgUserRelationDao.relate(userId, orgId);
	}

	@Override
	public User getCompleteUser(Long id) {
		User user = this.get(id);
		// 绑定角色
		List<UserRoleRelation> urs = this.userRoleRelationDao.getURRelation(id);
		if (!CollectionUtils.isEmpty(urs)) {
			ArrayList<Long> rolesId = new ArrayList<Long>();
			for (UserRoleRelation ur : urs) {
				rolesId.add(ur.getRoleId());
			}
			user.setRoleIds(rolesId.toArray(new Long[rolesId.size()]));
			String roleNames = this.roleDao.findRoleNames(rolesId);
			user.setRoleNamesStr(roleNames);
		}
		// 绑定权限
		List<Authority> authorities = this.authorityDao.findByUserId(user.getId());
		if (!CollectionUtils.isEmpty(authorities)) {
			user.setUserAuthorities(authorities);
			ArrayList<Long> authorityIds = new ArrayList<Long>();
			for (Authority authority : authorities) {
				authorityIds.add(authority.getId());
			}
			user.setAuthorityIds(authorityIds.toArray(new Long[authorityIds.size()]));
		}
		// 绑定组织
		OrgUserRelation ou = this.orgUserRelationDao.getUORelation(id);
		if (null != ou) {
			user.setOrgId(ou.getOrgId());
			Org org = this.orgDao.get(ou.getOrgId());
			if (null != org) {
				user.setOrgName(org.getName());
			}
		}
		return user;
	}

	@Override
	public User getRolesInfoUser(Long id) {
		User user = this.get(id);
		List<UserRoleRelation> urs = this.userRoleRelationDao.getURRelation(id);
		if (CollectionUtils.isEmpty(urs)) {
			return user;
		}
		ArrayList<Long> rolesId = new ArrayList<Long>();
		for (UserRoleRelation ur : urs) {
			rolesId.add(ur.getRoleId());
		}
		user.setRoleIds(rolesId.toArray(new Long[rolesId.size()]));
		return user;
	}

	@Override
	public User getByAccount(String account) {
		return this.userDao.getByAccount(account);
	}

	@Override
	public List<User> getUsersByRoleId(Long roleId) {
		List<User> users = new ArrayList<User>();
		List<UserRoleRelation> rus = this.userRoleRelationDao.getRURelation(roleId);
		if (!CollectionUtils.isEmpty(rus)) {
			for (UserRoleRelation ur : rus) {
				User u = this.userDao.get(ur.getUserId());
				users.add(u);
			}
		}
		return users;
	}

	@Override
	public List<User> getUsersByOrgId(Long orgId) {
		List<User> users = new ArrayList<User>();
		List<OrgUserRelation> ous = this.orgUserRelationDao.getOURelations(orgId);
		if (!CollectionUtils.isEmpty(ous)) {
			for (OrgUserRelation ou : ous) {
				User user = this.userDao.get(ou.getUserId());
				users.add(user);
			}
		}
		return users;
	}

	@Override
	public void saveUserPassword(Long id, String password) {
		MD5 md5 = new MD5();
		String passwordMd5 = md5.getMD5ofStr(password);
		this.userDao.saveUserPassword(id, passwordMd5);
	}

	@Override
	public List<User> getUsersByParam(User user, List<Long> roleIds,
			List<Long> orgIds) {
		return this.userDao.getUsersByParam(user, roleIds, orgIds);
	}

	@Override
	public void grantUserRoles(Long id, String roleIds) {
		List<Long> roleIdList = new ArrayList<Long>();
		if (!StringUtils.isBlank(roleIds)) {
			String[] roleIdsStrArr = roleIds.split(",");
			for (String str : roleIdsStrArr) {
				roleIdList.add(Long.parseLong(str));
			}
		} 
		this.userRoleRelationDao.relate(id, roleIdList);
	}

}
