/**
 * 
 */
package com.wolfpire.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.wolfpire.system.common.Constants;
import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.base.dao.IBaseHibernateDao;
import com.wolfpire.system.common.base.service.impl.BaseHibernateService;
import com.wolfpire.system.common.util.StringUtils;
import com.wolfpire.system.dao.RoleAuthorityRelationDao;
import com.wolfpire.system.dao.RoleDao;
import com.wolfpire.system.dao.UserRoleRelationDao;
import com.wolfpire.system.model.Role;
import com.wolfpire.system.model.RoleAuthorityRelation;
import com.wolfpire.system.service.RoleService;

/**
 * @author lihd
 *
 */

@Service("roleService")
public class RoleServiceImpl extends BaseHibernateService<Role, Long> implements
		RoleService {
	
	@Override
	public List<Role> list(Role role) {
		return this.roleDao.list(role);
	}

	@Override
	public Page<Role> setPageDataList(Page<Role> page, Role filterRole) {
		return this.roleDao.findRoles(page, filterRole);
	}

	@Override
	public void delete(Long id) {
		Role role = this.roleDao.get(id);
		if (null != role) {
			role.setDelFlag(Constants.DELETE_FLAG);
			this.update(role);
		}
	}

	@Override
	public void saveOrUpdate(Role role) {
		super.saveOrUpdate(role);
		Long id = role.getId();
		Long[] authorityIds = role.getAuthorityIds();
		this.roleAuthorityRelationDao.relate(id, authorityIds);
	}
	
	@Override
	public boolean isUnique(Role role) {
		return this.isPropertyUnique("name", role.getName(), role.getId());
	}
	
	@Override
	public Role getAuthorityInfoRole(Long id) {
		Role role = this.get(id);
		List<RoleAuthorityRelation> uas = this.roleAuthorityRelationDao.getRARealation(id);
		if (CollectionUtils.isEmpty(uas)) {
			return role;
		}
		List<Long> authorityIds = new ArrayList<Long>();
		for (RoleAuthorityRelation ua : uas) {
			authorityIds.add(ua.getAuthorityId());
		}
		role.setAuthorityIds(authorityIds.toArray(new Long[authorityIds.size()]));
		return role;
	}
	
	@Override
	public void saveRoleAuthority(Long id, String authorityIdsStr) {
		// TODO Auto-generated method stub
		List<Long> authorityIds = new ArrayList<Long>();
		if (!StringUtils.isBlank(authorityIdsStr)) {
			String[] authorityIdsArr = authorityIdsStr.split(",");
			for (String str : authorityIdsArr) {
				authorityIds.add(Long.parseLong(str));
			}
		}
		this.roleAuthorityRelationDao.relate(id, authorityIds);
	}

	@Override
	public void saveRoleUser(Long id, String userIdsStr) {
		this.userRoleRelationDao.relateRoleUsers(id, userIdsStr);
	}

	@Override
	public String getUserIdsByRoleId(Long roleId) {
		return this.roleDao.getUserIdsByRoleId(roleId);
	}

	@Override
	protected IBaseHibernateDao<Role, Long> getEntityDao() {
		return this.roleDao;
	}
	
	@Resource private RoleDao roleDao;
	
	@Resource private UserRoleRelationDao userRoleRelationDao;
	
	@Resource private RoleAuthorityRelationDao roleAuthorityRelationDao;

}
