/**
 * 
 */
package com.wolfpire.system.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wolfpire.system.common.Constants;
import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.base.dao.impl.BaseHibernateDao;
import com.wolfpire.system.dao.OrgDao;
import com.wolfpire.system.model.Org;

/**
 * @author lihd
 *
 */

@Repository("orgDao")
public class OrgDaoImpl extends BaseHibernateDao<Org, Long> implements OrgDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Org> list(Org org) {
		Criteria criteria = createCriteria(this.createFilter(org, "tree"));
		return criteria.list();
	}

	@Override
	public Page<Org> findOrgs(Page<Org> page, Org filterOrg) {
		Criterion[] criterions = this.createFilter(filterOrg, null);
		int totalCount = this.findIntByCriteria(criterions);
		page.setTotalCount(totalCount);
		if (0 < totalCount) {
			Criteria c = createCriteria(criterions);
			c.addOrder(Order.asc("parentId"));
			c = setPageParameter(c, page);
			@SuppressWarnings("unchecked")
			List<Org> orgs = c.list();
			page.setDataList(orgs);
		}
		return page;
	}
	
	private Criterion[] createFilter(Org filterOrg, String type) {
		if (null == filterOrg) {
			return null;
		}
		List<Criterion> criterions = new ArrayList<Criterion>();
		if (null != filterOrg.getId()) {
			criterions.add(Restrictions.eq("id", filterOrg.getId()));
		}
		if (null != filterOrg.getParentId()) {
			//if(filterOrg.get_parentId() >0)
				criterions.add(Restrictions.eq("parentId", filterOrg.getParentId()));
		} else {
			if ("tree".equals(type)) {
				criterions.add(Restrictions.isNull("parentId"));
			}
		}
		if (!StringUtils.isBlank(filterOrg.getName())) {
			criterions.add(Restrictions.like("name", filterOrg.getName(), MatchMode.ANYWHERE));
		}
		if (!StringUtils.isBlank(filterOrg.getCode())) {
			criterions.add(Restrictions.like("code", filterOrg.getCode(), MatchMode.ANYWHERE));
		}
		if (null != filterOrg.getDelFlag()) {
			criterions.add(Restrictions.eq("delFlag", filterOrg.getDelFlag()));
		}else{
			criterions.add(Restrictions.eq("delFlag", Constants.NORMAL_FLAG));
		}
		return criterions.toArray(new Criterion[criterions.size()]);
	}

	@Override
	public boolean isUniqueName(Org org) {
		// TODO : 待验证
		Criteria criteria = null;
		List<Criterion> criterions = new ArrayList<Criterion>();
		criterions.add(Restrictions.eq("name", org.getName()));
		criterions.add(Restrictions.eq("delFlag", Constants.NORMAL_FLAG));
		Long parentId = org.getParentId();
		if (null != org.getParentId()) {
			criterions.add(Restrictions.eq("parentId", parentId));
		}
		Long id = org.getId();
		if (null != id) {
			criterions.add(Restrictions.ne("id", id));
		}
		criteria = createCriteria(criterions.toArray(new Criterion[criterions.size()]));
		@SuppressWarnings("unchecked")
		List<Org> orgs = criteria.list();
		return orgs == null || orgs.size() < 1;
	}
}
