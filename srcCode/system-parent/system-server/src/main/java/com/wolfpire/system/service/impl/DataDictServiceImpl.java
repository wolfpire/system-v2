/**
 * 
 */
package com.wolfpire.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.wolfpire.system.common.Constants;
import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.Result;
import com.wolfpire.system.common.base.dao.IBaseHibernateDao;
import com.wolfpire.system.common.base.service.impl.BaseHibernateService;
import com.wolfpire.system.common.util.MatchType;
import com.wolfpire.system.dao.DataDictDao;
import com.wolfpire.system.model.DataDict;
import com.wolfpire.system.service.DataDictService;

/**
 * @author lihd
 *
 */

@Service("dataDictService")
public class DataDictServiceImpl extends BaseHibernateService<DataDict, Long>
		implements DataDictService {
	
	@Resource private DataDictDao dataDictDao;

	@Override
	public List<DataDict> list(DataDict dataDict) {
		return this.dataDictDao.lists(dataDict);
	}

	@Override
	public Page<DataDict> setPageDataList(Page<DataDict> page,
			DataDict filterDataDict) {
		return this.dataDictDao.findDataDicts(page, filterDataDict);
	}

	@Override
	public Result isUnique(DataDict newDataDict) {
		Result rs = new Result();
		boolean result_code = this.dataDictDao.isPropertyUnique(new String[] {
				"code", "delFlag", "parentId" },
				new Object[] { newDataDict.getCode(), newDataDict.getDelFlag(), newDataDict.getParentId() },
				new MatchType[] { MatchType.EQ, MatchType.EQ, MatchType.EQ }, newDataDict.getId());
		boolean result_name = this.dataDictDao.isUniqueName(newDataDict);
		rs.setSuccess(result_code && result_name);
		if (!rs.isSuccess()) {
			rs.setMessage("编码或名称重复");
		} else {
			rs.setMessage(StringUtils.EMPTY);
		}
		return rs;
	}

	@Override
	public void del(Long id) {
		DataDict dataDict = this.dataDictDao.get(id);
		if (null != dataDict) {
			dataDict.setDelFlag(Constants.DELETE_FLAG);
//			this.dataDictDao.saveOrUpdate(dataDict);
			this.saveOrUpdate(dataDict);
			/*Long parentId = dataDict.getParentId();
			if (null != parentId) {
				List<DataDict> children = this.findChildren(parentId);
				if (CollectionUtils.isEmpty(children)) {
					DataDict parent = this.dataDictDao.get(parentId);
					parent.setLeaf(true);
					this.dataDictDao.saveOrUpdate(parent);
				}
			}*/
		}
	}

	@Override
	public List<DataDict> findChildren(String code) {
		if (!StringUtils.isBlank(code)) {
			DataDict dataDict = new DataDict();
			dataDict.setCode(code);
			return this.dataDictDao.findChildrenByCode(code);
		}
		return new ArrayList<DataDict>();
	}
	
	@Override
	public List<DataDict> findChildren(Long parentId) {
		DataDict dataDict = new DataDict();
		dataDict.setParentId(parentId);
		return this.dataDictDao.lists(dataDict);
	}
	
	@Override
	protected IBaseHibernateDao<DataDict, Long> getEntityDao() {
		return this.dataDictDao;
	}

	@Override
	public void saveOrUpdate(DataDict dataDict) {
		super.saveOrUpdate(dataDict);
		Long parentId = dataDict.getParentId();
		if (null != parentId) {
			List<DataDict> children = this.findChildren(parentId);
			DataDict parent = this.dataDictDao.get(parentId);
			if (CollectionUtils.isEmpty(children)) {
				parent.setLeaf(true);
				this.dataDictDao.saveOrUpdate(parent);
			} else {
				if (parent.isLeaf()) {
					parent.setLeaf(false);
					this.dataDictDao.saveOrUpdate(parent);
				}
			}
		}
		if (null != dataDict.getId()) {
			List<DataDict> children = this.findChildren(dataDict.getId());
			if (!CollectionUtils.isEmpty(children)) {
				dataDict.setLeaf(false);
				super.saveOrUpdate(dataDict);
			}
		}
	}

	@Override
	public DataDict findUniqueByCode(String code) {
		return this.dataDictDao.findUniqueByCode(code);
	}

}
