/**
 * 
 */
package com.wolfpire.system.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.wolfpire.system.common.Constants;
import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.Result;
import com.wolfpire.system.common.base.dao.IBaseHibernateDao;
import com.wolfpire.system.common.base.service.impl.BaseHibernateService;
import com.wolfpire.system.common.util.MatchType;
import com.wolfpire.system.dao.OrgDao;
import com.wolfpire.system.model.Org;
import com.wolfpire.system.service.OrgService;

/**
 * @author lihd
 *
 */

@Service("orgService")
public class OrgServiceImpl extends BaseHibernateService<Org, Long> implements
		OrgService {
	
	@Resource private OrgDao orgDao;

	@Override
	protected IBaseHibernateDao<Org, Long> getEntityDao() {
		return this.orgDao;
	}
	
	@Override
	public List<Org> list(Org queryOrg) {
		return this.orgDao.list(queryOrg);
	}

	@Override
	public Page<Org> setPageDataList(Page<Org> page, Org filterOrg) {
		return this.orgDao.findOrgs(page, filterOrg);
	}

	@Override
	public Result isUnique(Org newOrg) {
		// TODO Auto-generated method stub
		Result rs = new Result();
		// 1.不能重code,2.同个parentId下不能重名
		boolean result_code = this.orgDao.isPropertyUnique(new String[] {
				"code", "delFlag" },
				new Object[] { newOrg.getCode(), newOrg.getDelFlag() },
				new MatchType[] { MatchType.EQ, MatchType.EQ }, newOrg.getId());
		boolean result_name = this.orgDao.isUniqueName(newOrg);
		rs.setSuccess(result_code && result_name);
		if (!rs.isSuccess()) {
			rs.setMessage("组织编码或名称重复");
		} else {
			rs.setMessage(StringUtils.EMPTY);
		}
		return rs;
	}

	@Override
	public void saveOrUpdate(Org org) {
		Long parentId = org.getParentId();
		if (null != parentId) {
			//判断是否存在父组织，如果存在父组织，将其父组织设置为非叶子节点
			Org parentOrg = this.orgDao.get(parentId);
			parentOrg.setLeaf(false);
			this.orgDao.save(parentOrg);
		}
		if (null != org.getId()) {
			List<Org> children = this.findChildRen(org.getId());
			if (!CollectionUtils.isEmpty(children)) {
				//判断是否存在子节点，如果存在，将其设置为非叶子节点
				org.setLeaf(false);
			}
		}
		super.saveOrUpdate(org);
	}

	@Override
	public List<Org> findChildRen(Long parentId) {
		Org org = new Org();
		org.setParentId(parentId);
		return this.orgDao.list(org);
	}

	@Override
	public void del(Long id) {
		Org org = this.orgDao.get(id);
		if (null != org) {
			org.setDelFlag(Constants.DELETE_FLAG);
			this.orgDao.saveOrUpdate(org);
			Long parentId = org.getParentId();
			if (null != parentId) {
				//判断是否存在父组织
				List<Org> children = this.findChildRen(parentId);
				if (CollectionUtils.isEmpty(children)) {
					//如果父组织不存在子节点
					Org parentOrg = this.orgDao.get(parentId);
					parentOrg.setLeaf(true);
					this.orgDao.saveOrUpdate(parentOrg);
				}
			}
		}
	}
	
	
}
