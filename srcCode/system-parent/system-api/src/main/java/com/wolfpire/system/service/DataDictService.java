/**
 * 
 */
package com.wolfpire.system.service;

import java.util.List;

import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.Result;
import com.wolfpire.system.common.base.service.IBaseHibernateService;
import com.wolfpire.system.model.DataDict;

/**
 * @author lihd
 *
 */
public interface DataDictService extends IBaseHibernateService<DataDict, Long> {

	/**
	 * 分页列表
	 * @param page
	 * @param filterDataDict
	 * @return
	 */
	public Page<DataDict> setPageDataList(Page<DataDict> page, DataDict filterDataDict);
	
	/**
	 * 查询newDataDict是否唯一
	 * @param newDataDict
	 * @return
	 */
	public Result isUnique(DataDict newDataDict);
	
	/**
	 * 伪删
	 * @param id
	 */
	public void del(Long id);
	
	/**
	 * 根据父code查询子集
	 * @param parentId
	 * @return
	 */
	public List<DataDict> findChildren(String code);
	
	/**
	 * 根据父Id查询子集
	 * @param parentId
	 * @return
	 */
	public List<DataDict> findChildren(Long parentId);
	
	/**
	 * 根据编码获取唯一数据字典对象
	 * @param code
	 * @return
	 */
	public DataDict findUniqueByCode(String code);
}
