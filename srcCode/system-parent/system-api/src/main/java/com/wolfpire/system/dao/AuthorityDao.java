/**
 * 
 */
package com.wolfpire.system.dao;

import java.util.List;

import com.wolfpire.system.common.EasyuiPage;
import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.base.dao.IBaseHibernateDao;
import com.wolfpire.system.model.Authority;

/**
 * 权限持久层接口
 * @author lihd
 *
 */
public interface AuthorityDao extends IBaseHibernateDao<Authority, Long> {
	
	/**
	 * 根据filterAuthority过滤查询列表[带分页]
	 * @param page
	 * @param filterAuthority
	 * @return
	 */
	public Page<Authority> findAuthoritys(EasyuiPage<Authority> page, Authority filterAuthority);
	
	/**
	 * 同一级下，判断名称的唯一性
	 * @param authority
	 * @return
	 */
	public boolean isUniqueName(Authority authority);
	
	/**
	 * 根据authority过滤list
	 * @param authority
	 * @return
	 */
	public List<Authority> list(Authority authority);
	
	/**
	 * 通过userId获取权限集合
	 * @param userId
	 * @return
	 */
	public List<Authority> findByUserId(Long userId);
	
	/**
	 * 获取用户Id获取目录和菜单
	 * @param userId
	 * @return
	 */
	public List<Authority> getCatalogAndMenuTypeByUserId(Long userId);
	
	public List<Authority> listTree(Authority authority);

}
