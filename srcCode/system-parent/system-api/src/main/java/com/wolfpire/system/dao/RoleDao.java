/**
 * 
 */
package com.wolfpire.system.dao;

import java.util.List;

import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.base.dao.IBaseHibernateDao;
import com.wolfpire.system.model.Role;

/**
 * 角色持久层接口
 * @author lihd
 *
 */
public interface RoleDao extends IBaseHibernateDao<Role, Long> {
	
	/**
	 * 根据user过滤查询列表
	 * @param user
	 * @return
	 */
	public List<Role> list(Role role);
	
	/**
	 * 根据role过滤查询带分页列表
	 * @param page
	 * @param filterRole
	 * @return
	 */
	public Page<Role> findRoles(Page<Role> page, Role filterRole);
	
	/**
	 * 将roleId集合转换成roleName串
	 * @param roleIds
	 * @return
	 */
	public String findRoleNames(List<Long> rolesId);
	
	/**
	 * 通过角色获取用户Id集合
	 * @param roleId
	 * @return
	 */
	public String getUserIdsByRoleId(Long roleId);

}
