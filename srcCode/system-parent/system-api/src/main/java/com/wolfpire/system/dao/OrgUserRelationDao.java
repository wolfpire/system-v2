/**
 * 
 */
package com.wolfpire.system.dao;

import java.util.List;

import com.wolfpire.system.common.base.dao.IBaseHibernateDao;
import com.wolfpire.system.model.OrgUserRelation;

/**
 * 用户组织关联表持久层接口
 * @author lihd
 *
 */
public interface OrgUserRelationDao extends IBaseHibernateDao<OrgUserRelation, Long> {
	
	/**
	 * 绑定用户和组织之间的关系
	 * @param userId
	 * @param orgId
	 */
	public void relate(Long userId, Long orgId);
	
	/**
	 * 获取用户组织关系
	 * @param userId
	 */
	public OrgUserRelation getUORelation(Long userId);
	
	/**
	 * 获取组织下的用户
	 * @param orgId	组织id
	 * @return
	 */
	public List<OrgUserRelation> getOURelations(Long orgId);

}
