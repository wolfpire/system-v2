/**
 * 
 */
package com.wolfpire.system.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 角色权限模型
 * @author lihd
 *
 */

@Entity
@Table(name = "t_sm_role_authority")
public class RoleAuthorityRelation implements Serializable {
	
	private static final long serialVersionUID = -4475808927098352952L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false, length = 32)
	private Long id;
	
	@Column(name = "role_id", length = 32)
	private Long roleId;
	
	@Column(name = "authority_id", length = 32)
	private Long authorityId;

	public RoleAuthorityRelation() {
		super();
	}

	public RoleAuthorityRelation(Long roleId, Long authorityId) {
		super();
		this.roleId = roleId;
		this.authorityId = authorityId;
	}

	public RoleAuthorityRelation(Long id, Long roleId, Long authorityId) {
		super();
		this.id = id;
		this.roleId = roleId;
		this.authorityId = authorityId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getAuthorityId() {
		return authorityId;
	}

	public void setAuthorityId(Long authorityId) {
		this.authorityId = authorityId;
	}
	
}
