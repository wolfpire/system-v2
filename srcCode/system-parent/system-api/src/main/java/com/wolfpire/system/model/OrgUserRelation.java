/**
 * 
 */
package com.wolfpire.system.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 用户组织关联表
 * @author lihd
 *
 */

@Entity
@Table(name = "t_sm_user_org")
public class OrgUserRelation implements Serializable {
	
	private static final long serialVersionUID = -7980223938501260231L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false, length = 32)
	private Long id;
	
	@Column(name = "user_id", length = 32)
	private Long userId;
	
	@Column(name = "org_id", length = 32)
	private Long orgId;

	public OrgUserRelation() {
		super();
	}

	public OrgUserRelation(Long userId, Long orgId) {
		super();
		this.userId = userId;
		this.orgId = orgId;
	}

	public OrgUserRelation(Long id, Long userId, Long orgId) {
		super();
		this.id = id;
		this.userId = userId;
		this.orgId = orgId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
}
