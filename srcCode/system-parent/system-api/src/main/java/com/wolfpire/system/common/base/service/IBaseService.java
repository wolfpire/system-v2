/**
 * 
 */
package com.wolfpire.system.common.base.service;

import java.io.Serializable;
import java.util.List;

/**
 * 领域对象业务管理类基类接口
 * @author lihd
 * 
 * @param <T>实体类
 * @param <PK>主键类型
 */
public interface IBaseService<T, PK extends Serializable> {
	
	/**
	 * 根据主键查找对象
	 * @param id 主键id
	 * @return
	 */
	public T get(PK id);
	
	/**
	 * 保存
	 * @param entity
	 * @return
	 */
	public PK save(T entity);
	
	/**
	 * 更新
	 * @param entity
	 */
	public void update(T entity);
	
	/**
	 * 保存或更新
	 * @param entity
	 */
	public void saveOrUpdate(T entity);
    
	/**
	 * 删除
	 * @param id
	 */
	public void delete(PK id);
	
	/**
	 * 删除
	 * @param entity
	 */
	public void delete(T entity);
	
	/**
	 * 删除
	 * @param ids
	 */
	public void delete(PK... ids);
	
	/**
	 * 根据主键判断对象是否存在
	 * @param id
	 * @return
	 */
	boolean exists(PK id);
	
	/**
	 * 判断对象的属性值在数据库内是否唯一
	 * @param propertyName
	 * @param propertyValue
	 * @param id
	 * @return
	 */
	public boolean isPropertyUnique(final String propertyName, final Object propertyValue, final PK id);
	
	/**
	 * 根据entity过滤列表
	 * @param entity
	 * @return
	 */
	public List<T> list(T entity);
}
