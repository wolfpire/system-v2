/**
 * 
 */
package com.wolfpire.system.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wolfpire.system.common.Constants;

/**
 * 权限实体类
 * @author lihd
 *
 */

@Entity
@Table(name = "t_sm_authority")
public class Authority implements Serializable {
	
	private static final long serialVersionUID = 2664426688151397348L;
	
	/**
	 * INSERT INTO workflow.T_SM_AUTHORITY (
BG,BIG_PIC_PATH, CREATE_TIME, CREATE_USER, ICON, IS_LEAF, IS_MAX,
 IS_QUICKSTART, IS_SHORTCUT, MEDIUM_PIC_PATH, NAME, PARENT_ID, PATH,
		SEQ, SHOW_HEADER, SHOW_TAIL, SMALL_PIC_PATH, STATUS, TYPE,
		WIN_HEIGHT, WIN_WIDTH, ID)
VALUES
	(
		'', '', '2016-05-10', '', '', '', '',
		'', '', '', '测试目录二', 1, '',
		11, 1, 1, 'menu2', 1, 1,
		'', '', 1718951
	)
	 */
	public static final Integer TYPE_NAV = 1; // 导航
	public static final Integer TYPE_CATALOG = 2; // 目录
	/***
	 * INSERT INTO workflow.T_SM_AUTHORITY (
	BG, BIG_PIC_PATH, CREATE_TIME, CREATE_USER, ICON, IS_LEAF, IS_MAX, IS_QUICKSTART, IS_SHORTCUT,
	MEDIUM_PIC_PATH, NAME, PARENT_ID, PATH, SEQ, SHOW_HEADER, SHOW_TAIL, SMALL_PIC_PATH,
	STATUS, TYPE, WIN_HEIGHT, WIN_WIDTH, ID
)
VALUES
	(
		'', 'icon-max', '2016-05-10', '', '', '', 0, 0, 0,
		'', '测试菜单一', 1718950, 'xxx.shtml?method=xxx', 6, 1, 1, 'icon-min',
		1, 2, 100, 100, 1718952
	)
	 */
	public static final Integer TYPE_MENU = 3; // 菜单
	/**INSERT INTO workflow.T_SM_AUTHORITY (
	BG, BIG_PIC_PATH, CREATE_TIME,
	CREATE_USER, ICON, IS_LEAF,
	IS_MAX, IS_QUICKSTART, IS_SHORTCUT,
	MEDIUM_PIC_PATH, NAME, PARENT_ID,
	PATH, SEQ, SHOW_HEADER,
	SHOW_TAIL, SMALL_PIC_PATH, STATUS,
	TYPE, WIN_HEIGHT, WIN_WIDTH,
	ID
)
VALUES
	(
		'', '', '2016-05-10',
		'', '', '',
		'', '', '',
		'', '新增', 1718952,
		'add', 1, 1,
		1, 'add', 1,
		3, '', '',
		1718954
	)  */
	public static final Integer TYPE_OPERATOR = 4; // 操作

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false, length = 32)
	private Long id; // 主键Id
	
	@Column(name = "name", length = 32)
	private String name; // 权限(菜单)名称
	
	@Column(name = "path", length = 255)
	private String path; // 路径/动作
	
	@Column(name = "parent_id", length = 32)
	private Long parentId; //父级ID
	
	@Column(name = "leaf")
	private boolean leaf = true; // 是否是叶子节点 true 是, flase 否
	
	@Column(name = "seq", length = 1000)
	private Integer seq; // 顺序
	
	@Column(name = "type", length = 1)
	private Integer type;// 类型
	
	@Column(name = "image", length = 50)
	private String image; //图标路径
	
	@Column(name = "del_flag", length = 1)
	private Integer delFlag = Constants.NORMAL_FLAG; // 伪删标识
	
	@Column(name = "create_user_id", length = 32)
	private Long createUserId; // 创建用户 
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")  
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") 
	private Date createTime = new Date(); //创建时间
	
	@Transient
	private String state;
	
	@Transient
	private Long _parentId;
	
	@Transient
	private String text;
	
	@Transient
	private List<Authority> children = new ArrayList<Authority>();
//	private List<Authority> childrenAuthories = new ArrayList<Authority>();

	public Authority() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}
	
	public String getState() {
		if (null == this.state) {
			state = (this.leaf ? "open" : "closed");
		}
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Long get_parentId() {
		_parentId = this.parentId;
		return _parentId;
	}

	public void set_parentId(Long _parentId) {
		this._parentId = _parentId;
	}

	public String getText() {
		text = this.name;
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	/*public List<Authority> getChildrenAuthories() {
		return childrenAuthories;
	}

	public void setChildrenAuthories(List<Authority> childrenAuthories) {
		this.childrenAuthories = childrenAuthories;
	}*/

	public String getImage() {
		return image;
	}

	public List<Authority> getChildren() {
		return children;
	}

	public void setChildren(List<Authority> children) {
		this.children = children;
	}

	public void setImage(String image) {
		this.image = image;
	}

}

