/**
 * 
 */
package com.wolfpire.system.service;

import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.base.service.IBaseHibernateService;
import com.wolfpire.system.model.Role;

/**
 * 角色业务层接口
 * @author lihd
 *
 */
public interface RoleService extends IBaseHibernateService<Role, Long> {
	
	/**
	 * 查询角色列表(带分页)
	 * @param page<User> 
	 * @param filterUser
	 * @return
	 */
	public Page<Role> setPageDataList(Page<Role> page, Role filterRole);
	
	/**
	 * 判断角色是否唯一
	 * @param role
	 * @return
	 */
	public boolean isUnique(Role role);
	
	/**
	 * 获取带有权限信息的角色
	 * @param id
	 * @return
	 */
	public Role getAuthorityInfoRole(Long id);
	
	/**
	 * 绑定角色权限关系
	 * @param id
	 * @param authorityIdsStr
	 */
	public void saveRoleAuthority(Long id, String authorityIdsStr);
	
	/**
	 * 角色关联用户
	 * @param id
	 * @param userIdsStr
	 */
	public void saveRoleUser(Long id, String userIdsStr);
	
	/**
	 * 通过角色获取用户Id集合
	 * @param roleId
	 * @return
	 */
	public String getUserIdsByRoleId(Long roleId);
}
