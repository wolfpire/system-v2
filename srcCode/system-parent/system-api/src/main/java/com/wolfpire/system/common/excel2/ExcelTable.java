package com.wolfpire.system.common.excel2;
import java.util.ArrayList;
import java.util.List;
public class ExcelTable {
	


	private String tableName; //-- 表名，文件名
	private List<ExcelCellData> dataList; //-- 数据
	private List<String> mergeCells; //--合并的单元格信息，每个合并项为，参数格式：开始列,开始行,结束列,结束行
	
	private List<String> columnWidths;//sheet.setColumnView(0, 30);
	

	/**
	 * 构造函数
	 * @param tableName
	 */
	public ExcelTable(String tableName) {
		this.tableName = tableName;
		dataList = new ArrayList<ExcelCellData>();
	}
	
	/**
	 * 往表格中添加数据
	 * @param data
	 */
	public void addData(ExcelCellData data) {
		if(dataList==null) dataList = new ArrayList<ExcelCellData>();
		dataList.add(data);
	}
	
	/**
	 * 添加合并单元格
	 * @param startCol 开始列
	 * @param startRow 开始行
	 * @param endCol 结束列
	 * @param endRow 结束行
	 */
	public void addMergeCell(int startCol, int startRow, int endCol, int endRow) {
		if(mergeCells==null) mergeCells = new ArrayList<String>();
		String mergeCell = (""+startCol+","+startRow+","+endCol+","+endRow);
		for(String mc : mergeCells) {
			if(mc.equals(mergeCell)) return;
		}
		mergeCells.add(mergeCell);
	}
	
	public void addColumnWidth(int column, int width) {
		if(columnWidths==null) columnWidths = new ArrayList<String>();
		String columnWidth = (""+column+","+width);
		for(String cw : columnWidths) {
			if(cw.equals(columnWidth)) return;
		}
		columnWidths.add(columnWidth);
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public List<ExcelCellData> getDataList() {
		return dataList;
	}

	public List<String> getMergeCells() {
		return mergeCells;
	}
	
	public List<String> getColumnWidths() {
		return columnWidths;
	}

}
