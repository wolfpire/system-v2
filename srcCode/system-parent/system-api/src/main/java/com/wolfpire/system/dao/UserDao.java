/**
 * 
 */
package com.wolfpire.system.dao;

import java.util.List;

import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.base.dao.IBaseHibernateDao;
import com.wolfpire.system.model.User;

/**
 * 用户模块 Dao接口
 * @author lihd
 *
 */
public interface UserDao extends IBaseHibernateDao<User, Long> {
	
	/**
	 * 通过账户和密码获取用户
	 * @param account
	 * @param password
	 * @return
	 */
	public User getByAccount(String account, String password);
	
	/**
	 * 根据user过滤查询列表
	 * @param user
	 * @return
	 */
	public List<User> list(User user);
	
	/**
	 * 根据user过滤查询带分页列表
	 * @param page
	 * @param filterUser
	 * @return
	 */
	public Page<User> findUsers(Page<User> page, User filterUser);
	
	/**
	 * 通过账户获取用户
	 * @param account
	 * @return
	 */
	public User getByAccount(String account);
	
	/**
	 * 保存密码
	 * @param id
	 * @param password
	 */
	public void saveUserPassword(Long id, String password);

	public List<User> getUsersByParam(User user, List<Long> roleIds,
			List<Long> orgIds);
}
