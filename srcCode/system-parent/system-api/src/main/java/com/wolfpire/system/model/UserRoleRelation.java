/**
 * 
 */
package com.wolfpire.system.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 用户角色模型
 * @author lihd
 *
 */

@Entity
@Table(name = "t_sm_user_role")
public class UserRoleRelation implements Serializable {
	
	private static final long serialVersionUID = 2667854646451179668L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false, length = 32)
	private Long id;
	
	@Column(name = "user_id", length = 32)
	private Long userId;
	
	@Column(name = "role_id", length = 32)
	private Long roleId;

	public UserRoleRelation() {
		super();
	}

	public UserRoleRelation(Long userId, Long roleId) {
		super();
		this.userId = userId;
		this.roleId = roleId;
	}

	public UserRoleRelation(Long id, Long userId, Long roleId) {
		super();
		this.id = id;
		this.userId = userId;
		this.roleId = roleId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

}
