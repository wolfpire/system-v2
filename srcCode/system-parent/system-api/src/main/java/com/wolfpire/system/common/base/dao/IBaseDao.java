/**
 * 
 */
package com.wolfpire.system.common.base.dao;

import java.io.Serializable;
import java.util.List;

/**
 * 集合持久层的公用的增，删，改，查接口
 * @author lihd
 * 
 * @param <T>实体类
 * @param <PK>主键
 */
public interface IBaseDao<T, PK extends Serializable> {
	
	/**
	 * 保存
	 * @param entity
	 * @return
	 */
	public PK save(T entity);
	
	/**
	 * 更新
	 * @param entity
	 */
	public void update(T entity);
	
	/**
	 * 保存或更新
	 * @param entity
	 */
	public void saveOrUpdate(T entity);
    
	/**
	 * 删除
	 * @param id
	 */
	public void delete(PK id);
	
	/**
	 * 删除
	 * @param entity
	 */
	public void delete(T entity);
	
	/**
	 * 删除
	 * @param ids
	 */
	public void delete(PK... ids);
	
	/**
	 * 根据主键查找对象
	 * @param id
	 * @return
	 */
	public T get(PK id);
	
	/**
	 * 按SQL查询唯一对象
	 * @param querySql
	 * @param values
	 * @return
	 */
	public Object findUniqueBySql(final String querySql, final Object... values);
	
	/**
	 * 按SQL查询
	 * @param querySql
	 * @param objects
	 * @return
	 */
	public List<Object> findBySql(final String querySql, final Object... values);
	
	/**
	 * 按SQL查询返回Integer类型结果
	 * @param querySql
	 * @param values
	 * @return
	 */
	public Integer findSqlInt(final String querySql, final Object... values);
	
	/**
	 * 按SQL查询返回Integer类型结果
	 * @param querySql
	 * @param propertyNames
	 * @param propertyValues
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Integer findSqlInt(String querySql, List<String> propertyNames, List propertyValues);
	
	/**
	 * 按SQL查询返回Long类型结果
	 * @param querySql
	 * @param values
	 * @return
	 */
	public Long findSqlLong(final String querySql, final Object... values);
	
	/**
	 * 按SQL执行修改和删除操作
	 * @param executeSql
	 * @param values
	 */
	public void executeSql(final String executeSql, final Object... values);
	
	/**
	 * 根据主键判断对象是否存在
	 * @param id
	 * @return
	 */
	boolean exists(PK id);
	
	/**
	 * 判断对象的属性值在数据库内是否唯一.
	 * @param propertyName
	 * @param newValue
	 * @param orgValue
	 * @return
	 */
	public boolean isPropertyUnique(final String propertyName, final Object newValue, final Object orgValue);
	
	/**
	 * 判断对象的属性值在数据库内是否唯一
	 * @param propertyName
	 * @param propertyValue
	 * @param id
	 * @return
	 */
	public boolean isPropertyUnique(final String propertyName, final Object propertyValue, final PK id);
	
}
