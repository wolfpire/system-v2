package com.wolfpire.system.common;

import java.util.List;

/**
 * Easyui页面参数
 * @author lihd
 *
 * @param <T>
 */
public class EasyuiPagination<T> {
	/** 总行数 */
	private int total; 
	
	/** 数据列表 */
	private List<?> rows;
	
	/** 汇总数据 */
	private List<?> footer;
	
	public EasyuiPagination() {
	}
	
	public EasyuiPagination(Page<T> page) {
		this.total = page.getTotalCount();
		this.rows = page.getDataList();
	}
	
	public EasyuiPagination(int total, List<?> rows, List<?> footer) {
		super();
		this.total = total;
		this.rows = rows;
		this.footer = footer;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<?> getRows() {
		return rows;
	}

	public void setRows(List<?> rows) {
		this.rows = rows;
	}

	public List<?> getFooter() {
		return footer;
	}

	public void setFooter(List<?> footer) {
		this.footer = footer;
	}
}
