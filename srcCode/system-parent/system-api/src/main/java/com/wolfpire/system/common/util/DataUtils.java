/**
 * 
 */
package com.wolfpire.system.common.util;

/**
 * 数据转换工具类
 * @author lihd
 *
 */
public class DataUtils {
	
	/**
	 * 将字符串转成Int数值
	 * 当字符串为空时候(或者无效)，转换成默认值
	 * @param str
	 * @param defaultValue
	 * @return
	 * @throws Exception 
	 */
	public static Integer convert(String str, int defaultValue, boolean isThrowException) throws Exception {
		if (StringUtils.isBlank(str)) {
			return defaultValue;
		}
		try {
			return Integer.parseInt(str.trim());
		} catch (Exception e) {
			if (isThrowException) {
				throw new Exception(e.getMessage());
			} else {
				return 0; 
			}
		}
	}
	
	/**
	 * 将字符串转成Int数值
	 * 当字符串为空时候(或者无效)，转换成 0
	 * @param str
	 * @return
	 */
	public static Integer convert(String str) {
		try {
			return convert(str, 0, false);
		} catch (Exception e) {
			e.printStackTrace();
			return 0; 
		}
	}
}
