/**
 * 
 */
package com.wolfpire.system.common.util;


/**
 * 拓展StringUtils的方法
 * @author lihd
 *
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils {
	
	/**
	 * 将指定对象转换成字符串
	 * @param obj
	 * 		指定对象
	 * @return
	 * 		转换后的字符串
	 */
	public static String convert(Object obj) {
		if (null == obj) {
			return EMPTY;
		}
		return obj.toString();
	}
	
	/**
	 * 将指定字符串首字母转换成大写字母
	 * @param str
	 * 			指定字符串
	 * @return 
	 * 		返回首字母大写的字符串
	 */
	public static String firstCharUpperCase(String str) {
        StringBuffer buffer = new StringBuffer(str);
        if (buffer.length() > 0) {
            char c = buffer.charAt(0);
            buffer.setCharAt(0, Character.toUpperCase(c));
        }
        return buffer.toString();
    }
}
