package com.wolfpire.system.common.excel2;

public class ExcelCellData {
	


	private int row; //--行标
	private int col; //--列标
	private String value; //--值
	
	/**
	 * 构造函数
	 * @param row 行标
	 * @param col 列标
	 * @param value 值
	 */
	public ExcelCellData(int row, int col, String value) {
		this.row = row;
		this.col = col;
		this.value = value;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
