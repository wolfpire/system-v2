/**
 * 
 */
package com.wolfpire.system.common;

/**
 * 常量类
 * @author lihd
 *
 */
public class Constants {
	
	/** 数据正常状态 */
	public static final Integer NORMAL_FLAG = 1;
	/** 数据伪删除状态 */
	public static final Integer DELETE_FLAG = 0;
	
	/** 用户启用状态 */
	public static final Integer USER_STATUS_ACTIVED = 1;
	/** 用户未激活状态 */
	public static final Integer USER_STATUS_NOT_ACTIVED = 0;
	/** 用户禁用状态 */
	public static final Integer USER_STATUS_FORBIDDEN = -1;
	
	/**性别 男*/
	public static final Integer MAN = 1;
	/**性别 女*/
	public static final Integer WOMAN = 0;
	
	/** 当前用户key */
	public static final String SESSION_USER = "loginUser";
	
	/** 任务审批通过 */
	public static final Integer TASK_STATUS_AGREE = 1;
	/** 任务审批中驳回*/
	public static final Integer TASK_STATUS_REJECT = 2;
	/** 任务审批不通过 */
	public static final Integer TASK_STATUS_REFUSE = -1;
	/** 任务审批中 */
	public static final Integer	TASK_STATUS_APPLY = 0;
	/*** 任务作废状态———— 作废正常*/
	public static final Integer TASK_STATUS_INVALID = 4;
	
	/** 附件路径 */
	public static final String FILE_ROOT_PATH = GlobalUtils.getStringGlobalProperty("file.root.path");
	
	/***任务表单类型Id*/
	public static final String TASK_FORM_TYPE = "1";
	
	/** 任务workflowId */
	public static final Long TASK_WORKFLOW_ID = 16L;
	
	/**任务申请*/
	public static final String TASK_NODE_1 = "任务申请";
	/** 项目负责人审批 */
	public static final String TASK_NODE_2 = "项目负责人审批";
	/** 分管领导审批 */
	public static final String TASK_NODE_3 = "分管领导审批";
	/** 领导审批 */
	public static final String TASK_NODE_4 = "领导审批";
	/** 审批结束 */
	public static final String TASK_NODE_END = "审批结束";
	
	/*** 任务反馈审核状态 未提交 */
	public static final Integer FEEDBACK_AUDIT_UNCOMMIT = 0;
	
	/*** 任务反馈审核状态 待审核 */
	public static final Integer FEEDBACK_AUDIT_UNAUDIT = 1;
	
	/*** 任务反馈审核状态 审核不通过 */
	public static final Integer FEEDBACK_AUDIT_REFUSE = 2;
	
	/*** 任务反馈审核状态 审核通过 */
	public static final Integer FEEDBACK_AUDIT_AGREE = 3;
	
	/*** 任务反馈审计角色编码 */
	public static final String FEEDBACK_AUDIT_ROLE = "feedback_audit";
	
	/*** 任务反馈进度状态 ————[0-25]*/
	public static final String PROGRESS_LEVEL_1 = "1"; //0-25
	/*** 任务反馈进度状态 ————[25-50]*/
	public static final String PROGRESS_LEVEL_2 = "2"; //0-25
	/*** 任务反馈进度状态 ————[50-75]*/
	public static final String PROGRESS_LEVEL_3 = "3"; //0-25
	/*** 任务反馈进度状态 ————[75-99.99]*/
	public static final String PROGRESS_LEVEL_4 = "4"; //0-25
	/*** 任务反馈进度状态 ————[100]*/
	public static final String PROGRESS_LEVEL_5 = "5"; //0-25
	
	/*** 数据字典——是否启用任务计划字段 */
	public static final String DATA_DICT_CODE_TASKPLAN = "is_task_plan";
	
	/**
	 * 完成状态 1：完成  0：正常
	 */
	public static final Integer FINISHED_STATUS = 1;
	public static final Integer NO_FINISHED_STATUS = 0;
	
	/** 超时状态 1：超时  0：未超时	 */
	public static final Integer TIME_OUT = 1;
	public static final Integer NO_TIME_OUT = 0;
	
	/**
	 * 任务类型 1：任务安排   2：流程审批
	 */
	public static final Integer task_type_assgin = 1;
	public static final Integer task_type_audit = 2;
	
}
