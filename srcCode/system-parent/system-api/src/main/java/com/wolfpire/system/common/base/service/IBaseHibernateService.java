/**
 * 
 */
package com.wolfpire.system.common.base.service;

import java.io.Serializable;

/**
 * 基于hibernate的领域对象业务管理类基类接口
 * @author lihd
 * 
 * @param <T> 实体类
 * @param <PK> 实体类主键类型
 */
public interface IBaseHibernateService<T, PK extends Serializable> extends IBaseService<T, PK> {

}
