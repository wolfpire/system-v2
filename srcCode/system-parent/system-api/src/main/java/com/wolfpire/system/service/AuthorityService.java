/**
 * 
 */
package com.wolfpire.system.service;

import java.util.List;

import com.wolfpire.system.common.EasyuiPage;
import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.base.service.IBaseHibernateService;
import com.wolfpire.system.model.Authority;

/**
 * 权限业务接口
 * @author lihd
 *
 */
public interface AuthorityService extends IBaseHibernateService<Authority, Long> {
	
	/**
	 * 查询权限列表[带分页]
	 * @param page
	 * @param filterAuthority
	 * @return
	 */
	public Page<Authority> setPageDataList(EasyuiPage<Authority> page, Authority filterAuthority);
	
	/**
	 * 同一级下，判断权限名称唯一性
	 * @param authority
	 * @return
	 */
	public boolean isUnique(Authority authority);
	
	/**
	 * 逻辑删除
	 * @param id
	 */
	public void del(Long id);
	
	/**
	 * 根据父id查询子权限信息
	 * @param parentId
	 * @return
	 */
	public List<Authority> findChildRen(Long parentId);
	
	/**
	 * 查询用户权限集合
	 * @param userId
	 * @return
	 */
	public List<Authority> findByUserId(Long userId);
	
	/**
	 * 根据用户构建菜单
	 * @param userId
	 * @return
	 */
	public List<Authority> buildMenuTree(Long userId);
	
	/**
	 * 根据用户权限构建菜单
	 * @param userId
	 * @return
	 */
	public List<Authority> buildMenuTree(List<Authority> userAuthorities);
	
	/**
	 * 构建树
	 * @param authority
	 * @return
	 */
	public List<Authority> listTree(Authority authority);
}
