/**
 * 
 */
package com.wolfpire.system.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wolfpire.system.common.Constants;

/**
 * 数据字典
 * @author lihd
 *
 */

@Entity
@Table(name = "t_sm_data_dict")
public class DataDict implements Serializable {
	
	private static final long serialVersionUID = -8645645792122590065L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false, length = 16)
	private Long id; //主键Id
	
	@Column(name = "name", length = 50)
	private String name; //名称
	
	@Column(name = "code", length = 50)
    private String code; // 编码
	
	@Column(name = "seq", length = 2)
    private Integer seq; //顺序
	
	@Column(name = "parent_id", length = 16)
	private Long parentId; //父级Id
	
	@Column(name = "leaf")
	private boolean leaf = true; // 是否是叶子节点 true 是, flase 否
	
	@Column(name = "remark", length = 255)
    private String remark; //备注
	
	@Column(name = "create_user_id", length = 16)
	private Long createUserId; //创建人
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")  
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") 
	private Date createTime = new Date(); //创建时间
	
	@Column(name = "del_flag", length = 1)
    private Integer delFlag = Constants.NORMAL_FLAG; //伪删标识
	
	@Transient
	private String state;
	
	@Transient
	private Long _parentId;
	
	@Transient
	private String text;

	public DataDict() {
		super();
	}

	public DataDict(String name, String code, Integer seq, Long parentId,
			boolean leaf, String remark, Long createUserId, Date createTime) {
		super();
		this.name = name;
		this.code = code;
		this.seq = seq;
		this.parentId = parentId;
		this.leaf = leaf;
		this.remark = remark;
		this.createUserId = createUserId;
		this.createTime = createTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	public String getState() {
		state = (this.leaf ? "open" : "closed");
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Long get_parentId() {
		_parentId = this.parentId;
		return _parentId;
	}

	public void set_parentId(Long _parentId) {
		this._parentId = _parentId;
	}

	public String getText() {
		text = this.name;
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
}
