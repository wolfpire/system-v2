/**
 * 
 */
package com.wolfpire.system.dao;

import java.util.List;

import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.base.dao.IBaseHibernateDao;
import com.wolfpire.system.model.Org;

/**
 * 组织模块Dao接口
 * @author lihd
 *
 */
public interface OrgDao extends IBaseHibernateDao<Org, Long> {
	
	/**
	 * 根据org过滤查询组织列表
	 * @param org
	 * @return
	 */
	public List<Org> list(Org org);
	
	/**
	 * 根据filterOrg过滤查询带分页列表
	 * @param page
	 * @param filterOrg
	 * @return
	 */
	public Page<Org> findOrgs(Page<Org> page, Org filterOrg);
	
	/**
	 * 相同parentId下name是否重复
	 * @param name
	 * @return
	 */
	public boolean isUniqueName(Org org);

}
