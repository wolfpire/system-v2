/**
 * 
 */
package com.wolfpire.system.model;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 附件实体类
 * @author lihd
 *
 */

@Entity
@Table(name = "t_sm_attachment")
public class Attachment implements Serializable {
	
	private static final long serialVersionUID = 431327510460361997L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "att_id", unique = true, nullable = false, length = 16)
	private Long attId;
	
	@Column(name = "atta_type", length=20)
	private String attaType;
	
	@Column(name = "biz_id", length = 16)
	private Long bizId;
	
	@Column(name = "title", length = 255)
	private String title;
	
	@Column(name = "file_path", length = 255)
	private String filePath;
	
	@Column(name = "file_content")
	private Blob fileContent;
	
	@Column(name = "file_name", length = 255)
	private String fileName;
	
	@Column(name = "file_size", length = 50)
	private String fileSize;
	
	@Column(name = "down_count", length = 16)
	private Long downCount;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")  
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") 
	private Date createTime = new Date();//上传时间

	public Attachment() {
		super();
	}

	public Attachment(Long attId, String attaType, Long bizId, String title,
			String filePath, Blob fileContent, String fileName,
			String fileSize, Long downCount) {
		super();
		this.attId = attId;
		this.attaType = attaType;
		this.bizId = bizId;
		this.title = title;
		this.filePath = filePath;
		this.fileContent = fileContent;
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.downCount = downCount;
	}

	public Long getAttId() {
		return attId;
	}

	public void setAttId(Long attId) {
		this.attId = attId;
	}

	public String getAttaType() {
		return attaType;
	}

	public void setAttaType(String attaType) {
		this.attaType = attaType;
	}

	public Long getBizId() {
		return bizId;
	}

	public void setBizId(Long bizId) {
		this.bizId = bizId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	@JsonIgnore
	@JsonIgnoreProperties
	public Blob getFileContent() {
		return fileContent;
	}

	public void setFileContent(Blob fileContent) {
		this.fileContent = fileContent;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public Long getDownCount() {
		return downCount;
	}

	public void setDownCount(Long downCount) {
		this.downCount = downCount;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
