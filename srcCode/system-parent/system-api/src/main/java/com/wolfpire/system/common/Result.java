/**
 * 
 */
package com.wolfpire.system.common;

import java.io.Serializable;


/**
 * 返回结果集
 * @author lihd
 *
 */
public class Result implements Serializable {
	
	private static final long serialVersionUID = 2903270692344710081L;

	/*** 成功或失败 */
	private boolean success;
	
	/*** 返回信息 */
	private String message;
	
	/*** 返回对象 */
	private Object data;
	
	/*** 简单结果,只返回true */
	public static final Result SUCCESS = new Result(true);
	
	/*** 简单结果,只返回false */
	public static final Result FAILURE = new Result(false);

	public Result() {
		super();
	}

	public Result(boolean success) {
		super();
		this.success = success;
	}

	public Result(boolean success, String message) {
		super();
		this.success = success;
		this.message = message;
	}

	public Result(boolean success, String message, Object data) {
		super();
		this.success = success;
		this.message = message;
		this.data = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	
}
