/**
 * 
 */
package com.wolfpire.system.common;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * 分页工具
 * @author lihd
 *
 */
public class Page<T> implements Serializable {
	
	private static final long serialVersionUID = 7787721141883771155L;

	/** 默认当前页 */
	public final static int DEFAULT_CURRENT_PAGE = 0;
	
	/** 默认页面记录数 */
	public final static int DEFAULT_PAGE_SIZE = Integer.MAX_VALUE;
	
	/** 当前页码,从0开始 */
	private int currentPage = DEFAULT_CURRENT_PAGE;
	
	/** 每页记录数,为0则返回所有的记录数 */
	private int pageSize = DEFAULT_PAGE_SIZE;
	
	/** 总记录数 */
	private int totalCount;
	
	/** 页面记录集 */
	private List<T> dataList = Collections.emptyList();

	public Page() {
		super();
	}

	public Page(int currentPage, int pageSize) {
		super();
		this.currentPage = currentPage;
		this.pageSize = pageSize;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public List<T> getDataList() {
		return dataList;
	}

	public Page<T> setDataList(List<T> dataList) {
		this.dataList = dataList;
		return this;
	}
	
}
