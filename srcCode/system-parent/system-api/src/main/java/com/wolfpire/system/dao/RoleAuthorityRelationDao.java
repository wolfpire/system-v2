/**
 * 
 */
package com.wolfpire.system.dao;

import java.util.List;

import com.wolfpire.system.common.base.dao.IBaseHibernateDao;
import com.wolfpire.system.model.RoleAuthorityRelation;

/**
 * 角色权限持久层接口
 * @author lihd
 *
 */
public interface RoleAuthorityRelationDao extends IBaseHibernateDao<RoleAuthorityRelation, Long> {
	
	/**
	 * 绑定角色权限关系
	 * @param roleId
	 * @param authorityIds
	 */
	public void relate(Long roleId, Long[] authorityIds);
	
	/**
	 * 绑定角色权限关系
	 * @param roleId
	 * @param authorityIds
	 */
	public void relate(Long roleId, List<Long> authorityIds);
	
	/**
	 * 查询角色关联权限
	 * @param roleId	角色Id
	 * @return
	 */
	public List<RoleAuthorityRelation> getRARealation(Long roleId);

}
