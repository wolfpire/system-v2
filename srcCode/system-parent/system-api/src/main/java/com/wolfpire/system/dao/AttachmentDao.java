/**
 * 
 */
package com.wolfpire.system.dao;

import java.util.List;
import java.util.Map;

import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.base.dao.IBaseHibernateDao;
import com.wolfpire.system.model.Attachment;

/**
 * 附件持久层接口
 * @author lihd
 * @date 20160719
 *
 */ 
public interface AttachmentDao extends IBaseHibernateDao<Attachment, Long> {
	
	/**
	 * 根据业务信息获取附件
	 * @param bizId	业务Id
	 * @param attaType	业务类型
	 * @return
	 */
	public List<Attachment> getByBiz(Long bizId, String attaType);
	
	/**
	 * 根据主键获取对应附件
	 * @param attId
	 * @return
	 */
	public List<Attachment> getById(Long[] attId);
	
	/**
	 * 任务附件列表查询
	 * @param page
	 * @param filterAttachment
	 * @return
	 */
	Page<Map<String, Object>> findTaskDetails(Page<Map<String, Object>> page,
			Attachment filterAttachment, Long taskId, List<Long> feedBackIds);
	
	/**
	 * 附件列表(根据项目指标id进行查询统计)
	 * @param page
	 * @param projectIndexId
	 * @return
	 */
	public Page<Map<String, Object>> findAttachmentsByPiId(Page<Map<String, Object>> page, Long projectIndexId, Attachment filterAttachment);
}
