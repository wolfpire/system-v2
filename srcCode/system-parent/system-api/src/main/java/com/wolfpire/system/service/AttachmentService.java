/**
 * 
 */
package com.wolfpire.system.service;

import java.util.List;
import java.util.Map;

import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.base.service.IBaseHibernateService;
import com.wolfpire.system.model.Attachment;

/**
 * 附件业务层接口
 * @author lihd
 *
 */
public interface AttachmentService extends IBaseHibernateService<Attachment, Long> {
	
	/**
	 * 根据业务信息获取附件
	 * @param bizId	业务Id
	 * @param attaType	业务类型
	 * @return
	 */
	public List<Attachment> getByBiz(Long bizId, String attaType);
	
	/**
	 * 根据主键获取对应附件
	 * @param attId
	 * @return
	 */
	public List<Attachment> getById(Long[] attId);
	
	/**
	 * 逻辑删除
	 * @param attId
	 */
	public void del(Long attId);
	
	/**
	 * 批量更新附近与业务关系
	 * @param attId
	 * @param bizId
	 * @param attaType
	 */
	public void batchRelateAttachment(Long[] attId, Long bizId, String attaType);
	
	/**
	 * 根据项目指标id查询附近
	 * @param page
	 * @param projectIndexId
	 * @return
	 */
	public Page<Map<String, Object>> setAttachmentsByPiId(Page<Map<String, Object>> page, Long projectIndexId, Attachment filterAttachment);
}
