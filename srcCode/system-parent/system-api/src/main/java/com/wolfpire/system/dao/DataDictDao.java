/**
 * 
 */
package com.wolfpire.system.dao;

import java.util.List;

import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.base.dao.IBaseHibernateDao;
import com.wolfpire.system.model.DataDict;

/**
 * 数据字典持久层接口
 * @author lihd
 *
 */
public interface DataDictDao extends IBaseHibernateDao<DataDict, Long> {
	
	/**
	 * 根据过滤条件查询dataDict列表
	 * @param dataDict
	 * @return
	 */
	public List<DataDict> lists(DataDict dataDict);
	
	/**
	 * 分页列表
	 * @param page
	 * @param filterDataDict
	 * @return
	 */
	public Page<DataDict> findDataDicts(Page<DataDict> page, DataDict filterDataDict);
	
	/**
	 * 相同parentId下name是否重复
	 * @param name
	 * @return
	 */
	public boolean isUniqueName(DataDict dataDict);
	
	/**
	 * 根据编码获取子集
	 * @param code
	 * @return
	 */
	public List<DataDict> findChildrenByCode(String code);
	
	/**
	 * 根据code获取唯一数据字典对象
	 * @param code
	 * @return
	 */
	public DataDict findUniqueByCode(String code);
	
}
