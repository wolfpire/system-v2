/**
 * 
 */
package com.wolfpire.system.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wolfpire.system.common.Constants;

/**
 * 用户表
 * @author lihd
 *
 */

@Entity
@Table(name = "t_sm_user")
public class User implements Serializable, UserDetails {
	
	private static final long serialVersionUID = 1339937204555946220L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false, length = 16)
	private Long id;//主键Id
	
	@Column(name = "account", length = 32)
	private String account;//账号
	
	@Column(name = "nickname", length = 20)
	private String nickName;//昵称
	
	@Column(name = "password", length = 50)
	private String password;//密码
	
	@Column(name = "status", length = 2)
	private Integer status = Constants.USER_STATUS_ACTIVED;//用户状态 
	
	@Column(name = "del_flag", length = 1)
	private Integer delFlag = Constants.NORMAL_FLAG;//伪删标识
	
	@Column(name = "age", length = 4)
	private Integer age;//年龄
	
	@Column(name = "gender", length = 1)
	private Integer gender;//性别
	
	@Column(name = "email", length = 75)
	private String email; //电子邮箱
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")  
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") 
	private Date createTime = new Date();//创建时间[注册时间]
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_time")
	private Date updateTime = new Date();//修改时间
	
	@Column(name = "firstname", length = 20)
	private String firstName;//名字
	
	@Column(name = "middlename", length = 20)
	private String middleName;//教名
	
	@Column(name = "lastname", length = 20)
	private String lastName;//姓氏
	
	@Column(name = "fullname", length = 75)
	private String fullName;//全名
	
	@Column(name = "remark", length = 255)
	private String remark;//备注
	
	@Column(name = "office_phone", length = 20)
	private String officePhone; //办公室电话
	
	@Column(name = "fax_number", length = 20)
	private String faxNumber; //传真号码
	
	@Column(name = "mobile_phone", length = 20)
	private String mobilePhone; //手机号码
	
	@Transient
	private Long[] roleIds; // 角色Id集合
	
	@Transient
	private String roleIdsStr; // 角色Id串
	
	@Transient
	private String roleNamesStr; //角色名称
	
	@Transient
	private Long orgId; //所属组织Id
	
	@Transient
	private String orgName ; //所属组织名称
	
	@Transient
	private Long postId; //职务Id
	
	@Transient
	private String postName; //职务名称
	
	@Transient
	private Long[] authorityIds; //权限Id集合
	
	@Transient
	private List<Authority> userAuthorities;

	public User() {
		super();
	}

	public User(Long id, String account, String nickName, String password,
			Integer age, Integer gender, String email, String remark) {
		super();
		this.id = id;
		this.account = account;
		this.nickName = nickName;
		this.password = password;
		this.age = age;
		this.gender = gender;
		this.email = email;
		this.remark = remark;
	}

	public User(Long id, String account, String nickName, String password,
			Integer status, Integer delFlag, Integer age, Integer gender,
			String email, Date createTime, Date updateTime, String firstName,
			String middleName, String lastName, String fullName, String remark) {
		super();
		this.id = id;
		this.account = account;
		this.nickName = nickName;
		this.password = password;
		this.status = status;
		this.delFlag = delFlag;
		this.age = age;
		this.gender = gender;
		this.email = email;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.remark = remark;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public Long[] getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(Long[] roleIds) {
		this.roleIds = roleIds;
	}

	public String getRoleIdsStr() {
		if (!ArrayUtils.isEmpty(this.roleIds)) {
			return StringUtils.join(this.roleIds, ",");
		}
		return roleIdsStr;
	}

	public void setRoleIdsStr(String roleIdsStr) {
		this.roleIdsStr = roleIdsStr;
	}

	public String getRoleNamesStr() {
		return roleNamesStr;
	}

	public void setRoleNamesStr(String roleNamesStr) {
		this.roleNamesStr = roleNamesStr;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public String getPostName() {
		return postName;
	}

	public void setPostName(String postName) {
		this.postName = postName;
	}

	public Long[] getAuthorityIds() {
		return authorityIds;
	}

	public void setAuthorityIds(Long[] authorityIds) {
		this.authorityIds = authorityIds;
	}
	
	public List<Authority> getUserAuthorities() {
		return userAuthorities;
	}

	public void setUserAuthorities(List<Authority> userAuthorities) {
		this.userAuthorities = userAuthorities;
	}

	/***spring security userdetails**/
	@Override
	@Transient
	@JsonIgnore
	public Collection<GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
		if (!ArrayUtils.isEmpty(this.authorityIds)) {
			for (Long authorityId : this.authorityIds) {
				grantedAuthorities.add(new GrantedAuthorityImpl("AUTH_" + authorityId));
			}
		}
		return grantedAuthorities;
	}

	@Override
	@Transient
	@JsonIgnore
	public String getUsername() {
		return this.account;
	}

	@Override
	@Transient
	@JsonIgnore
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	@Transient
	@JsonIgnore
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	@Transient
	@JsonIgnore
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	@Transient
	@JsonIgnore
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	
}
