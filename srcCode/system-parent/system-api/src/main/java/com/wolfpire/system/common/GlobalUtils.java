/**
 * 
 */
package com.wolfpire.system.common;

import java.io.InputStream;
import java.util.Properties;

/**
 * properties读取工具类
 * @author lihd
 *
 */
public class GlobalUtils {
	
	private static Properties initProps = null;

	public static String getGlobalProperty(String name) {
		loadProperties();
		if(initProps.getProperty(name) != null && !initProps.getProperty(name).equals(""))
			return initProps.getProperty(name);
		else
			System.out.println("===================================");
			System.out.println("unknow properties key:" + name + "");
			System.out.println("===================================");
			return "";
	}
	
	public static boolean getBooleanGlobalProperty(String name) {
		loadProperties();
		if(initProps.getProperty(name) != null && !initProps.getProperty(name).equals("")) {
			if(initProps.getProperty(name).equals("true"))
				return true;
			else return false;
		}
		else return false;
	}
	
	public static int getIntGlobalProperty(String name) {
		String prop = getGlobalProperty(name);
		return Integer.parseInt(prop);
	}
	
	public static String getStringGlobalProperty(String name) {
		return getGlobalProperty(name);
	}
	
	private synchronized static void loadProperties() {
		if (initProps == null) {
			initProps = new Properties();
			InputStream in = null;
			try {
				in = GlobalUtils.class.getResourceAsStream("/global.properties");
				initProps.load(in);
			} catch (Exception e) {
				System.err.println("Load global.properties configure file error!");
			} finally {
				try {
					if (in != null) {
						in.close();
					}
				} catch (Exception e) {
				}
			}
		}
	}

}
