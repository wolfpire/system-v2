/**
 * 
 */
package com.wolfpire.system.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wolfpire.system.common.Constants;

/**
 * 角色实体类
 * @author lihd
 *
 */

@Entity
@Table(name = "t_sm_role")
public class Role implements Serializable {
	
	private static final long serialVersionUID = -1289062153270697592L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false, length = 32)
	private Long id;//主键Id
	
	@Column(name = "name", length = 75)
	private String name;// 名称
	
	@Column(name = "remark", length = 75)
	private String remark; //备注
	
	@Column(name = "del_flag", length = 1)
	private Integer delFlag = Constants.NORMAL_FLAG;//伪删标识
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")  
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") 
	private Date createTime = new Date();//创建时间[注册时间]
	
	@Column(name = "create_user_id", length = 16)
	private Long createUserId;
	
	@Transient
	private Long[] authorityIds;
	
	@Transient
	private String authorityIdsStr;
	
	@Transient
	private String text;

	public Role() {
		super();
	}

	public Role(Long id, String name, String remark, Integer delFlag,
			Date createTime, Long createUserId) {
		super();
		this.id = id;
		this.name = name;
		this.remark = remark;
		this.delFlag = delFlag;
		this.createTime = createTime;
		this.createUserId = createUserId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

	public String getAuthorityIdsStr() {
		if (!ArrayUtils.isEmpty(this.authorityIds)) {
			return StringUtils.join(this.authorityIds, ",");
		}
		return authorityIdsStr;
	}

	public void setAuthorityIdsStr(String authorityIdsStr) {
		this.authorityIdsStr = authorityIdsStr;
	}

	public void setAuthorityIds(Long[] authorityIds) {
		this.authorityIds = authorityIds;
	}

	public Long[] getAuthorityIds() {
		return authorityIds;
	}

	public String getText() {
		text = this.name;
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
}
