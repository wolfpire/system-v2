/**
 * 
 */
package com.wolfpire.system.dao;

import java.util.List;

import com.wolfpire.system.common.base.dao.IBaseHibernateDao;
import com.wolfpire.system.model.UserRoleRelation;

/**
 * 用户角色持久层接口
 * @author lihd
 *
 */
public interface UserRoleRelationDao extends IBaseHibernateDao<UserRoleRelation, Long> {
	
	/**
	 * 绑定用户和角色之间的关系
	 * @param userId
	 * @param roleIds
	 */
	public void relate(Long userId, Long[] roleIds);
	
	/**
	 * 绑定用户和角色之间的关系
	 * @param userId
	 * @param roleIds
	 */
	public void relate(Long userId, List<Long> roleIds);
	
	/**
	 * 获取用户角色关系
	 * @param userId
	 */
	public List<UserRoleRelation> getURRelation(Long userId);
	
	/**
	 * 通过角色Id获取用户
	 * @param roleId
	 * @return
	 */
	public List<UserRoleRelation> getRURelation(Long roleId);
	
	/**
	 * 绑定角色与用户之间的关系
	 * @param roleId
	 * @param userIdsStr
	 */
	public void relateRoleUsers(Long roleId, String userIdsStr);

}
