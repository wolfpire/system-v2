/**
 * 
 */
package com.wolfpire.system.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.wolfpire.system.common.Constants;

/**
 * 组织表
 * @author lihd
 *
 */

@Entity
@Table(name = "t_sm_org")
public class Org implements Serializable {
	
	private static final long serialVersionUID = 4379587659579408707L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false, length = 32)
	private Long id; //主键 组织ID
	
	@Column(name = "name", length = 75)
	private String name; //组织名称
	
	@Column(name = "code", length = 20)
	private String code; //组织编码
	
	@Column(name = "parent_id", length = 32)
	private Long parentId; //父级ID
	
	@Column(name = "seq", length = 3)
	private Integer seq; //顺序
	
	@Column(name = "leaf")
	private boolean leaf = true; // 是否是叶子节点 true 是, flase 否
	
	@Column(name = "remark", length = 255)
	private String remark; //备注
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time")
	private Date createTime = new Date(); //创建时间
	
	@Column(name = "del_flag", length = 1)
	private Integer delFlag = Constants.NORMAL_FLAG; //伪删标识
	
	@Transient
	private String state;
	
	@Transient
	private Long _parentId;
	
	@Transient
	private String text;

	public Org() {
		super();
	}

	public Org(String name, String code, Integer seq) {
		super();
		this.name = name;
		this.code = code;
		this.seq = seq;
	}

	public Org(String name, String code, Long parentId, Integer seq) {
		super();
		this.name = name;
		this.code = code;
		this.parentId = parentId;
		this.seq = seq;
	}

	public Org(String name, String code, Long parentId, Integer seq,
			boolean leaf, String remark) {
		super();
		this.name = name;
		this.code = code;
		this.parentId = parentId;
		this.seq = seq;
		this.leaf = leaf;
		this.remark = remark;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getState() {
		state = (this.leaf ? "open" : "closed");
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Long get_parentId() {
		_parentId = this.parentId;
		return _parentId;
	}

	public void set_parentId(Long _parentId) {
		this._parentId = _parentId;
	}

	public String getText() {
		text = this.name;
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
