package com.wolfpire.system.service;

import java.util.List;

import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.base.service.IBaseHibernateService;
import com.wolfpire.system.model.User;

/**
 * 用户模块 业务层接口
 * @author lihd
 *
 */
public interface UserService extends IBaseHibernateService<User, Long> {
	
	/**
	 * 通过账号和密码获取用户
	 * @param account
	 * @param password
	 * @return
	 */
	public User getByAccount(String account, String password);
	
	/**
	 * 获取账户信息
	 * @param account
	 * @return
	 */
	public User getByAccount(String account);
	
	/**
	 * 查询用户列表(带分页)
	 * @param page<User> 
	 * @param filterUser
	 * @return
	 */
	public Page<User> setPageDataList(Page<User> page, User filterUser);
	
	/**
	 * 获取完整信息的用户
	 * @param id	用户主键Id
	 * @return
	 */
	public User getCompleteUser(Long id);
	
	/**
	 * 获取带有角色信息的用户
	 * @param id	用户主键Id
	 * @return
	 */
	public User getRolesInfoUser(Long id);
	
	/**
	 * 根据角色Id获取用户
	 * @param roleId
	 * @return
	 */
	public List<User> getUsersByRoleId(Long roleId);
	
	/**
	 * 根据组织获取用户
	 * @param orgId
	 * @return
	 */
	public List<User> getUsersByOrgId(Long orgId);
	
	/**
	 * 修改密码
	 * @param id
	 * @param password
	 */
	public void saveUserPassword(Long id, String password);
	
	/**
	 * 查找用户
	 * @param user
	 * @param roleIds
	 * @param orgIds
	 * @return
	 */
	public List<User> getUsersByParam(User user, List<Long> roleIds, List<Long> orgIds);
	
	/**
	 * 赋予用户权限
	 * @param id
	 * @param roleIds
	 */
	public void grantUserRoles(Long id, String roleIds);
}
