/**
 * 
 */
package com.wolfpire.system.service;

import java.util.List;

import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.Result;
import com.wolfpire.system.common.base.service.IBaseHibernateService;
import com.wolfpire.system.model.Org;

/**
 * 组织模块业务层接口
 * @author lihd
 *
 */
public interface OrgService extends IBaseHibernateService<Org, Long> {
	
	/**
	 * 查询组织列表[带分页]
	 * @param page	分页对象
	 * @param filterOrg	过滤条件
	 * @return
	 */
	public Page<Org> setPageDataList(Page<Org> page, Org filterOrg);
	
	/**
	 * 查询org是否唯一
	 * @param newOrg
	 * @return
	 */
	public Result isUnique(Org newOrg);
	
	/**
	 * 根据父组织id查询子组织
	 * @param parentId
	 * @return
	 */
	public List<Org> findChildRen(Long parentId);
	
	/**
	 * 伪删
	 * @param id
	 */
	public void del(Long id);
}
