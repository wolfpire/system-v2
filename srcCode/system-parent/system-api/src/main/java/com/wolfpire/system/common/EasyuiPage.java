/**
 * 
 */
package com.wolfpire.system.common;


/**
 * @author lihd
 *
 */
public class EasyuiPage<T> extends Page<T> {
	
	private static final long serialVersionUID = -3421014752459230807L;

	/** 当前页码,从0开始 */
	private int page = DEFAULT_CURRENT_PAGE;
	
	/** 每页记录数,为0则返回所有的记录数 */
	private int rows = Integer.MAX_VALUE;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page - 1;
		this.setCurrentPage(this.page);
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
		this.setPageSize(rows);
	}
	
}
