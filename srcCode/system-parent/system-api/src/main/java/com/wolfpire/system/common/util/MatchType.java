/**
 * 
 */
package com.wolfpire.system.common.util;

/**
 * @author lihd
 *
 */
public enum MatchType {
	EQ, NE, LE, LT, GE, GT, LIKE, LIKEEND, LIKESTART;
}