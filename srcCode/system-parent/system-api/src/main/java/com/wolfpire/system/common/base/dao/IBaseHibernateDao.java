/**
 * 
 */
package com.wolfpire.system.common.base.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Criterion;

import com.wolfpire.system.common.util.MatchType;

/**
 * 封装Hibernate原生API的CRUD泛型基类.
 * @author lihd
 * 
 * @param <T>实体类
 * @param <PK>主键
 */
public interface IBaseHibernateDao<T, PK extends Serializable> extends IBaseDao<T, PK> {
	
	/**
	 * load方式加载对象
	 * @param id
	 * @return
	 */
	public T load(final PK id);
	
	public void merge(T entity);
	
	/**
	 * 初始化大数据对象或级联对象
	 * @param proxy 代理对象
	 */
	public void init(Object proxy);
	
	/**
	 * 按HQL查询对象
	 * @param queryHql
	 * @param values
	 * @return
	 */
	public Object findUniqueByHql(final String queryHql, final Object... values);
	
	/**
	 * 按属性查找对象列表,匹配方式为相等.
	 * @param propertyName 属性名称[注意是实体类属性名称，不是表字段名称]
	 * @param value
	 * @return
	 */
	public T findUniqueBy(final String propertyName, final Object value);
	
	/**
	 * 按Criteria查询唯一对象
	 * @param criterions
	 * @return
	 */
	public T findUniqueByCriteria(final Criterion... criterions);
	
	/**
	 * 按HQL查询对象列表
	 * @param queryHql
	 * @param values
	 * @return
	 */
	public List<T> findByHql(final String queryHql, final Object... values);
	
	/**
	 * 按属性查找对象列表,匹配方式为相等.
	 * @param propertyName 属性名称[注意是实体类属性名称，不是表字段名称]
	 * @param value
	 * @return
	 */
	public List<T> findBy(final String propertyName, final Object value);
	
	/**
	 * 按Criteria查询对象列表.
	 * @param criterions
	 * @return
	 */
	public List<T> findByCriteria(final Criterion... criterions);
	
	/**
	 * 按Criteria查询Integer类型结果
	 * @param criterions
	 * @return
	 */
	public Integer findIntByCriteria(final Criterion... criterions);
	
	/**
	 * 按HQL查询Integer类型结果.
	 * @param queryHql
	 * @param values
	 * @return
	 */
	public Integer findInt(final String queryHql, final Object... values);
	
	/**
	 * 按HQL查询Long类型结果.
	 * @param queryHql
	 * @param values
	 * @return
	 */
	public Long findLong(final String queryHql, final Object... values);
	
	/**
	 * 根据HQL执行修改和删除操作
	 * @param updateHql
	 * @param values
	 */
	public void executeHql(final String updateHql, final Object... values);
	
	/**
	 * 根据查询HQL与参数列表创建Query对象.
	 * @param queryHql
	 * @param values
	 * @return
	 */
	public Query createQuery(final String queryHql, final Object... values);
	
	/**
	 * 根据查询SQL与参数列表创建Query对象.
	 * @param querySql
	 * @param values
	 * @return
	 */
	public Query createSqlQuery(final String querySql, final Object... values);
	
	/**
	 * 根据查询SQL与参数列表创建Query对象.
	 * @param querySql
	 * @param propertyNames
	 * @param propertyValues
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public SQLQuery createSqlQuery(final String querySql, List<String> propertyNames, List propertyValues);
	
	/**
	 * 根据Criterion条件创建Criteria.
	 * @param criterions
	 * @return
	 */
	public Criteria createCriteria(final Criterion... criterions);
	
	/**
	 * 判断对象的属性值在数据库内是否唯一
	 * @param propertyName
	 * @param propertyValue
	 * @param id
	 * @return
	 */
	public boolean isPropertyUnique(final String[] propertyName, final Object[] propertyValue, final MatchType[] matchType, final PK id);
	
}
