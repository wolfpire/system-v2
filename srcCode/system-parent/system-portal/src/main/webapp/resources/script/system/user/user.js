$(function(){
	/*** 初始化组织树 */
	$('#org_tree').tree({
	    url : 'org/orgs',
	    onLoadSuccess:function(node,data){
	         var t = $(this);
	  		 if(data){
			     $(data).each(function(index,d){
					if(this.state == 'closed'){
					    t.tree('expandAll');
					}
			     });
			}
	    },
	    onClick : function (node) {
	    	//console.log(node);
	    	$('#dg').datagrid('reload', {
	    		orgId : node.id
	    	});
	    }
	});
	
	/** 新增 */
	$('#add').click(function() {
		/*$('#form').form('clear');
		$('#win').window('open');*/
		var url = 'user/add.shtml';
		var node = $('#org_tree').tree('getSelected');
		if (null != node) {
			url = url + '?orgId=' + node.id;
		}
		showEasyUiWindow('contentFrame', url, 'win', '新增用户', 625, 350);
	});
	
	/** 编辑 */
	$('#edit').click(function() {
		var row = $('#dg').datagrid('getSelected');
		if (null == row) {
			$.messager.alert('warning', '请选择一条数据！', 'warning');
		} else {
			var url = 'user/edit.shtml?id=' + row.id;
			showEasyUiWindow('contentFrame', url, 'win', '编辑用户', 625, 350);
		}
	});
	/*** 删除 */
	$('#remove').click(function(){
		var row = $('#dg').datagrid('getSelected');
		if (row) {
			$.messager.confirm('确认', '你确认要删除此条数据吗?', function(r) {
				if (r) {
					var id = row.id;				
					$.get('user/delete.shtml', {id : id}, function(rs) {
						if (rs.success == true) {
							reSearch();
							$.messager.alert('warning', rs.message, 'warning');
						} else {
							$.messager.alert('warning', rs.message, 'warning');
						}
					});
				}
			});
		} else {
			$.messager.alert('warning', '请选择一条数据！', 'warning');
		}
	});
	/*** 详情 */
	$('#detail').click(function() {
		var row = $('#dg').datagrid('getSelected');
		if (null == row) {
			$.messager.alert('warning', '请选择一条数据！', 'warning');
		} else {
			var url = 'user/detail.shtml?id=' + row.id;
			showEasyUiWindow('contentFrame', url, 'win', '用户详情', 625, 350);
		}
	});
	
	/*** 赋予角色 */
	$('#grantRole').click(function(){
		var row = $('#dg').datagrid('getSelected');
		if (null == row) {
			$.messager.alert('warning', '请选择一条数据！', 'warning');
		} else {
			var url = 'user/grantRloe.shtml?id=' + row.id;
			showEasyUiWindow('contentFrame', url, 'win', '用户详情', 300, 400);
		}
	});
});

/*** 重载页面 */
function reSearch() {
	$('#accountAscondition').val('');
	$('#nickNameAscondition').val('');
	$('#statusAscondition').combobox('setValue', '');
	doSearch();
}
/*** 搜索 */
function doSearch() {
	$('#dg').datagrid('reload', {
		account : $('#accountAscondition').val(),
		nickName : $('#nickNameAscondition').val(),
		status : $('#statusAscondition').combobox('getValue')
	});
}
/**
 * 将long型转成date型
 * @param value
 * @param row
 * @returns
 */
function formatterDate(value, row) {
	return formatDate(value, 'yyyy-MM-dd hh:mm:ss');
}

