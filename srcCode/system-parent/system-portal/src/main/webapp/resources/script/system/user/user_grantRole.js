$(function(){
	$('#dg').datagrid({    
	    //title:"任务反馈列表",
		border:1,
		nowrap:true,
		striped:true,
		fit: true,//自动大小
		url:'role/queryList',
	    columns : [[
			{field : 'id', title : 'id', hidden:true},
			{field : 'ck', checkbox : true},
	        {field : 'name', title : '角色名称', width : 200}
        ]],
		singleSelect:false,
		rownumbers:true,
		//toolbar:'#tb',
		pagination:false,
		pageSize:9999,
		onClickRow:function(rowIndex, rowData){},
		onLoadSuccess:function(data){
			var _roleIdsStr = $('#roleIdsStr').val();
			var _roleIds = _roleIdsStr.split(",");
//			console.log(_roleIds);
			//console.log(data);
			var rows = data.rows;
			for (var i=0; i < rows.length; i++) {
				var _row = rows[i];
				var _id = _row.id;
				if (_roleIds.indexOf(_id + '') > -1) {
					//console.log(_id);
					$('#dg').datagrid('selectRow', i);
				}
			}
		}
	});
	
	/** 保存 */
	$('#btnSave').click(function(){
		/*$('#form').ajaxSubmit({
			url : 'user/save',
			success : function(data) {
				if (!data.result) {
					$.messager.alert('提醒', data.msg,'info');
				} else {
					$.messager.alert('提醒', data.msg,'info', function() {
						$('#form').form('clear');
						parent.reSearch();
						parent.closeEasyUiWindow('win');
					});
				}
			}
		});*/
		var _roleIds = '';
		var rows = $('#dg').datagrid('getSelections');
		if (rows.length > 0) {
			var data = [];
			for ( var i = 0; i < rows.length; i++) {
				data.push(rows[i].id);
			}
			_roleIds = data.join(',')
		}
		$("body").mask();
		$.post('user/saveUserRoles.shtml',
			{id : $('#id').val(), roleIds : _roleIds},
			function(data){
				$("body").unmask();
				if(data.success == true) {
					$.messager.alert('提醒', '角色修改成功','info', function() {
						parent.closeEasyUiWindow('win');
					});
				} else {
					$.messager.alert('提醒', '请联系管理员','warning');
				}
		});
	});
	
	/** 取消 */
	$('#btnCancel').click(function(){
		/*$('#form').form('clear');
		parent.reSearch();*/
		parent.closeEasyUiWindow('win');
	});
});