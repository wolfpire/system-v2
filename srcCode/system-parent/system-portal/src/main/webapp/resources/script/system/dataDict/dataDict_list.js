$(function(){
	/** 新增 */
	$('#add').click(function(){
		var url = 'dataDict/add.shtml';
		var row = $('#dg').treegrid('getSelected');
		if (row) {
			url = url + '?parentId=' + row.id;
		}
		showEasyUiWindow('contentFrame', url, 'win', '新增数据字典', 350, 300);
		/*$('#win').window('open');*/
	});
	
	/*** 编辑 */
	$('#edit').click(function(){
		var row = $('#dg').treegrid('getSelected');
		if (row) {
			var url = 'dataDict/edit.shtml?id=' + row.id;
			showEasyUiWindow('contentFrame', url, 'win', '编辑数据字典', 350, 300);
		} else {
			$.messager.alert('warning', '请选择一条数据！', 'warning');
		}
	});
	
	/*** 删除 */
	$('#remove').click(function(){
		var row = $('#dg').treegrid('getSelected');
		if (row) {
			$.messager.confirm('确认', '你确认要删除此条数据吗?', function(r) {
				if (r) {
					var id = row.id;				
					$.get('dataDict/delete.shtml', {id : id}, function(rs) {
						if (rs.success == true) {
							reSearch();
							$.messager.alert('warning', rs.message, 'warning');
						} else {
							$.messager.alert('warning', rs.message, 'warning');
						}
					});
				}
			});
		} else {
			$.messager.alert('warning', '请选择一条数据！', 'warning');
		}
	});
});

/**
 * 重载
 */
function reSearch() {
	$('#nameAscondition').val('');
	$('#codeAscondition').val('');
	doSearch();
}

function doSearch() {
	$('#dg').treegrid('load', {
		name : $('#nameAscondition').val(),
		code : $('#codeAscondition').val()
	})
}


