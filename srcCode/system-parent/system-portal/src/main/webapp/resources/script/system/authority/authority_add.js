var _type ='';
var _name_html = '';
var _path_html = '';
$(function(){
	/** 初始化 */
	$('#path_tr').hide();
	_type = $('#type').val();
	if ('1' == _type) {
		_name_html = '导航名称';
		$('#name_html').html(_name_html + '：');
	} else if ('2' == _type) {
		_name_html = '目录名称';
		$('#name_html').html(_name_html + '：');
	}  else if ('3' == _type) {
		_name_html = '菜单名称';
		$('#name_html').html(_name_html + '：');
		_path_html = '路径';
		$('#path_html').html(_path_html + '：');
		$('#path_tr').show();
	}  else if ('4' == _type) {
		_name_html = '操作名称';
		$('#name_html').html(_name_html + '：');
		_path_html = '动作';
		$('#path_html').html(_path_html + '：');
		$('#path_tr').show();
	} 
	
	/** 保存 */
	$('#btnSave').click(function(){
		$('#name').validatebox({
			required : true,
			tipPosition : 'bottom',
			missingMessage : _name_html + '不能为空'
		});
		$('#seq').numberbox({
			required : true,
			min : 0,
			tipPosition : 'bottom',
			missingMessage : '顺序不能空,且大于等于0'
		});
		if ('3' == _type || '4' == _type) {
			$('#path').validatebox({
				required : true,
				tipPosition : 'bottom',
				missingMessage : _path_html + '不能为空'
			});
		}
		var valid = $('#form').form('validate');
		if(valid) {
			$('#form').ajaxSubmit({
				url : 'authority/save',
				success : function(data) {
					if (!data.success) {
						$.messager.alert('提醒', data.message,'info');
					} else {
						$.messager.alert('提醒', data.message,'info', function(){
							$('#form').form('clear');
							parent.reSearch();
							parent.closeEasyUiWindow('win');
						});
					}
				}
			});
		}
	});
	
	/** 取消 */
	$('#btnCancel').click(function(){
		$('#form').form('clear');
		parent.closeEasyUiWindow('win');
	});
});