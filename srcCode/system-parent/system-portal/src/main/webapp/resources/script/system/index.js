$(function(){
	$('#change_pwd').click(function(){
		var url = 'user/changePwd.shtml';
		showEasyUiWindow('win_contentFrame', url, 'win_index', '修改密码', 320, 185);
	});
});
/**
 * 打开tab页
 * @param plugin
 */
function addTab(title, url, menuId){
	url = url.indexOf("?") > -1 ? (url + "&menuId=" + menuId) : (url + "?menuId=" + menuId);
	if ($('#tabs').tabs('exists', title)){
		$('#tabs').tabs('select', title);
	} else {
		var html = '';
		html = '<div class="easyui-panel" data-options="fit:true" style="overflow: hidden;">'
			+ '<iframe scrolling="auto" frameborder="0" style="width: 100%; height: 100%;" src="' + url + '"></iframe>'
			+ '</div>'; 
		$('#tabs').tabs('add',{
			title : title,
			fit : true,
			border : false,
			closable : true,
			content : html
				//'<iframe scrolling="auto" frameborder="0" src="'+url+'" style="width:100%; height:99%;overflow: hidden;"></iframe>'
			
			/*extractor:function(data){
				//data = $.fn.panel.defaults.extractor(data);
				//console.log(data);
				//var tmp = $('<div></div>').html(data);
				//data = tmp.find('#content').html();
				//tmp.remove();
				return data;
			}*/
		});
	}
}

function closeTab(closeTitle,selectTitle){
	$('#tabs').tabs('close', closeTitle);
	var _selectTitle = selectTitle == null || selectTitle=='' ? '主页':selectTitle;
	$('#tabs').tabs('select', _selectTitle);
}
/**
 * 切换iframe显示内容
 * @param mainFrameURL
 */
function mainFrameChange(mainFrameURL){
//	document.getElementById("mainFrame").src=mainFrameURL;
	$("#mainFrame").attr("src", mainFrameURL);
}

/**
 * 退出触发事件
 */
function loginout() {
	$.messager.confirm('确认', '你确定要退出吗？', function(r){
		if (r) {
			window.location.href = contextPath + '/system/loginout';
		}
	});
}

