/**
 * easyui工具类
 * 弹窗函数
 */

/**
 * easyui弹窗函数
 * @param iframe_id
 * @param iframe_url
 * @param win_id
 * @param title
 * @param width
 * @param height
 * @param max
 */
function showEasyUiWindow(iframe_id, iframe_url, win_id, title, width, height, max) {
	$('#' + iframe_id).attr('src', iframe_url);
	$('#' + win_id).window({
		title : title,
		left : ($(window).width() - width) / 2,
		top : ($(window).height() - height) /2, // + $(window).scrollTop()
		width : width,
		height : height,
		onBeforeClose : function() {$('#' + iframe_id).attr('src', '');return true;},
		maximized : max || false
	}).window('open');
}

function closeEasyUiWindow(win_id) {
	$('#' + win_id).window('close');
}