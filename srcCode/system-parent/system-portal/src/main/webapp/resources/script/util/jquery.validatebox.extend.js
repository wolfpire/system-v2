/**
 * 拓展jquery easyui validatebox的校验工具
 */

/**
 * 拓展远程校验
 */
$.extend($.fn.validatebox.defaults.rules, {
    remotes: {
		validator: function(value, param){
			var _url = param[0];
			var params = param[1].split(',');
			var _data = new Object();
			for ( var int = 0; int < params.length; int++) {
				var attr = params[int];
				//console.log(attr);
				_data[params[int]] = $('#' + attr + '').val();
			}
			var _result = $.ajax({url:_url,dataType:"json",data:_data,async:false,cache:false,type:"post"}).responseText;
			return _result;
		},
		message : '{0}已经存在'
    },
    chechbox : {
    	validator: function(value, param){
    		console.log(value);
    		var _type = param[0];
    		if ('1' == _type) {
    			// 单选
    			console.log(value);
    			return false;
    		} else {
				// 可以多选
    			return false;
			}
    	},
    	message : '{0}'
    }
});

