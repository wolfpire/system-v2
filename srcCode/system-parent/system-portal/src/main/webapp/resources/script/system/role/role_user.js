$(function(){
	$('#dg').datagrid({    
	    //title:"任务反馈列表",
		border:1,
		nowrap:true,
		striped:true,
		fit: true,//自动大小
		url:'user/queryList.shtml',
	    columns : [[
			{field : 'id', title : 'id', hidden:true},
			{field : 'ck', checkbox : true},
	        {field : 'account', title : '账号', width : 100},
	        {field : 'nickName', title : '昵称', width : 200}
        ]],
		singleSelect:false,
		rownumbers:true,
		//toolbar:'#tb',
		pagination:false,
		pageSize:99999,
		onClickRow:function(rowIndex, rowData){},
		onLoadSuccess:function(data){
			var _userIdsStr = $('#userIdsStr').val();
			var _userIds = _userIdsStr.split(",");
//			console.log(_userIds);
			//console.log(data);
			var rows = data.rows;
			for (var i=0; i < rows.length; i++) {
				var _row = rows[i];
				var _id = _row.id;
				if (_userIds.indexOf(_id + '') > -1) {
					//console.log(_id);
					$('#dg').datagrid('selectRow', i);
				}
			}
		}
	});
	
	/** 保存 */
	$('#btnSave').click(function(){
		/*$('#form').ajaxSubmit({
			url : 'user/save',
			success : function(data) {
				if (!data.result) {
					$.messager.alert('提醒', data.msg,'info');
				} else {
					$.messager.alert('提醒', data.msg,'info', function() {
						$('#form').form('clear');
						parent.reSearch();
						parent.closeEasyUiWindow('win');
					});
				}
			}
		});*/
		var _userIds = '';
		var rows = $('#dg').datagrid('getSelections');
		if (rows.length > 0) {
			var data = [];
			for ( var i = 0; i < rows.length; i++) {
				data.push(rows[i].id);
			}
			_userIds = data.join(',')
		}
		$("body").mask();
		$.post('role/saveRoleUser.shtml',
			{id : $('#id').val(), userIds : _userIds},
			function(data){
				$("body").unmask();
				if(data.success == true) {
					$.messager.alert('提醒', '关联人员成功','info', function() {
						parent.closeEasyUiWindow('win');
					});
				} else {
					$.messager.alert('提醒', '操作失败,请联系管理员','warning');
				}
		});
	});
	
	/** 取消 */
	$('#btnCancel').click(function(){
		/*$('#form').form('clear');
		parent.reSearch();*/
		parent.closeEasyUiWindow('win');
	});
});