$(function(){
	
	$.extend($.fn.validatebox.defaults.rules, {    
	    equals: {    
	        validator: function(value,param){    
	            return value == $(param[0]).val();    
	        },    
	        message: 'Field do not match.'   
	    }    
	});
	
	$('#btnSave').click(function(){
		var valid = $('#form').form('validate');
		if(valid) {
			$("body").mask();
			$('#form').ajaxSubmit({
				url : 'user/savePwd',
				success : function(data) {
					$("body").unmask();
					if (!data.success) {
						$.messager.alert('提醒', '修改密码失败，请联系管理员','info');
					} else {
						$.messager.alert('提醒', '修改成功','info', function(){
							$('#form').form('clear');
							//parent.reSearch();
							parent.closeEasyUiWindow('win_index');
						});
					}
				}
			});
		}
	});
});