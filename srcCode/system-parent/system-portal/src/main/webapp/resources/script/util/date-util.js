/**
 * 将long型数值转成date格式
 * @param dateStr
 * @param format	yyyy-MM-dd/yyyy-MM-dd HH:mm:ss
 * @returns
 */
function formatDate(dateStr, format) {
	var date = new Date(dateStr);
	var o = {
		"M+" : date.getMonth() + 1, //month  
		"d+" : date.getDate(), //day  
		"h+" : date.getHours(), //hour  
		"m+" : date.getMinutes(), //minute  
		"s+" : date.getSeconds(), //second  
		"q+" : Math.floor((date.getMonth() + 3) / 3), //quarter  
		"S" : date.getMilliseconds()
	//millisecond  
	}
	if (/(y+)/.test(format))
		format = format.replace(RegExp.$1, (date.getFullYear() + "")
				.substr(4 - RegExp.$1.length));
	for ( var k in o) {
		if (new RegExp("(" + k + ")").test(format)) {
			format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
					: ("00" + o[k]).substr(("" + o[k]).length));
		}
	}
	return format;
}

/**
 * 获取当前时间
 * @returns {String}
 */
function getCurrentDateString() {
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth();
	var month_vo = month + 1;
	var day = date.getDate();
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var secounds = date.getSeconds();
	return date + '-' + month_vo + '-' + day + ' ' + hours + ':' + minutes + ':' + secounds;
}
