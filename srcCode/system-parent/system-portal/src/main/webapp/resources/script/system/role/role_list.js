$(function(){
	/** 新增 */
	$('#add').click(function(){
		var url = 'role/add.shtml';
		showEasyUiWindow('contentFrame', url, 'win', '新增角色', 325, 295);
		/*$('#win').window('open');*/
	});
	
	/*** 编辑 */
	$('#edit').click(function(){
		var row = $('#dg').datagrid('getSelected');
		if (row) {
			var url = 'role/edit.shtml?id=' + row.id;
			showEasyUiWindow('contentFrame', url, 'win', '编辑角色', 325, 295);
		} else {
			$.messager.alert('warning', '请选择一条数据！', 'warning');
		}
	});
	
	/*** 删除 */
	$('#remove').click(function(){
		var row = $('#dg').datagrid('getSelected');
		if (row) {
			$.messager.confirm('确认', '你确认要删除此条数据吗?', function(r) {
				if (r) {
					var id = row.id;				
					$.get('role/delete.shtml', {id : id}, function(rs) {
						if (rs.success == true) {
							reSearch();
							$.messager.alert('warning', rs.message, 'warning');
						} else {
							$.messager.alert('warning', rs.message, 'warning');
						}
					});
				}
			});
		} else {
			$.messager.alert('warning', '请选择一条数据！', 'warning');
		}
	});
	
	/** 分配权限 */
	$('#roleAuthority').click(function(){
		var row = $('#dg').datagrid('getSelected');
		if (row) {
			var url = 'role/roleAuthority.shtml?id=' + row.id;
			showEasyUiWindow('contentFrame', url, 'win', '分配权限', 400, 475);
		} else {
			$.messager.alert('warning', '请选择一条数据！', 'warning');
		}
	});
	
	/*** 关联用户 */
	$('#roleUser').click(function(){
		var row = $('#dg').datagrid('getSelected');
		if (row) {
			var url = 'role/roleUser.shtml?id=' + row.id;
			showEasyUiWindow('contentFrame', url, 'win', '关联用户', 325, 475);
		} else {
			$.messager.alert('warning', '请选择一条数据！', 'warning');
		}
	});
});

/**
 * 重载
 */
function reSearch() {
	$('#name').val('');
	doSearch();
}


function doSearch() {
	$('#dg').datagrid('reload', {
		name : $('#name').val()
	});
}