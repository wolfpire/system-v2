$(function(){
	
	/*** 初始化组织树 */
	$('#org_tree').tree({
	    url : 'org/orgs',
	    onLoadSuccess:function(node,data){
	         var t = $(this);
	  		 if(data){
			     $(data).each(function(index,d){
					if(this.state == 'closed'){
					    t.tree('expandAll');
					}
			     });
			}
	    },
	    onClick : function (node) {
	    	//console.log(node);
	    	$('#dg').datagrid('reload', {
	    		parentId : node.id
	    	});
	    }
	});
	
	/** 新增 */
	$('#add').click(function(){
		var url = 'org/add.html';
		/*var row = $('#dg').treegrid('getSelected');
		if (row) {
			url = url + '?parentId=' + row.id;
		}*/
		var node = $('#org_tree').tree('getSelected');
		if (null != node) {
			url = url + '?parentId=' + node.id;
		}
		showEasyUiWindow('contentFrame', url, 'win', '新增组织机构', 350, 300);
		/*$('#win').window('open');*/
	});
	
	/*** 编辑 */
	$('#edit').click(function(){
		var row = $('#dg').datagrid('getSelected');
		if (row) {
			var url = 'org/edit.html?id=' + row.id;
			showEasyUiWindow('contentFrame', url, 'win', '编辑组织机构', 350, 300);
		} else {
			$.messager.alert('warning', '请选择一条数据！', 'warning');
		}
	});
	
	/*** 删除 */
	$('#remove').click(function(){
		var row = $('#dg').treegrid('getSelected');
		if (row) {
			$.messager.confirm('确认', '你确认要删除此条数据吗?', function(r) {
				if (r) {
					var id = row.id;				
					$.get('org/delete.html', {id : id}, function(rs) {
						if (rs.success == true) {
							reSearch();
							$.messager.alert('warning', rs.message, 'warning');
						} else {
							$.messager.alert('warning', rs.message, 'warning');
						}
					});
				}
			});
		} else {
			$.messager.alert('warning', '请选择一条数据！', 'warning');
		}
	});
});
/**
 * 将long型转成date型
 * @param value
 * @param row
 * @returns
 */
function formatterDate(value, row) {
	return formatDate(value, 'yyyy-MM-dd hh:mm:ss');
}

/**
 * 重载
 */
function reSearch(parentId) {
	$('#nameAscondition').val('');
	$('#codeAscondition').val('');	
	doSearch(parentId);
	/*if (!parentId) {
		$('#org_tree').tree('reload');
	}*/
}

/**
 * 搜索
 */
function doSearch(parentId) {
	$('#dg').datagrid('reload', {
		name : $('#nameAscondition').val(),
		code : $('#codeAscondition').val()/*,
		parentId : parentId || ''*/
	});
}


