$(function(){
//	console.log('---------------------->>>edit user');
//	console.log($('#status').val());
	$('#status_span').html(formatUserStatus($('#status').val()));
	var _createTime = $('#createTime').val();
	_createTime = _createTime.substring(0, _createTime.length - 2);
	$('#create_time_span').html(_createTime);
	$('#createTime').val(_createTime);
	/**角色下拉框初始化*/
	$('#roleIds').combobox({
        url:'role/roles',
        method:'get',
        valueField:'id',
        textField:'name',
        panelHeight:'150',
        multiple:true
    });
	$('#roleIds').combobox('setValues',$('#roleIdsStr').val().split(','));
	/**组织初始化*/
	$('#orgId').combotree({
		url : 'org/orgs',
		panelHeight:'150',
		onLoadSuccess : function (node, data) {
			 var t = $(this);
	  		 if(data){
			     $(data).each(function(index,d){
					if(this.state == 'closed'){
					    t.tree('expandAll');
					}
			     });
			}
		}
	});
	$('#oId_clear_btn').click(function(){
		$('#orgId').combotree('clear');
	});
	//console.log($('#roleIdsStr').val());
	/** 保存 */
	$('#btnSave').click(function(){
		$('#account').validatebox({
			required : true,
			missingMessage : '账号不能为空'
		});
		/*$('#password').validatebox({
			required : true,
			missingMessage : '密码不能为空'
		});*/
		$('#email').validatebox({
			required : true,
			validType : 'email',
			missingMessage : '邮箱不能为空'
		});
		var valid = $('#form').form('validate');
		if(valid) {
			$('#form').ajaxSubmit({
				url : 'user/save',
				success : function(data) {
					if (!data.result) {
						$.messager.alert('提醒', data.msg,'info');
					} else {
						$.messager.alert('提醒', data.msg,'info', function() {
							$('#form').form('clear');
							parent.reSearch();
							parent.closeEasyUiWindow('win');
						});
					}
				}
			});
		}
	});
	
	/** 取消 */
	$('#btnCancel').click(function(){
		$('#form').form('clear');
		parent.reSearch();
		parent.closeEasyUiWindow('win');
	});
});