$(function(){
	$('#dg').treegrid({    
	    //title:"任务反馈列表",
		border:1,
		nowrap:true,
		striped:true,
		fit: true,//自动大小
		autoRowHeight: false,
		url:'authority/queryList.shtml',
		idField:'id',    
	    treeField:'name',
	    animate:true,
	    columns : [[
			{field : 'id', title : 'id', hidden:true},
			{field : 'ck', checkbox : true},
	        {field : 'name', title : '权限名称', width : 200},
	        {field : 'type', title : '类型', width : 80, 
	        	formatter : function(value, row, index){
	        		return formatAuthorityType(value, row)
	        	}
	        }
        ]],
		singleSelect:false,
		rownumbers:true,
		//toolbar:'#tb',
		pagination:false,
		pageSize:9999,
		onClickRow:function(rowIndex, rowData){},
		onLoadSuccess:function(row, data){
			var _authorityIdsStr = $('#authorityIdsStr').val();
			var _authorityIds = _authorityIdsStr.split(",");
			var rows = data.rows;
			for (var i=0; i < rows.length; i++) {
				var _row = rows[i];
				var _id = _row.id;
				if (_authorityIds.indexOf(_id + '') > -1) {
					$('#dg').treegrid('select', _id);
				}
				var _parentId = _row._parentId;
				if (null == _parentId){
					$('#dg').treegrid('expand', _id);
				}
			}
		}
	});
	
	/** 保存 */
	$('#btnSave').click(function(){
		/*$('#form').ajaxSubmit({
			url : 'user/save',
			success : function(data) {
				if (!data.result) {
					$.messager.alert('提醒', data.msg,'info');
				} else {
					$.messager.alert('提醒', data.msg,'info', function() {
						$('#form').form('clear');
						parent.reSearch();
						parent.closeEasyUiWindow('win');
					});
				}
			}
		});*/
		var _authorityIds = '';
		var rows = $('#dg').treegrid('getSelections');
		if (rows.length > 0) {
			var data = [];
			for ( var i = 0; i < rows.length; i++) {
				data.push(rows[i].id);
			}
			_authorityIds = data.join(',')
		}
		$("body").mask();
		$.post('role/saveRoleAuthority.shtml',
			{id : $('#id').val(), authorityIds : _authorityIds},
			function(data){
				$("body").unmask();
				if(data.success == true) {
					$.messager.alert('提醒', '分配权限成功','info', function() {
						parent.closeEasyUiWindow('win');
					});
				} else {
					$.messager.alert('提醒', '操作失败,请联系管理员','warning');
				}
		});
	});
	
	/** 取消 */
	$('#btnCancel').click(function(){
		/*$('#form').form('clear');
		parent.reSearch();*/
		parent.closeEasyUiWindow('win');
	});
});