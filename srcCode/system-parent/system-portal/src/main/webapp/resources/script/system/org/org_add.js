$(function(){
	$('#parentId').combotree({
		url : 'org/orgs',
		onLoadSuccess : function (node, data) {
			 var t = $(this);
	  		 if(data){
			     $(data).each(function(index,d){
					if(this.state == 'closed'){
					    t.tree('expandAll');
						//t.combotree('tree').tree("expandAll");
					}
			     });
			}
		}
	});
	//$('#parentId').combotree('setValue', $('#old_parentId').val());
	$('#pId_clear_btn').click(function(){
		$('#parentId').combotree('clear');
	});
	
	/** 保存 */
	$('#btnSave').click(function(){
		$('#name').validatebox({
			required : true,
			tipPosition : 'bottom',
			missingMessage : '组织名称不能为空'
		});
		$('#code').validatebox({
			required : true,
			tipPosition : 'bottom',
			missingMessage : '组织编码不能为空'
		});
		$('#seq').numberbox({
			required : true,
			min : 0,
			tipPosition : 'bottom',
			missingMessage : '顺序不能空,且大于等于0'
		});
		var valid = $('#form').form('validate');
		if(valid) {
			$('#form').ajaxSubmit({
				url : 'org/save',
				success : function(data) {
					if (!data.success) {
						$.messager.alert('提醒', data.message,'info');
					} else {
						$.messager.alert('提醒', data.message,'info', function(){
							parent.reSearch($('#parentId').val());
							$('#form').form('clear');
							parent.closeEasyUiWindow('win');
						});
					}
				}
			});
		}
	});
	
	/** 取消 */
	$('#btnCancel').click(function(){
		$('#form').form('clear');
		parent.closeEasyUiWindow('win');
	});
});