$(function(){
	$('#parentId').combotree({
		url : 'dataDict/dataDicts.shtml',
		onLoadSuccess : function (node, data) {
			 var t = $(this);
	  		 if(data){
			     $(data).each(function(index,d){
					if(this.state == 'closed'){
					    t.tree('expandAll');
					}
			     });
			}
		}
	});
	//$('#parentId').combotree('setValue', $('#old_parentId').val());
	$('#pId_clear_btn').click(function(){
		$('#parentId').combotree('clear');
	});
	
	/** 保存 */
	$('#btnSave').click(function(){
		$('#name').validatebox({
			required : true,
			tipPosition : 'bottom',
			missingMessage : '名称不能为空'
		});
		$('#code').validatebox({
			required : true,
			tipPosition : 'bottom',
			missingMessage : '编码不能为空'
		});
		$('#seq').numberbox({
			required : true,
			min : 0,
			tipPosition : 'bottom',
			missingMessage : '顺序不能空,且大于等于0'
		});
		var valid = $('#form').form('validate');
		if(valid) {
			$('#form').ajaxSubmit({
				url : 'dataDict/save.shtml',
				success : function(data) {
					if (!data.success) {
						$.messager.alert('提醒', data.message,'info');
					} else {
						$.messager.alert('提醒', data.message,'info', function(){
							$('#form').form('clear');
							parent.reSearch();
							parent.closeEasyUiWindow('win');
						});
					}
				}
			});
		}
	});
	
	/** 取消 */
	$('#btnCancel').click(function(){
		$('#form').form('clear');
		parent.closeEasyUiWindow('win');
	});
});