/**
 * 格式化用户状态
 * @param value
 * @param row
 * @returns {String}
 */
function formatUserStatus(value, row) {
	if (value == 0) {
		return "未激活";
	} else if (value == 1) {
		return "已激活"
	} else if (value == -1) {
		return "禁用";
	}
}

/**
 * 格式化权限类型
 * @param value
 * @param row
 * @returns {String}
 */
function formatAuthorityType(value, row) {
	var result = "";
	switch (value) {
	case 1:
		result ="导航";
		break;
	case 2:
		result ="目录";
		break;
	case 3:
		result ="菜单";
		break;
	case 4:
		result ="操作";
		break;
	default:
		break;
	}
	return result;
}

/**
 * 格式化任务状态
 * @param value
 * @param row
 * @returns {String}
 */
function formatTaskStatus(value, row) {
	if (value == 0) {
		return "申请中";
	} else if (value == 1) {
		return "同意"
	} else if (value == -1) {
		return "拒绝";
	}
}

/**
 * 格式化任务时间段
 * @param value
 * @param row
 */
function formatTaskTime(value, row) {
	return row.startTime + " ~ " + row.endTime;
}

function formatTaskTimeAdv(value, row) {
	return '【开始时间】' + (row.startTime).split(" ")[0] + '<br/>【结束时间】' + (row.endTime).split(" ")[0];
}

/**
 * 格式化title
 * @param value
 * @param row
 */
function formatListRowTitle(value, row, maxlength) {
	var text = value;
	if (value.length > maxlength) {
		text = value.substring(0, maxlength) + '...';
	}
	return '<a javascript:"void(0)" alt="' + value + '">' + text +'</a>';
}

/**
 * 格式化workflow状态
 * @param value
 * @param row
 */
function formatWorkflowStatus(value, row) {
	if (value == 0) {
		return "删除";
	} else if (value == 1) {
		return "正常"
	} else if (value == 2) {
		return "挂起";
	}
}

/**
 * 格式化任务流程时间
 * @param value
 * @param row
 */
function formatTaskWfTime(value, row) {
	return value + "~" + row.end_time;
}

/**
 * 格式化任务流程时间
 * @param value
 * @param row
 */
function formatTaskWfTimeAdv(value, row) {
	return '【开始时间】' + value + '<br/>【结束时间】' + row.end_time;
}

/**
 * 格式化时间
 * @param value
 * @param row
 */
function formatTaskWfDate (value, row) {
	var start_date = (value.split(" "))[0];
	var end_date = (row.end_time.split(" "))[0];
	return '【开始时间】' + start_date + '<br/>【结束时间】' + end_date;
}

/**
 * 格式化流程实例状态
 * @param value
 * @param row
 * @returns
 */
function formatWfStatus(value, row) {
	if ("1" == value) {
		return row.cur_node;
	} else if ("2" == value) {
		return "审批结束";
	}
}



/**
 * 任务文件
 * @param value
 * @param row
 * @returns {String}
 */
function formatTaskAtta(value, row) {
	/*if ("task" == value) {
		return "预佐证材料";
	} else {
		return "任务反馈材料";
	}*/
	if ("task" == value) {
		return "预佐证材料";
	} else if ("feedBack_funds" == value || 'projectIndex_funds' == value) {
		return "资金类佐证材料(任务反馈)";
	} else if ("feedBack_result" == value || 'projectIndex_result' == value) {
		return "成果类佐证材料(任务反馈)";
	}
}

/**
 * 格式化资金来源
 * @param value
 * @returns {String}
 */
function formatTaskFundSources(value) {
	if(null == value || '' == value) {
		return '';
	}
	var str = '';
	var val = value.split(',');
	for(var i=0; i<val.length; i++) {
		if ('1' ==val[i]) {
			str = str + "," + '省级财政专项';
		} else if ('2' ==val[i]) {
			str = str + "," + '市县（区）财政资金';
		} else if ('3' ==val[i]) {
			str = str + "," + '行业企业资金';
		} else if ('4' ==val[i]) {
			str = str + "," + '院校自筹资金';
		}
	}
	str = str.substring(1, str.length);
	return str;
}

/**
 * 格式化任务反馈审核状态
 * @param value
 * @returns {String}
 */
function formateAuditStatus(value) {
	if ('1' == value) {
		return '待审核';
	} else if ('2' == value) {
		return '审核不通过';
	} else if ('3' == value) {
		return '审核通过';
	} else {
		return '未提交';
	}
}

/**
 * 格式化资金
 * @param value
 */
function formateAmount(value) {
	if (null == value || '' == value || undefined == value || 'undefined' == value) {
		return '￥  0.00';
	}
	return '￥ ' + value;
}

/**
 * 格式化进度
 * @param value
 */
function formateProgress(value) {
	if (null == value || '' == value || undefined == value || 'undefined' == value) {
		return '0.00 %';
	}
	return value + ' %';
}

function formateTaskStatusAdv(value, row) {
	if (value == 0) {
		var wf_status = row.wf_status;
		if ("1" == wf_status) {
			return row.cur_node + "<br/>（申请中）";
		} else if ("2" == wf_status) {
			return "审批结束";
		} else {
			return "申请中";
		}
	} else if (value == 1) {
		return "同意<br/>（审批结束）"
	} else if (value == -1) {
		return "拒绝<br/>（审批结束）";
	} else if (value == 2) {
		var wf_status = row.wf_status;
		if ("1" == wf_status) {
			return row.cur_node + "<br/>（审批驳回）";
		} else if ("2" == wf_status) {
			return "审批结束";
		} else {
			return "驳回<br/>（申请中）";
		}
	} else if (value == 4) {
		return "作废";
	}
}

function displayTaskActor(value) {
	return displayString(value, 12);
}

/**
 * 字符串显示
 * @param value
 * @param length
 * @returns
 */
function displayString(value, length) {
	if (value == '' || value == null || value == undefined || value == 'undefined') {
		return '';
	}
	if (length == '' || length == null || length == undefined || length == 'undefined') {
		return value;
	}
	if (value.length <= length) {
		return value;
	} else {
		return '<span title="' + value + '">' + value.substring(0, length) + '...</span>';
	}
}

/**
 * 格式化完成状态
 * @param value
 * @returns {String}
 */
function formateFinishStatus(value) {
	if ('1' == value) {
		return '已完成';
	} else if ('0' == value) {
		return '未完成';
	} 
}

/**
 * 将1，0转换成是否
 * @param value
 * @returns {String}
 */
function formateBoolean(value) {
	if (value == '1') {
		return '是';
	} else {
		return '否';
	}
}

/**
 * 获取逗号前后内容
 * @param value
 */
function getCommaValue(value, index) {
	if (value == '' || value == null || value == undefined || value == 'undefined') {
		return '';
	}
	var rs = value.split(',');
	return rs[index];
}

/***************************************************/
function formatterPreProjectNums(value, row, index) {
	return getCommaValue(row.project_nums, 0);
}
function formatterSuffProjectNums(value, row, index) {
	return getCommaValue(row.project_nums, 1);
}

function formatterPreProfession(value, row, index) {
	var nums =  getCommaValue(row.profession_nums, 0);
	var percent = getCommaValue(row.profession_percents, 0);
	return nums + "/" + percent;
}
function formatterSuffProfession(value, row, index) {
	var nums =  getCommaValue(row.profession_nums, 1);
	var percent = getCommaValue(row.profession_percents, 1);
	return nums + "/" + percent;
}

function formatterPreStudent(value, row, index) {
	var nums =  getCommaValue(row.student_nums, 0);
	var percent = getCommaValue(row.student_percents, 0);
	return nums + "/" + percent;
}
function formatterSuffStudent(value, row, index) {
	var nums =  getCommaValue(row.student_nums, 1);
	var percent = getCommaValue(row.student_percents, 1);
	return nums + "/" + percent;
}

function formatterPreCourseNums(value, row, index) {
	return getCommaValue(row.course_nums, 0);
}
function formatterSuffCourseNums(value, row, index) {
	return getCommaValue(row.course_nums, 1);
}

function formatterPreTextbookNums(value, row, index) {
	return getCommaValue(row.textbook_nums, 0);
}
function formatterSuffTextbookNums(value, row, index) {
	return getCommaValue(row.textbook_nums, 1);
}

function formatterPreTwoStudentNums(value, row, index) {
	return getCommaValue(row.twoway_student_nums, 0);
}
function formatterSuffTwoStudentNums(value, row, index) {
	return getCommaValue(row.twoway_student_nums, 1);
}

function formatterPreTwoTeacherNums(value, row, index) {
	return getCommaValue(row.twoway_teacher_nums, 0);
}
function formatterSuffTwoTeacherNums(value, row, index) {
	return getCommaValue(row.twoway_teacher_nums, 1);
}

function formatterPreExpertNums(value, row, index) {
	return getCommaValue(row.expert_nums, 0);
}
function formatterSuffTwoExpertNums(value, row, index) {
	return getCommaValue(row.expert_nums, 1);
}

function formatterPreCertifyNums(value, row, index) {
	return getCommaValue(row.certify_nums, 0);
}
function formatterSuffCertifyNums(value, row, index) {
	return getCommaValue(row.certify_nums, 1);
}

/***************************************************/
