$(function(){
	/** 新增 */
	$('#add_nav').click(function(){
		var url = 'authority/add.html?type=1';
		showEasyUiWindow('contentFrame', url, 'win', '新增导航', 450, 300);
	});
	$('#add_dir').click(function(){
		var url = 'authority/add.html?type=2';
		var row = $('#dg').treegrid('getSelected');
		if (row) {
			var type = row.type;
			if (type == '1') {
				url = url + '&parentId=' + row.id;
				showEasyUiWindow('contentFrame', url, 'win', '新增目录', 450, 300);
			} else {
				$.messager.alert('warning', '请先选择导航！', 'warning');
			}
		} else {
			$.messager.alert('warning', '请选择一条数据！', 'warning');
		}
		
	});
	$('#add_menu').click(function(){
		var url = 'authority/add.html?type=3';
		var row = $('#dg').treegrid('getSelected');
		if (row) {
			var type = row.type;
			if (type == '2') {
				url = url + '&parentId=' + row.id;
				showEasyUiWindow('contentFrame', url, 'win', '新增菜单', 450, 300);
			} else {
				$.messager.alert('warning', '请先选择目录！', 'warning');
			}
		} else {
			$.messager.alert('warning', '请选择一条数据！', 'warning');
		}
	});
	$('#add_operator').click(function(){
		var url = 'authority/add.html?type=4';
		var row = $('#dg').treegrid('getSelected');
		if (row) {
			url = url + '&parentId=' + row.id;
			showEasyUiWindow('contentFrame', url, 'win', '新增操作', 450, 300);
		} else {
			$.messager.alert('warning', '请选择一条数据！', 'warning');
		}
	});
	
	/*** 编辑 */
	$('#edit').click(function(){
		var row = $('#dg').treegrid('getSelected');
		if (row) {
			var url = 'authority/edit.html?id=' + row.id;
			showEasyUiWindow('contentFrame', url, 'win', '编辑权限信息', 450, 300);
		} else {
			$.messager.alert('warning', '请选择一条数据！', 'warning');
		}
	});
	
	/*** 删除 */
	$('#remove').click(function(){
		var row = $('#dg').treegrid('getSelected');
		if (row) {
			$.messager.confirm('确认', '你确认要删除此条数据以及其下级权限吗?', function(r) {
				if (r) {
					var id = row.id;				
					$.get('authority/delete.html', {id : id}, function(rs) {
						if (rs.success == true) {
							reSearch();
							$.messager.alert('warning', rs.message, 'warning');
						} else {
							$.messager.alert('warning', rs.message, 'warning');
						}
					});
				}
			});
		} else {
			$.messager.alert('warning', '请选择一条数据！', 'warning');
		}
	});
});

/**
 * 重载
 */
function reSearch() {
	$('#name').val('');
	doSearch();
}
/** 搜索 */
function doSearch() {
	$('#dg').treegrid('reload', {
		name : $('#name').val()
	});
}

/**
 * 全部展开
 * 测试过，太耗性能了
 */
function expendTree() {
	 $('#dg').treegrid('expandAll');
}