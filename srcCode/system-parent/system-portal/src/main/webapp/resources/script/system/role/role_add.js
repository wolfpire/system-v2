$(function(){
	/** 初始化权限 */
	$('#authorityIds').combotree({
		url : 'authority/authoritys',
		cascadeCheck: false,
		multiple: true,
		panelHeight:'180',
		onLoadSuccess : function (node, data) {
			 var t = $(this);
	  		 if(data){
			     $(data).each(function(index,d){
					if(this.state == 'closed'){
					    t.tree('expandAll');
						//t.combotree('tree').tree("expandAll");
					}
			     });
			}
		}
	});
	var _authorityIdsStr = $('#authorityIdsStr');
	if ($('#authorityIdsStr')) {
		var _authorityIdsStr = $('#authorityIdsStr').val();
		//console.log(_authorityIdsStr);
		if (!(null == _authorityIdsStr || undefined == _authorityIdsStr || '' == _authorityIdsStr || 'undefined' == _authorityIdsStr)) {
			var id_arr = _authorityIdsStr.split(',');
			//console.log(id_arr);
			$('#authorityIds').combotree('setValues', id_arr);
		}
		
	}
	/** 保存 */
	$('#btnSave').click(function(){
		var _url = 'role/isUnique?id=' + $('#id').val() + '&name=' + $('#name').val();
		$('#name').validatebox({
			required : true,
			tipPosition : 'bottom',
			remote : _url,
			missingMessage : '角色名称不能为空',
			invalidMessage : '角色名称已存在'
		});
		var valid = $('#form').form('validate');
		if(valid) {
			$('#form').ajaxSubmit({
				url : 'role/save',
				success : function(data) {
					if (!data.result) {
						$.messager.alert('提醒', data.msg,'info');
					} else {
						$.messager.alert('提醒', data.msg,'info', function(){
							$('#form').form('clear');
							parent.reSearch();
							parent.closeEasyUiWindow('win');
						});
					}
				}
			});
		}
	});
	
	/** 取消 */
	$('#btnCancel').click(function(){
		$('#form').form('clear');
		parent.closeEasyUiWindow('win');
	});
});