$(function(){
	$('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
	$(window).resize(function(){  
		$('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
    });
 	$('#logining').click(function() {
 		$('#account').validatebox({required: true, missingMessage:'输入用户名'});  
 		$('#password').validatebox({required: true, missingMessage:'输入密码'});
 		var r = $('#loginform').form('validate'); 
 		if (r) {
 			$('#loginform').ajaxSubmit({
				url : contextPath + '/j_spring_security_check',
				success : function(result) {
					if (!result.success) {
						var html = '&nbsp; &nbsp;&nbsp; &nbsp;<font color="red">' + result.message +'</font>';
						$('#msg').html(html);
					} else {
						location.href = contextPath + result.data;
					}
				}
			});
 		} 
 	});
 	// 回车键登录效果
 	$("body").keydown(function(){if(event.keyCode == 13) {$("#logining").click();}});
});


