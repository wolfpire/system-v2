<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!doctype html>
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>权限管理</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	
	<jsp:include page="/WEB-INF/views/common.jsp"></jsp:include>
	<script type="text/javascript" src="<%=path%>/resources/script/system/authority/authority_list.js"></script>
  </head>
  
  <body>
  	
  	<!-- datagrid -->
	<table id="dg" class="easyui-treegrid" title="权限列表" style="width: auto; height: auto;"
    	data-options="url:'authority/authoritysTree.shtml',
    			idField:'id',
    			treeField:'name',
    			fit:true,
    			autoRowHeight:false,
    			singleSelect:true,
    			rownumbers:true,
    			toolbar:'#tb',
    			animate:true">
 		<thead>
 			<tr>
 				<th data-options="field:'id',hidden:true">权限ID</th>
 				<th data-options="field:'parentId',hidden:true">父级权限ID</th>
    			<th data-options="field:'name', width:300">权限名称</th>
    			<th data-options="field:'type', align:'center', width:125, formatter:formatAuthorityType">类型</th>
    			<th data-options="field:'seq', align:'center', width:100">顺序</th>
    			<th data-options="field:'path', width:300">路径/动作 </th>
    			<th data-options="field:'createTime', align:'center', width:150">创建时间</th>
 			</tr>
 		</thead>
	</table>
	
	<!-- toolbar -->
    <div id="tb" style="padding: 5px; height: auto;">
    	<!-- search -->
    	<!-- <div>
    		<table>
    			<tr>
    				<td>名称：<input class="easyui-validatebox edit-input" id="name" name="name" style="width: 150px;" /></td>
    				<td><a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-search" onclick="doSearch();">查询</a></td>
    				<td><a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-reload" onclick="reSearch();">重置</a></td>
    			</tr>
    		</table>
    	</div> -->
    	<!-- opreate -->
    	<div style="margin-bottom: 5px; text-align: right;">
    		<%@include file="/WEB-INF/views/common/operate.jsp" %>
    		<!-- <a href="javascript:void(0);" id="add_nav" class="easyui-linkbutton" title="添加导航" iconCls="icon-add" plain="true">添加导航</a>
    		<a href="javascript:void(0);" id="add_dir" class="easyui-linkbutton" title="添加目录" iconCls="icon-add" plain="true">添加目录</a>
    		<a href="javascript:void(0);" id="add_menu" class="easyui-linkbutton" title="添加菜单" iconCls="icon-add" plain="true">添加菜单</a>
    		<a href="javascript:void(0);" id="add_operator" class="easyui-linkbutton" title="添加操作" iconCls="icon-add" plain="true">添加操作</a>
    		<a href="javascript:void(0);" id="edit" class="easyui-linkbutton" title="修改" iconCls="icon-edit" plain="true">修改</a>
    		<a href="javascript:void(0);" id="remove" class="easyui-linkbutton" title="删除" iconCls="icon-remove" plain="true">删除</a> -->
    	</div>
    </div>
    
    <div id="win" class="easyui-window"
    	 data-options="modal:true,
    	 				closed:true,
    	 				maximizable:false,
    	 				minimizable:false,
    	 				collapsible:false,
    	 				iconCls:'icon-save',
    	 				draggable:true">
    	 <iframe id="contentFrame" src="" width=100% height=99% marginwidth=0 framespacing=0 marginheight=0 frameborder=no scrolling=no></iframe>
    </div>
  	
  </body>
</html>
