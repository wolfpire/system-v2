<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>修改密码</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<jsp:include page="/WEB-INF/views/common.jsp"></jsp:include>
	<script type="text/javascript" src="<%=path%>/resources/script/system/user/user_pwd.js"></script>
  </head>
  
  <body>
    <div class="easyui-layout" fit="true">
   		<div region="center" border="false" style="padding: 10px; background: #fff; border: 1px solid #ccc;">
		<form id="form" method="post">
			<table>
				<tr>
					<td>旧密码：</td>
					<td><input id="originPwd" name="originPwd" type="password" class="easyui-validatebox" required="true" missingMessage="旧密码不能为空" validtype="remote['user/isCorrectPwd','originPwd']" invalidMessage="旧密码错误" tipPosition="bottom"  /></td>
				</tr>
				<tr>
					<td>新密码：</td>
					<td><input id="newPwd" name="newPwd" type="password" required="true" missingMessage="新密码不能为空"  class="easyui-validatebox" tipPosition="bottom" /></td>
				</tr>
				<tr>
					<td>确认密码:</td>
					<td><input id="reNewPwd" name="reNewPwd" type="password" validType="equals['#newPwd']" required="true" missingMessage="确认密码不能为空" invalidMessage="新密码输入不一致" tipPosition="bottom" class="easyui-validatebox" /></td>
				</tr>
			</table> 
		</form>
   		</div>
   		<div region="south" border="false" style="text-align: right; height: 35px; line-height: 8px; padding:5px 5px 0 0;">
   			<a id="btnSave" class="easyui-linkbutton" icon="icon-ok" href="javascript:void(0);">确定</a>
   			<a id="btnCancel" class="easyui-linkbutton" icon="icon-cancel" href="javascript:void(0);">取消</a>
   		</div>
   	</div>
  </body>
</html>
