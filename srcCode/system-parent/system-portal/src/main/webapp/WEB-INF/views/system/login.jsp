<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!doctype html>
<html>
<head>
<base href="<%=basePath%>">

<title>项目库跟踪管理系统——登录</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">

<script type="text/javascript">
if (window != top) {
	top.location.href = location.href;
}
</script>
<link href="<%=path %>/resources/css/newlogin/basic.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/resources/css/newlogin/cloud.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/resources/css/newlogin/login.css" rel="stylesheet" type="text/css" />

<jsp:include page="/WEB-INF/views/common.jsp"></jsp:include>
<script type="text/javascript" src="<%=path%>/resources/script/system/cloud.js"></script>
<script type="text/javascript" src="<%=path%>/resources/script/system/login.js"></script>

</head>

<body
	style="background-color:#1c77ac; background-image:url(./resources/images/newlogin/light.png); background-repeat:no-repeat; background-position:center top; overflow:hidden;">



	<div id="mainBody">
		<div id="cloud1" class="cloud"></div>
		<div id="cloud2" class="cloud"></div>
	</div>

	<div class="loginbody">
		<span class="systemlogo"></span>
		<form id="loginform" name="loginform" method="post">
			<div class="loginbox">
				<ul>
					<li><input id="account" name="j_username" type="text" class="loginuser" value="" />
					</li>
					<li><input id="password" name="j_password" type="password" class="loginpwd" value=""/>
					</li>
					<li>
						<span id="msg"></span>
						<a href="javascript:void(0);" id="logining" class="loginbtn"><img src="<%=path %>/resources/images/system/bnt5_meitu_1.png" /></a>
						<!-- <input name="" type="button" class="loginbtn" value="登录"/> -->
					</li>
				</ul>
			</div>
		</form>
	</div>

</body>
</html>
