<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!doctype html>
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>编辑用户</title>
    
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<jsp:include page="/WEB-INF/views/common.jsp"></jsp:include>
	<script type="text/javascript" src="<%=path%>/resources/script/system/user/user_edit.js"></script>

  </head>
  
  <body>
    
    <div class="easyui-layout" fit="true">
   		<div region="center" border="false" style="padding: 10px; background: #fff; border: 1px solid #ccc;">
		<form id="form" method="post">
			<input id="id" name="id" type="hidden" value="${user.id }" />
			<input id="roleIdsStr" name="roleIdsStr" type="hidden" value="${user.roleIdsStr }" />
			<%-- <input id="orgIdHidden" name="orgIdHidden" type="hidden" value="${user.orgId }" /> --%>
			<input type="hidden" id="password" name="password" value="${user.password }" />
			<table class="edit-table">
				<tr>
					<th>账号：</th>
					<td><input id="account" name="account" value="${user.account }" class="easyui-validatebox edit-input" style="width: 250px;" /></td>
				</tr>
				<tr>
					<th>昵称：</th>
					<td><input id="nickName" name="nickName" value="${user.nickName }" class="easyui-validatebox edit-input" style="width: 250px;" /></td>
				</tr>
				<tr>
					<th>邮箱：</th>
					<td><input id="email" name="email" value="${user.email}" class="easyui-validatebox edit-input" style="width: 250px;" /></td>
				</tr>
				<tr>
					<th>所属组织：</th>
					<td>
						<input id="orgId" name="orgId" value="${user.orgId }" class="easyui-validatebox" style="width: 200px;" />
						<input id="oId_clear_btn" name="oId_clear_btn" type="button" value="清空" style="width: 40px;"/>
					</td>
				</tr>
				<tr>
					<th>所属角色：</th>
					<td><input id="roleIds" name="roleIds" class="easyui-validatebox" style="width: 250px;"  /></td>
				</tr>
				<tr>
					<th>状态：</th>
					<td>
						<input id="status" name="status" value="${user.status}" type="hidden" />
						<span id="status_span"></span>
					</td>
				</tr>
				<tr>
					<th>创建时间：</th>
					<td>
						<input id="createTime" name="createTime" value="${user.createTime }" type="hidden" />
						<span id="create_time_span"></span>
					</td>
				</tr>
				<tr>
					<th>描述:</th>
					<td><textarea id="remark" name="remark" class="easyui-validatebox" style="width: 300px;height: 100px;">${user.remark }</textarea></td>
				</tr>
			</table> 
		</form>
   		</div>
   		<div region="south" border="false" style="text-align: right; height: 35px; line-height: 8px; padding:5px 5px 0 0;">
   			<a id="btnSave" class="easyui-linkbutton" icon="icon-ok" href="javascript:void(0);">确定</a>
   			<a id="btnCancel" class="easyui-linkbutton" icon="icon-cancel" href="javascript:void(0);">取消</a>
   		</div>
   	</div>
    
  </body>
</html>
