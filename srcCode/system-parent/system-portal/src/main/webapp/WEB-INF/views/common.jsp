<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>
<link type="text/css" rel="stylesheet" href="<%=path%>/resources/script/jquery/easyui/themes/default/easyui.css" />
<link type="text/css" rel="stylesheet" href="<%=path%>/resources/script/jquery/easyui/themes/icon.css" />
<link type="text/css" rel="stylesheet" href="<%=path%>/resources/script/jquery/easyui/portal/portal.css" />
<link type="text/css" rel="stylesheet" href="<%=path%>/resources/css/system/form.css" />
<link rel="stylesheet" type="text/css" href="<%=path%>/resources/script/swfupload/css/swfupload.css" />


<script type="text/javascript">
var contextPath = "<%=path%>";
</script> 

<script type="text/javascript" src="<%=path%>/resources/script/jquery/jquery-1.11.3.min.js"></script>
<!-- 这里缺省cookie.js -->
<script type="text/javascript" src="<%=path%>/resources/script/jquery/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/resources/script/jquery/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="<%=path%>/resources/script/jquery/easyui/portal/jquery.portal.js"></script>
<script type="text/javascript" src="<%=path%>/resources/script/jquery/easyui/jquery.easyui.patch.js"></script>

<!-- easyui工具类 -->
<script type="text/javascript" src="<%=path%>/resources/script/util/easyui-util.js"></script>
<!-- 自定义easyui验证工具 -->
<script type="text/javascript" src="<%=path%>/resources/script/util/jquery.validatebox.extend.js"></script>
<!-- 这里缺省datagrid-detailview.js,jquery.edatagrid.js, -->
<script type="text/javascript" src="<%=path%>/resources/script/jquery/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="<%=path%>/resources/script/jquery/validation/localization/messages_zh.min.js"></script>
<script type="text/javascript" src="<%=path%>/resources/script/jquery/form/jquery.form.js"></script>
<!-- js时间 工具类 -->
<script type="text/javascript" src="<%=path%>/resources/script/util/date-util.js"></script>
<!-- 系统公用工具类 -->
<script type="text/javascript" src="<%=path%>/resources/script/system/system-util.js"></script>
<!-- 日历控件 -->
<script type="text/javascript" src="<%=path%>/resources/script/My97DatePicker/WdatePicker.js"></script>
<!-- 报表控件 -->
<script src="<%=path %>/resources/script/echarts/echarts.min.js"></script>

<!-- 上传控件 -->
<script type="text/javascript" src="<%=path%>/resources/script/swfupload/swfupload.js"></script>
<script type="text/javascript" src="<%=path%>/resources/script/swfupload/handlers.js"></script>

<!-- 遮罩层控件 -->
<script type="text/javascript" src="<%=path%>/resources/script/jquery/loadmask/jquery.loadmask.min.js"></script>
