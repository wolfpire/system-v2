<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!doctype html">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>编辑权限信息</title>
    
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	    
	<jsp:include page="/WEB-INF/views/common.jsp"></jsp:include>
	<script type="text/javascript" src="<%=path%>/resources/script/system/authority/authority_add.js"></script>

  </head>
  
  <body>
	
	<div class="easyui-layout" fit="true">
   		<div region="center" border="false" style="padding: 10px; background: #fff; border: 1px solid #ccc;">
			<form id="form" method="post">
				<input id="id" name="id" type="hidden" value="${authority.id }" />
				<input id="parentId" name="parentId" type="hidden" value="${authority.parentId }">
				<input id="type" name="type" type="hidden" value="${authority.type }">
				<table>
					<tr>
						<td id="name_html"></td>
						<td><input id="name" name="name" value="${authority.name }" class="easyui-validatebox" style="width: 300px;" /></td>
					</tr>
					<tr id="path_tr">
						<td id="path_html"></td>
						<td><input id="path" name="path" value="${authority.path }" class="easyui-validatebox" style="width: 300px;" /></td>
					</tr>
					<tr>
						<td>顺序：</td>
						<td><input id="seq" name="seq" value="${authority.seq }" class="easyui-numberbox" style="width: 300px;" /></td>
					</tr>
					<tr>
						<td>图标：</td>
						<td><input id="image" name="image" value="${authority.image }" class="easyui-validatebox" style="width: 300px;" /></td>
					</tr>
				</table> 
			</form>
   		</div>
   		<div region="south" border="false" style="text-align: right; height: 35px; line-height: 8px; padding:5px 5px 0 0;">
   			<a id="btnSave" class="easyui-linkbutton" icon="icon-ok" href="javascript:void(0);">确定</a>
   			<a id="btnCancel" class="easyui-linkbutton" icon="icon-cancel" href="javascript:void(0);">取消</a>
   		</div>
   	</div>
	
  </body>
</html>
