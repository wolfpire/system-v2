<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:forEach var="oper" items="${operAuthorities}">
	<a href="javascript:void(0);" id="${oper.path}"
		class="easyui-linkbutton" title="${oper.name }"
		<c:choose>
			<c:when test="${not empty oper.image }">
				iconCls="${oper.image}"
			</c:when>
			<c:otherwise>
				iconCls="icon-${oper.path}"
			</c:otherwise>
		</c:choose>
		plain="true">${oper.name}</a>
</c:forEach>

		