<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!doctype html>
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>角色管理</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<jsp:include page="/WEB-INF/views/common.jsp"></jsp:include>
	<script type="text/javascript" src="<%=path%>/resources/script/system/role/role_list.js"></script>

  </head>
  
  <body>
    
    <!-- DataGrid -->
    <table id="dg" class="easyui-datagrid" title="角色列表" style="width: auto; height: auto;"
    	data-options="url:'role/queryList',
    			fit:true,
    			singleSelect:true,
    			rownumbers:true,
    			toolbar:'#tb',
    			pagination:true">
    	<thead>
    		<tr>
    			<th data-options="field:'id',hidden:true">角色ID</th>
    			<th data-options="field:'name', width:150">名称</th>
    			<th data-options="field:'remark', width:200">描述</th>
    			<th data-options="field:'createTime', width:200">创建时间</th>
    		</tr>
    	</thead>
    </table>
    
    <!-- toolbar -->
    <div id="tb" style="padding: 5px; height: auto;">
    	<!-- search -->
    	<div>
    		<table>
    			<tr>
    				<td>角色名称：<input class="easyui-validatebox" id="name" name="name" style="width: 150px;" /></td>
    				<td><a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-search" onclick="doSearch();">查询</a></td>
    				<td><a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-reload" onclick="reSearch();">重置</a></td>
    			</tr>
    		</table>
    	</div>
    	<!-- opreate -->
    	<div style="margin-bottom: 5px; text-align: right;">
    		<%@include file="/WEB-INF/views/common/operate.jsp" %>
    		
    		<!-- <a href="javascript:void(0);" id="add" class="easyui-linkbutton" title="添加" iconCls="icon-add" plain="true">添加</a>
    		<a href="javascript:void(0);" id="edit" class="easyui-linkbutton" title="修改" iconCls="icon-edit" plain="true">修改</a>
    		<a href="javascript:void(0);" id="remove" class="easyui-linkbutton" title="删除" iconCls="icon-remove" plain="true">删除</a>
    		<a href="javascript:void(0);" id="permissions" class="easyui-linkbutton" title="分配权限" iconCls="icon-edit" plain="true">分配权限</a>
    		<a href="javascript:void(0);" id="roleAuthority" class="easyui-linkbutton" title="分配权限" iconCls="icon-edit" plain="true">分配权限</a>
    		<a href="javascript:void(0);" id="roleUser" class="easyui-linkbutton" title="分配权限" iconCls="icon-edit" plain="true">关联用户</a> -->
    	</div>
    </div>
    
    <!-- add/edit window -->
    <div id="win" class="easyui-window"
    	 data-options="modal:true,
    	 				closed:true,
    	 				maximizable:true,
    	 				minimizable:false,
    	 				collapsible:false,
    	 				iconCls:'icon-save',
    	 				draggable:false">
    	 <iframe id="contentFrame" src="" width=100% height=99% marginwidth=0 framespacing=0 marginheight=0 frameborder=no scrolling=no></iframe>
    </div>
  </body>
</html>
