<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!doctype html>
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>编辑数据字典</title>
    
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	
	<jsp:include page="/WEB-INF/views/common.jsp"></jsp:include>
	<script type="text/javascript" src="<%=path%>/resources/script/system/dataDict/dataDict_add.js"></script>
  </head>
  
  <body>

	
	<div class="easyui-layout" fit="true">
		<div region="center" border="false" style="padding: 10px; background: #fff; border: 1px solid #ccc;">
			<form id="form" method="post">
				<input id="id" name="id" type="hidden" value="${dataDict.id }" />
				<table>
					<tr>
						<td>名称：</td>
						<td><input id="name" name="name" class="easyui-validatebox" value="${dataDict.name }" style="width: 200px;" /></td>
					</tr>
					<tr>
						<td>编码：</td>
						<td><input id="code" name="code" class="easyui-validatebox" value="${dataDict.code }" style="width: 200px;" /></td>
					</tr>
					<tr>
						<td>父级：</td>
						<td>
							<input id="parentId" name="parentId" value="${dataDict.parentId }" class="easyui-validatebox" style="width: 150px;" />
							<input id="pId_clear_btn" name="pId_clear_btn" type="button" value="清空" style="width: 50px;"/>
						</td>
					</tr>
					<tr>
						<td>顺序：</td>
						<td><input id="seq" name="seq" value="${dataDict.seq }" class="easyui-numberbox" style="width: 200px;" /></td>
					</tr>
					<tr>
						<td>描述:</td>
						<td><textarea id="remark" name="remark" class="easyui-validatebox" style="width: 200px;height: 80px;">${dataDict.remark }</textarea></td>
					</tr>
				</table> 
			</form>
		</div>
		<div region="south" border="false" style="text-align: right; height: 35px; line-height: 8px; padding:5px 5px 0 0;">
   			<a id="btnSave" class="easyui-linkbutton" icon="icon-ok" href="javascript:void(0);">确定</a>
   			<a id="btnCancel" class="easyui-linkbutton" icon="icon-cancel" href="javascript:void(0);">取消</a>
   		</div>
	</div>
	
  </body>
</html>
