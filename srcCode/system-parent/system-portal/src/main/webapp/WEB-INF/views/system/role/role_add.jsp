<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!doctype html>
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>新增角色</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	
	<jsp:include page="/WEB-INF/views/common.jsp"></jsp:include>
	<script type="text/javascript" src="<%=path%>/resources/script/system/role/role_add.js"></script>

  </head>
  
  <body>
    
    <div class="easyui-layout" fit="true">
   		<div region="center" border="false" style="padding: 10px; background: #fff; border: 1px solid #ccc;">
		<form id="form" method="post">
			<input id="id" name="id" type="hidden" />
			<table>
				<tr>
					<td>角色名称：</td>
					<td><input id="name" name="name" class="easyui-validatebox" required="true" missingMessage="角色名称不能为空" validtype="remote['role/isUnique','name']" invalidMessage="角色名称已存在" tipPosition="bottom" style="width: 200px;" /></td>
				</tr>
				<tr>
					<td>分配权限：</td>
					<td><input id="authorityIds" name="authorityIds" class="easyui-validatebox" style="width: 200px;" /></td>
				</tr>
				<tr>
					<td>描述:</td>
					<td><textarea id="remark" name="remark" class="easyui-validatebox" style="width: 200px;height: 120px;"></textarea></td>
				</tr>
			</table> 
		</form>
   		</div>
   		<div region="south" border="false" style="text-align: right; height: 35px; line-height: 8px; padding:5px 5px 0 0;">
   			<a id="btnSave" class="easyui-linkbutton" icon="icon-ok" href="javascript:void(0);">确定</a>
   			<a id="btnCancel" class="easyui-linkbutton" icon="icon-cancel" href="javascript:void(0);">取消</a>
   		</div>
   	</div>
    
  </body>
</html>
