<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!doctype html>
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>用户管理</title>
    
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	
	<jsp:include page="/WEB-INF/views/common.jsp"></jsp:include>
	<script type="text/javascript" src="<%=path%>/resources/script/system/user/user.js"></script>
	
  </head>
  
  <body class="easyui-layout" fit="true">
    <div data-options="region:'west',split:true" title="组织树" style="width:250px;">
		<ul id="org_tree" class="easyui-tree"></ul>
	</div>
	<div data-options="region:'center'">
	    <!-- DataGrid -->
	    <table id="dg" class="easyui-datagrid" title="用户列表" style="width: auto; height: auto;"
	    	data-options="url:'user/queryList',
	    			fit:true,
	    			singleSelect:true,
	    			rownumbers:true,
	    			toolbar:'#tb',
	    			pagination:true">
	    	<thead>
	    		<tr>
	    			<th data-options="field:'id',hidden:true">用户ID</th>
	    			<th data-options="field:'account', width:150">账号</th>
	    			<th data-options="field:'nickName', width:150">昵称</th>
	    			<th data-options="field:'email', width:200">邮箱</th>
	    			<th data-options="field:'remark', width:200">描述</th>
	    			<th data-options="field:'status', width:100, hidden:true, formatter:formatUserStatus">状态</th>
	    			<th data-options="field:'createTime', width:150">创建时间</th>
	    		</tr>
	    	</thead>
	    </table>
	    
	    <!-- toolbar -->
	    <div id="tb" style="padding: 5px; height: auto;">
	    	<!-- search -->
	    	<div>
	    		<table>
	    			<tr>
	    				<td>账号：<input class="easyui-validatebox edit-input" id="accountAscondition" name="account" style="width: 150px;" /></td>
	    				<td></td>
	    				<td>昵称：<input class="easyui-validatebox edit-input" id="nickNameAscondition" name="nickName" style="width: 150px;" /></td>
	    				<td style="display: none;">
	    					<select class="easyui-combobox" id="statusAscondition" name="status" panelHeight="auto">
	    						<option value="">全部</option>
	    						<option value="1" selected="selected">已激活</option>
	    						<option value="0">未激活</option>
	    						<option value="-1">禁用</option>
	    					</select>
	    				</td>
	    				<td><a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-search" onclick="doSearch();">查询</a></td>
	    				<td><a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-reload" onclick="reSearch();">重置</a></td>
	    			</tr>
	    		</table>
	    	</div>
	    	<!-- opreate -->
	    	<div style="margin-bottom: 5px; text-align: right;">
	    		<%@include file="/WEB-INF/views/common/operate.jsp" %>
	    		<!-- <a href="javascript:void(0);" id="add" class="easyui-linkbutton" title="添加" iconCls="icon-add" plain="true">添加</a>
	    		<a href="javascript:void(0);" id="edit" class="easyui-linkbutton" title="修改" iconCls="icon-edit" plain="true">修改</a>
	    		<a href="javascript:void(0);" id="remove" class="easyui-linkbutton" title="删除" iconCls="icon-remove" plain="true">删除</a>
	    		<a href="javascript:void(0);" id="grantRole" class="easyui-linkbutton" title="添加" iconCls="icon-edit" plain="true">赋予角色</a> -->
	    	</div>
	    </div>
    </div>
    <!-- add/edit window -->
    <div id="win" class="easyui-window"
    	 data-options="modal:true,
    	 				closed:true,
    	 				maximizable:true,
    	 				minimizable:false,
    	 				collapsible:false,
    	 				iconCls:'icon-save',
    	 				draggable:false">
    	 <iframe id="contentFrame" src="" width=100% height=99% marginwidth=0 framespacing=0 marginheight=0 frameborder=no scrolling=no></iframe>
    </div>
  </body>
</html>
