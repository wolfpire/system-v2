<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!doctype html>
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>项目库跟踪管理系统平台——首页</title>
    
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link href="<%=path %>/resources/css/system/base.css" rel="stylesheet" type="text/css" />	
	<link href="<%=path %>/resources/css/system/index.css" rel="stylesheet" type="text/css" />
	<jsp:include page="/WEB-INF/views/common.jsp"></jsp:include>
	<script type="text/javascript" src="<%=path%>/resources/script/system/index.js"></script>
<style type="text/css">
#myindex .panel {
	margin : 5px;
	float: left;
}
</style>
  </head>
  
  <body class="easyui-layout" style="width: 100%; height: 100%">
  	
  	<div data-options="region:'north'" border="false" style="overflow: hidden; height: 69px;">
  		<div class="header">
  			<span class="fl"><img src="<%=path %>/resources/images/system/logo_2.png"></span>
  			<div class="yhm fr">
  				<span class="name">
  					<img class="mr10" src="<%=path %>/resources/images/system/user_min.png" />
  					${loginUser.nickName}（${loginUser.account }）<br/>
  				</span>
  				<span>|</span><span><a class="change_pwd" href="javascript:void(0);" id="change_pwd">修改密码</a></span>
  				<span>|</span><span><a href="javascript:void(0);" onclick="loginout();">退出</a></span>
  			</div>
  		</div>
  	</div>
  	
  	<div data-options="region:'west',split:false" title="导航菜单" style="width:200px;">
  		<div id="leftmenu" class="easyui-accordion" data-options="fit:true,border:false">
  			<c:forEach items="${authorities }" var="au">
  				<div title="${au.name }">
	  				<ul>
	  					<c:forEach var="a" items="${au.children }">
	  						<li style='margin:5px 0px 5px 0px;font-size:13px;'>
		  						<a href="javascript:void(0);" onclick="addTab('${a.name}','${a.path }','${a.id }')" class="menu-links">
									<img src="<%=path %>${a.image}" width=32 height=32 class="menu-img">
									<span class="menu-span-text">${a.name}</span>
								</a>
		  					</li>
	  					</c:forEach>
		  			</ul>
	  			</div>
  			</c:forEach>
  		</div>
  	</div>
  	
  	<div data-options="region:'center'" border="false">
  		<div id="tabs" class="easyui-tabs" data-options="fit:true,border:false">
            <div title="主页" style="padding:10px;overflow:auto;">
            <div id="myindex">
            	<div class="easyui-panel" style="width:20%; height:500px; overflow:visible; border: 0px;">
            		<div id="p1" class="easyui-panel" title="个人工作"     
				        style="width:98%;height:240px; overflow:visible;margin: 0px !important; padding: 0 !important;">  
				     	<iframe src="personInfo/todoCount.shtml" frameborder="0" style="width: 100%; height: 100%;"></iframe> 
					</div>
					<div id="p8" class="easyui-panel" title="日历"     
					        style="width:98%; height:240px; overflow:visible;">  
					    <div id="cc" class="easyui-calendar" style="width:100%;height:100%;"></div> 
					</div>
            	</div>
				<div class="easyui-panel" style="width:38%; height:500px; overflow:visible; border: 0px;">
					<div id="p4" class="easyui-panel" title="任务分布图"     
					        style="width:98%;height:490px;padding:10px; overflow:visible; ">
					     <iframe src="bireport/taskCount.shtml?portal=index" frameborder="0" style="width: 100%; height: 100%;"></iframe>   
					</div>
				</div>
				<div class="easyui-panel" style="width:38%; height:500px; overflow:visible; border: 0px;">
					<div id="p6" class="easyui-panel" title="资金投入/使用情况"     
					        style="width:98%;height:490px;padding:10px;overflow:visible;">
						<iframe src="bireport/amountUse.shtml?portal=index" frameborder="0" style="width: 100%; height: 100%;"></iframe>
					</div>
				</div>
			</div>
            </div>
        </div>
  		<div id="win_index" class="easyui-window"
	    	 data-options="modal:true,
	    	 				closed:true,
	    	 				maximizable:false,
	    	 				minimizable:false,
	    	 				collapsible:false,
	    	 				iconCls:'icon-save',
	    	 				draggable:false">
	    	 <iframe id="win_contentFrame" src="" width=100% height=99% marginwidth=0 framespacing=0 marginheight=0 frameborder=no scrolling=no></iframe>
	    </div>
  	</div>
  	
  </body> 
<script type="text/javascript">
 /* var panels = [
    {id:'p1',title:'个人待办',height:250,collapsible:true},
    {id:'p2',title:'公告',height:250,collapsible:true},
    {id:'p3',title:'项目增加月表',height:250,collapsible:true,closable:true,href:'bireport/taskInc.html'},
    {id:'p4',title:'任务分布图',height:250,collapsible:true,closable:true,href:'bireport/taskCount.html'},
    {id:'p5',title:'资金使用情况',height:250,collapsible:true,closable:true,href:'bireport/amountUse.html'},
    {id:'p6',title:'资金投入比例',height:250,collapsible:true,closable:true,href:'bireport/amountInput.html'}
]; */
</script>  
</html>
