<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<input type="hidden" id="deploymentId" name="deploymentId" value="${deploymentId}">
<input type="hidden" id="nodeId" name="nodeId"> 
<input type="hidden" id="selectedNodeValue" name="selectedNodeValue" value=""> 
<input type="hidden" id="selectedNodeDynamicSelect" name="selectedNodeDynamicSelect" value=""> 
<input type="hidden" id="selectedNodenNodeType" name="selectedNodenNodeType" value=""> 
<input type="hidden" id="sendSms_value" name="sendSms_value" value="0"> 
<div style="padding-top:7px;padding-left:60px;background-color:#F2F7FB; border-bottom:1px solid #bed3df; text-align: left;">
	<div id="tb_a_operate" style="height: 23px;">
		<span id="freeJumpSpan" style="display: none;">
			<input type='checkbox' id='freeJumpCheck' name='freeJumpCheck' class="toolbar-check" />自由跳转&nbsp;&nbsp;
		</span>
		<span id="sendSpan">
			<%--<input type='checkbox' id='sendMail' name='sendMail' class="toolbar-check" style="display: none;"/>发送邮件&nbsp;&nbsp;--%>
			<input type="hidden" name="sendMail" id="sendMail" value="true"/>
		</span>
		
	</div>
</div>
<!--流程有关-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="edit-table">
	<tr class="item" id="tr_epath">
		<th class="itemtit"><span style="color:red;">*</span>下一环节：</th>
		<td>
			<c:forEach var="node" items="${taskNodes}">
				<input type="radio" id="nextNode" name="nextNode" 
					nodeName="${node.nodeName}" nodeType="${node.nodeType}" 
					dynamicSelect="${node.dynamicSelect}" nodeId="${node.nodeId}" 
					value="${node.nodeName}">${node.nodeName}&nbsp;&nbsp;</input>
			</c:forEach>
		</td>
	</tr>
	
	 <tr id="userDiv" style="display: none;"> 
	    <th>选择办理人：</th>
	    <td>
			<input type="hidden" name="userIds" id="userIds"/>
			<input type="text" name="userNames" id="userNames" class="edit-input" readonly="readonly" style="width: 250px !important;"/>
			<a id="userDiv_a" href="javascript:selectUser('userIds', 'userNames', 'task');" class="easyui-linkbutton" data-options="iconCls:'icon-man'">选择办理人</a>
	    </td>
   	</tr>
   	
   	<tr class="item" id="advicePath">
		<th class="itemtit">办理意见：</th>
		<td class="border_b" colspan="5">
			<textarea rows="5" cols="80" style="width:500px !important;height: 80px !important;" id="advice" name="advice"
				 class="easyui-validatebox must-input" onpropertychange="if(value.length>500) value=value.substr(0,500)"></textarea></td>
	</tr>
</table>
<%@include file="/WEB-INF/views/common/select_user.jsp"%>

<script type="text/javascript">

$("#sendSms_").change(function(){
	if($(this).attr("checked")){
		$("#sendSms_value").val('1');
	} else {
		$("#sendSms_value").val('0');
	}
});

//是否发送短信
/*
$("#sendMail").change(function(){
	if($(this).attr("checked")){
		$("#sendMail").val(true);
	} else {
		$("#sendMail").val(false);
	}
});
*/

var radioCount = $('input[name=nextNode]').length;
if (radioCount == 1) {
	$("#nextNode").attr('checked',true);
	$('#userIds').val('');
	$('#userNames').val('');
	
	$('#selectedNodeValue').val('');
	$('#selectedNodeDynamicSelect').val('');
	$('#selectedNodenNodeType').val('');
	
	$('#userDiv').hide();
	$('#nodeId').val($("#nextNode").attr('nodeId'));
	
	$('#selectedNodeValue').val($("#nextNode").val());
	$('#selectedNodeDynamicSelect').val($("#nextNode").attr('dynamicSelect'));
	$('#selectedNodenNodeType').val($("#nextNode").attr('nodeType'));
	//console.log('----------------------------->>>' + $("#nextNode").attr('nodeType'));
	if($("#nextNode").attr('nodeType') == '5'){
		
	} else{
		var dynamicSelect = $("#nextNode").attr('dynamicSelect');
		//人员初始化
		$.ajax({ 
			type: "POST",
			//url: "node.shtml?method=getNodeActorUsers",
			url: "node/getNodeActorUsers.shtml",
			data:{
				nodeId:$('#nodeId').val()
			},
			success: function(result){
				//console.log(result);
				if(result.result=='success'){
					
					if($("#nextNode").attr('nodeType') == '3'){
						$('#userDiv').hide();
					}else{
						$('#userIds').val(result.userIds);
						$('#userNames').val(result.userNames);
						$('#userIds').attr("start","0");
						$('#userDiv').show();
						//alert('dynamicSelect=='+);
						if(dynamicSelect == '1'){
							
						}else{
							$('#userDiv_a').hide();
						}
					}
				} else {
					alert('人员初始化错误');
				}
			}
		});
	}
}



//执行路径判断是否需要动态选择人员
//$("input[name=nextNode]").click(function(){
	 
//$("#tr_epath > td input[type='radio']").click(function(){
$('input[name=nextNode]').click(function(){

	$('#userIds').val('');
	$('#userNames').val('');
	
	$('#selectedNodeValue').val('');
	$('#selectedNodeDynamicSelect').val('');
	$('#selectedNodenNodeType').val('');
	
	$('#userDiv').hide();
	$('#nodeId').val($(this).attr('nodeId'));
	
	$('#selectedNodeValue').val($(this).val());
	$('#selectedNodeDynamicSelect').val($(this).attr('dynamicSelect'));
	$('#selectedNodenNodeType').val($(this).attr('nodeType'));
	//console.log('---------------------->>>' + $(this).attr('nodeType'));
	if($(this).attr('nodeType') == '5'){

	} else if($(this).attr('nodeType') == '3'){
		$('#userDiv').hide();
	} else{
		var dynamicSelect = $(this).attr('dynamicSelect');
		//人员初始化
		$.ajax({ 
			type: "POST",
			//url: "node.shtml?method=getNodeActorUsers",
			url: "node/getNodeActorUsers.shtml",
			data:{
				nodeId:$('#nodeId').val()
			},
			success: function(result){

				if(result.result=='success'){
					
					if($(this).attr('nodeType') == '3'){
						$('#userDiv').hide();
					}else{
						$('#userIds').val(result.userIds);
						$('#userNames').val(result.userNames);
						$('#userIds').attr("start","0");
						$('#userDiv').show();
						//alert('dynamicSelect=='+);
						if(dynamicSelect == '1'){
							
						}else{
							$('#userDiv_a').hide();
						}
					}
				} else {
					alert('人员初始化错误');
				}
			}
		});
		/*
		var isCostConfirm = $("#isCostConfirm").val();
		if(isCostConfirm=="1"&&$(this).attr('nodeType') == '3'){
			$("#costConfirmPath1").show();
			$("#costConfirmPath2").show();
		}else if(isCostConfirm=="1"){
			$("#costConfirmPath1").hide();
			$("#costConfirmPath2").hide();
		}*/
	}

});


function dynamicSelect(_nodeName){
	$('#userIds').attr("value",'');
    $('#userNames').val('');
	$('#userDiv').hide();
	if(_nodeName!=""){
		$.ajax({ 
			type: "POST",
			url: "<%=path%>/zzgl/flowService/getNode.jhtml",
			data:{
				deploymentId:$("#deploymentId").val(),
				nodeName:_nodeName
			},
			dataType:"json",
			success: function(data){
				
				if(data.dynamicSelect == '1'){
					$('#userDiv').show();
					$('#nodeId').val(data.nodeId);
				}
			}
		});
	}
}


function operateEvent(){
	//判断是否分支
	var nodeType = $('#selectedNodenNodeType').val();
	if(nodeType=='5'){
		//字符串格式  143021-184,185`143023-184,186,187`143024-184
	
		var forkUserIds = '';
		$("#forkPath > td input[type='checkbox']").each(function(i, obj){
			if($(this).attr('checked')){
				
				forkUserIds+=obj.id + '-'+$("#userIds"+obj.id).val()+'`'
			}
		});

		$("#forkUserIds").val(forkUserIds.substring(0, forkUserIds.length-1));
	}
}


function __validate(callback){
	
	var validate =true;

    if($("#freeJumpCheck").attr("checked")){
//    	freeJumpCheck = "checked";
		var temp= $("#userDiv").is(":visible");//是否可见
		if(temp){
			var userIds = $("#userIds").val();
	    	if(userIds==null||userIds==""){
	    		$.messager.alert('提示-->跳转路径',"请选择办理人员!");
	            validate = false;
	    	}
		}else{
			var jumpTask= $('#jumpTask').combobox('getValue');
			if(jumpTask==""){
				$.messager.alert('提示-->跳转路径',"请选择跳转路径!");
	            validate = false;
			}
		}
    }else{
    	//执行路径
    	var execPath=$("#selectedNodeValue").val();
    	if(execPath==null||execPath==""){
            $.messager.alert('提示-->下一环节',"请选择下一环节!");
            validate = false;
        }else if($("#selectedNodenNodeType").val()=='5'){

        	var flag = "0";
        	
        	$("#forkPath > td input[type='checkbox']").each(
                function() {
                	var a=$(this).attr("checked");
                	if(a=="checked"){
                		flag = "1";
	        			var id = $("#userIds"+$(this).attr('id')).val();
	        			if(id==null||id==""){
	        				$.messager.alert('提示-->分支路径',"勾选的分支路径未完全配置办理人员!");
	        				validate = false;
	        			}
                	}
                 }
             );
        	
        	if(flag=="0"&&validate){
        		$.messager.alert('提示-->分支路径',"请勾选分支路径!");
        		validate = false;
        	}
        }else if($("#selectedNodenNodeType").val()=='1'&&
        		$("#selectedNodeDynamicSelect").val()=='1'){
        	var userIds = $("#userIds").val();
        	if(userIds==null||userIds==""){
        		$.messager.alert('提示-->下一环节',"请选择办理人员!");
	            validate = false;
        	}
        }else if($("#selectedNodenNodeType").val()=='2'){
        	$('#decisionName').attr('decisionId',"1");
        	if($("#selectedNodeDynamicSelect").val()=='1'){
        		var userIds = $("#userIds").val();
            	if(userIds==null||userIds==""){
            		$.messager.alert('提示-->决策路径',"请选择办理人员!");
    	            validate = false;
            	}
        	}
        }
    }
    callback(validate);
}




</script>
