/**
 * 
 */
package com.wolfpire.system.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.wolfpire.system.common.AuthorityUtils;
import com.wolfpire.system.common.EasyuiPage;
import com.wolfpire.system.common.EasyuiPagination;
import com.wolfpire.system.common.MD5;
import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.Result;
import com.wolfpire.system.common.springmvc.PrincipalArgument;
import com.wolfpire.system.common.util.StringUtils;
import com.wolfpire.system.model.Authority;
import com.wolfpire.system.model.User;
import com.wolfpire.system.service.UserService;

/**
 * 用户控制器
 * @author lihd
 *
 */

@Controller
@RequestMapping("/user")
public class UserController {
	
	@Resource UserService userService;
	
	/*@RequestMapping("/list")
	public ModelAndView list(@ModelAttribute(Constants.SESSION_USER) User user) {
//		ModelAndView mv = new ModelAndView("system/user/user_list");
//		User filterUser = new User();
//		filterUser.setDelFlag(Constants.NORMAL_FLAG);
//		List<User> users = this.userService.list(filterUser);
//		mv.addObject("users", users);
		return new ModelAndView("system/user/user_list");
	}*/
	
	@RequestMapping("/list")
	public ModelAndView list(@PrincipalArgument User user, @RequestParam Long menuId) {
		//获取对应菜单的操作权限
//		List<Authority> operAuthorities = getAperAuthorities(user, menuId);
		List<Authority> operAuthorities = AuthorityUtils.getAperAuthorities(user, menuId);
		return new ModelAndView("system/user/user_list", "operAuthorities", operAuthorities);
	}
	
	@ResponseBody
	@RequestMapping("/queryList")
	public EasyuiPagination<User> queryList(EasyuiPage<User> page, User filterUser) {
		Page<User> users = this.userService.setPageDataList(page, filterUser);
		return new EasyuiPagination<User>(users);
	}
	
	@ResponseBody
	@RequestMapping("/save")
	public Map<String, Object> save(User newUser) {
		Map<String, Object> map = new HashMap<String, Object>();
		boolean result = false;
		String msg = null;
		// 判断添加的用户是否存在
		User targetUser = this.userService.getByAccount(newUser.getAccount(), newUser.getPassword());
		if (null == newUser.getId()) {
			// 新增
			if (null == targetUser) {
				this.userService.saveOrUpdate(newUser);
				result = true;
				msg = "新增成功";
			} else {
				result = true;
				msg = "此用户名及密码已经存在,请更换";
			}
		} else {
			// 更新
			this.userService.saveOrUpdate(newUser);
			result = true;
			msg = "更新成功";
		}
		map.put("result", result);
		map.put("msg", msg);
		return map;
	}
	
	@RequestMapping(value = "/add")
	public ModelAndView add(HttpSession session, @RequestParam(value = "orgId", required = false) String orgId) {
		ModelAndView mv = new ModelAndView("system/user/user_add");
		mv.getModelMap().put("orgId", StringUtils.isBlank(orgId) ? StringUtils.EMPTY : orgId);
		return mv;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(Long id) {
		ModelAndView mv = new ModelAndView("system/user/user_edit");
		if (null != id) {
//			User user = this.userService.get(id);
			User user = this.userService.getCompleteUser(id);
			mv.getModelMap().put("user", user);
		}
		return mv;
	}
	
	@RequestMapping("/detail")
	public ModelAndView detail(Long id) {
		ModelAndView mv = new ModelAndView("system/user/user_detail");
		if (null != id) {
//			User user = this.userService.get(id);
			User user = this.userService.getCompleteUser(id);
			mv.getModelMap().put("user", user);
		}
		return mv;
	}
	
	@ResponseBody
	@RequestMapping("/delete")
	public Result delete(Long id) {
		try {
			this.userService.delete(id);
			return new Result(true, "删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败");
		}
	}
	
	@RequestMapping("/changePwd")
	public ModelAndView changePwd() {
		return new ModelAndView("system/user/user_pwd");
	}
	
	@ResponseBody
	@RequestMapping(value = "/isCorrectPwd", method = {RequestMethod.GET, RequestMethod.POST})
	public Boolean isCorrectPwd(@PrincipalArgument User user, @RequestParam(value = "originPwd") String originPwd) {
		try {
			MD5 md5 = new MD5();
			String md5Pwd = md5.getMD5ofStr(originPwd);
			return (user.getPassword()).equals(md5Pwd);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/savePwd", method = {RequestMethod.GET, RequestMethod.POST})
	public Result savePwd(@PrincipalArgument User user, @RequestParam(value="newPwd") String newPwd) {
		try {
			MD5 md5 = new MD5();
			String md5Pwd = md5.getMD5ofStr(newPwd);
			user.setPassword(md5Pwd);
			this.userService.saveUserPassword(user.getId(), newPwd);
			return Result.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return Result.FAILURE;
		}
	}
	
	@RequestMapping("/grantRloe")
	public ModelAndView grantRloe(Long id) {
		ModelAndView mv = new ModelAndView("system/user/user_grantRole");
		if (null != id) {
//			User user = this.userService.get(id);
			User user = this.userService.getCompleteUser(id);
			mv.getModelMap().put("user", user);
		}
		return mv;
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveUserRoles", method = {RequestMethod.GET, RequestMethod.POST})
	public Result saveUserRoles(@RequestParam(value = "id") Long id,
			@RequestParam(value="roleIds", required = false) String roleIds) {
		try {
			this.userService.grantUserRoles(id, roleIds);
			return Result.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return Result.FAILURE;
		}
	}
}
