/**
 * 
 */
package com.wolfpire.system.common.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wolfpire.system.common.Result;

/**
 * @author lihd
 *
 */
public class MyAuthenticationFailureHandler implements
		AuthenticationFailureHandler {

	@Override
	public void onAuthenticationFailure(HttpServletRequest reqeust,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		String message = exception.getMessage();
		if(exception instanceof BadCredentialsException){
			message = "密码错误";
		}
				
		Result result = new Result(false, message, null);
		
		ObjectMapper mapper = new ObjectMapper();
		
		response.setContentType("application/json;charset=utf-8");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		
		mapper.writeValue(response.getWriter(), result);
		

	}

}
