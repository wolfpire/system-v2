/**
 * 
 */
package com.wolfpire.system.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.wolfpire.system.common.Constants;
import com.wolfpire.system.model.User;
import com.wolfpire.system.service.UserService;

/**
 * 登录控制器
 * @author lihd
 *
 */

@Controller
public class LoginController {
	
	@Resource UserService userService;
	
	@RequestMapping("/system/login")
	public ModelAndView login(User loginUser) {
		/**
		 * 跳转至控制器
		 * 如果想直接跳转到index页面，可以
		 * String url = "system/index";
		 * 如果想跳转至控制器用forward:/system/main
		 * 
		 */
		String url = null;
		ModelAndView view = new ModelAndView();
		loginUser = this.userService.getByAccount(loginUser.getAccount(), loginUser.getPassword());
		if (null == loginUser) {
			url = "system/login";
			view.addObject("msg", "账户或密码错误");
		} else {
			url = "redirect:/system/index";
//			view.getModelMap().addAttribute(loginUser);
			view.getModelMap().put(Constants.SESSION_USER, loginUser);
		}
		view.setViewName(url);
		return view;
	}
	
	@RequestMapping("/system/loginout")
	public ModelAndView loginout(@ModelAttribute(Constants.SESSION_USER)User loginUser) {
		ModelAndView view = new ModelAndView("system/login");
		loginUser = new User();
		view.addObject(loginUser);
		return view;
	}
}
