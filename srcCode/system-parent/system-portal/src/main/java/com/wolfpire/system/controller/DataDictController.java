/**
 * 
 */
package com.wolfpire.system.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.wolfpire.system.common.AuthorityUtils;
import com.wolfpire.system.common.EasyuiPage;
import com.wolfpire.system.common.EasyuiPagination;
import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.Result;
import com.wolfpire.system.common.springmvc.PrincipalArgument;
import com.wolfpire.system.model.Authority;
import com.wolfpire.system.model.DataDict;
import com.wolfpire.system.model.User;
import com.wolfpire.system.service.DataDictService;

/**
 * 数据字典控制器
 * @author lihd
 *
 */

@Controller
@RequestMapping("/dataDict")
public class DataDictController {
	
	@Resource private DataDictService dataDictService;
	
	@RequestMapping("/list")
	public ModelAndView list(@PrincipalArgument User user, @RequestParam Long menuId) {
		//获取对应菜单的操作权限
//		List<Authority> operAuthorities = getAperAuthorities(user, menuId);
		List<Authority> operAuthorities = AuthorityUtils.getAperAuthorities(user, menuId);
		return new ModelAndView("system/dataDict/dataDict_list", "operAuthorities", operAuthorities);
	}
	
	@ResponseBody
	@RequestMapping("/queryList")
	public EasyuiPagination<DataDict> queryList(EasyuiPage<DataDict> page, DataDict dataDict) {
		if (null != dataDict && null != dataDict.getId()) {
			dataDict.setParentId(dataDict.getId());
			dataDict.setId(null);
		}
		Page<DataDict> dataDicts = this.dataDictService.setPageDataList(page, dataDict);
		return new EasyuiPagination<DataDict>(dataDicts);
	}
	
	@ResponseBody
	@RequestMapping("/dataDicts")
	public List<DataDict> dataDicts(DataDict dataDict) {
		if (null != dataDict && null != dataDict.getId()) {
			dataDict.setParentId(dataDict.getId());
			dataDict.setId(null);
		}
		return this.dataDictService.list(dataDict);
	}
	
	@ResponseBody
	@RequestMapping("/dataDictsByCode")
	public List<DataDict> dataDictsByCode(String code) {
		return this.dataDictService.findChildren(code);
	}
	
	@RequestMapping(value = "/add")
	public ModelAndView add(HttpSession session, Long parentId) {
		ModelAndView mv = new ModelAndView("system/dataDict/dataDict_edit");
		DataDict dataDict = new DataDict();
		dataDict.setParentId(parentId);
		mv.getModelMap().put("dataDict", dataDict);
		return mv;
	}
	
	@ResponseBody
	@RequestMapping("/save")
	public Result save(DataDict newDataDict) {
		Result rs = this.dataDictService.isUnique(newDataDict);
		if (rs.isSuccess()) {
			this.dataDictService.saveOrUpdate(newDataDict);
			rs.setMessage("保存成功");
		}
		return rs;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(Long id) {
		ModelAndView mv = new ModelAndView("system/dataDict/dataDict_edit");
		if (null != id) {
			DataDict dataDict = this.dataDictService.get(id);
			mv.getModelMap().put("dataDict", dataDict);
		}
		return mv;
	}
	
	@ResponseBody
	@RequestMapping("/delete")
	public Result delete(@RequestParam(value="id")Long id) {
		try {
			this.dataDictService.del(id);
			return new Result(true, "删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败");
		}
	}
	
}
