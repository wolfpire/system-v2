/**
 * 
 */
package com.wolfpire.system.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.wolfpire.system.common.AuthorityUtils;
import com.wolfpire.system.common.EasyuiPage;
import com.wolfpire.system.common.EasyuiPagination;
import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.Result;
import com.wolfpire.system.common.springmvc.PrincipalArgument;
import com.wolfpire.system.model.Authority;
import com.wolfpire.system.model.Role;
import com.wolfpire.system.model.User;
import com.wolfpire.system.service.RoleService;

/**
 * @author lihd
 *
 */

@Controller
@RequestMapping("/role")
public class RoleController {
	
	@Resource private RoleService roleService;
	
	@RequestMapping("/list")
	public ModelAndView list(@PrincipalArgument User user, @RequestParam Long menuId) {
		//获取对应菜单的操作权限
//		List<Authority> operAuthorities = getAperAuthorities(user, menuId);
		List<Authority> operAuthorities = AuthorityUtils.getAperAuthorities(user, menuId);
		return new ModelAndView("system/role/role_list", "operAuthorities", operAuthorities);
	}
	
	@ResponseBody
	@RequestMapping("/queryList")
	public EasyuiPagination<Role> queryList(EasyuiPage<Role> page, Role filterRole) {
		Page<Role> roles = this.roleService.setPageDataList(page, filterRole);
		return new EasyuiPagination<Role>(roles);
	}
	
	@RequestMapping(value = "/add")
	public ModelAndView add(HttpSession session) {
		ModelAndView mv = new ModelAndView("system/role/role_add");
		return mv;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(Long id) {
		ModelAndView mv = new ModelAndView("system/role/role_edit");
		if (null != id) {
			Role role = this.roleService.getAuthorityInfoRole(id); 
					//this.roleService.get(id);
			mv.getModelMap().put("role", role);
		}
		return mv;
	}
	
	@ResponseBody
	@RequestMapping("/delete")
	public Result delete(Long id) {
		try {
			this.roleService.delete(id);
			return new Result(true, "删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败");
		}
	}

	@ResponseBody
	@RequestMapping("/save")
	public Map<String, Object> save(Role newRole) {
		Map<String, Object> map = new HashMap<String, Object>();
		boolean result = false;
		String msg = null;
		try {
			this.roleService.saveOrUpdate(newRole);
			result = true;
			msg = "保存成功";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败，请联系管理员";
		}
		map.put("result", result);
		map.put("msg", msg);
		return map;
	}
	
	/*@ResponseBody
	@RequestMapping("/isUnique")
	public Result isUnique(Role role) {
		try {
			boolean isUnique = this.roleService.isUnique(role);
			if (isUnique) {
				return new Result(true, "可以使用");
			}
			return new Result(false, "角色名字已经存在");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "角色名字已经存在");
		}
	}*/
	
	@ResponseBody
	@RequestMapping("/isUnique")
	public Boolean isUnique(Role role) {
		try {
			boolean isUnique = this.roleService.isUnique(role);
			return isUnique;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@ResponseBody
	@RequestMapping("/roles")
	public List<Role> roles(Role role) {
		List<Role> roles = this.roleService.list(role);
		return roles;
	}
	
	@RequestMapping("/roleAuthority")
	public ModelAndView roleAuthority(Long id) {
		ModelAndView mv = new ModelAndView("system/role/role_authority");
		if (null != id) {
			Role role = this.roleService.getAuthorityInfoRole(id); 
					//this.roleService.get(id);
			mv.getModelMap().put("role", role);
		}
		return mv;
	}
	
	@ResponseBody
	@RequestMapping("/saveRoleAuthority")
	public Result saveRoleAuthority(
			@RequestParam(value = "id") Long id,
			@RequestParam(value = "authorityIds", required = false) String authorityIds) {
		try {
			System.out.println(authorityIds);
			this.roleService.saveRoleAuthority(id, authorityIds);
			return Result.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return Result.FAILURE;
		}
	}
	
	@RequestMapping("/roleUser")
	public ModelAndView roleUser(Long id) {
		ModelAndView mv = new ModelAndView("system/role/role_user");
		if (null != id) {
			Role role = this.roleService.get(id); 
					//this.roleService.getAuthorityInfoRole(id); 
					//this.roleService.get(id);
			String userIdsStr = this.roleService.getUserIdsByRoleId(id);
			mv.getModelMap().put("role", role);
			mv.getModelMap().put("userIdsStr", userIdsStr);
		}
		return mv;
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveRoleUser", method = {RequestMethod.POST, RequestMethod.GET})
	public Result saveRoleUser(@RequestParam(value = "id") Long id,
			@RequestParam(value = "userIds", required = false) String userIds) {
		try {
			System.out.println(userIds);
			this.roleService.saveRoleUser(id, userIds);
			return Result.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return Result.FAILURE;
		}
	}
}
