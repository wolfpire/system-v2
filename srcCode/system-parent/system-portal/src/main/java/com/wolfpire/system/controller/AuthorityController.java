/**
 * 
 */
package com.wolfpire.system.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.wolfpire.system.common.AuthorityUtils;
import com.wolfpire.system.common.EasyuiPage;
import com.wolfpire.system.common.EasyuiPagination;
import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.Result;
import com.wolfpire.system.common.springmvc.PrincipalArgument;
import com.wolfpire.system.model.Authority;
import com.wolfpire.system.model.User;
import com.wolfpire.system.service.AuthorityService;

/**
 * 组织模块控制器
 * @author lihd
 *
 */

@Controller
@RequestMapping("/authority")
public class AuthorityController {
	
	@Resource private AuthorityService authorityService;
	
	@RequestMapping("/list")
	public ModelAndView list(@PrincipalArgument User user, @RequestParam Long menuId) {
		//获取对应菜单的操作权限
//		List<Authority> operAuthorities = getAperAuthorities(user, menuId);
		List<Authority> operAuthorities = AuthorityUtils.getAperAuthorities(user, menuId);
		return new ModelAndView("system/authority/authority_list", "operAuthorities", operAuthorities);
	}
	
	@ResponseBody
	@RequestMapping("/queryList")
	public EasyuiPagination<Authority> queryList(EasyuiPage<Authority> page, Authority authority) {
		if (null != authority && null != authority.getId()) {
			authority.setParentId(authority.getId());
			authority.setId(null);
		}
//		page.setRows(99999999);
		Page<Authority> authoritys = this.authorityService.setPageDataList(page, authority);
		return new EasyuiPagination<Authority>(authoritys);
	}
	
	@RequestMapping(value = "/add")
	public ModelAndView add(HttpSession session, Authority authority) {
		ModelAndView mv = new ModelAndView("system/authority/authority_add");
		mv.addObject(authority);
		return mv;
	}
	
	@ResponseBody
	@RequestMapping("/save")
	public Result save(Authority authority) {
		try {
			this.authorityService.saveOrUpdate(authority);
			return new Result(true, "保存成功！");
		} catch (Exception e) {
			return new Result(false, "保存失败");
		}
	}
	
	@ResponseBody
	@RequestMapping("/isUnique")
	public Boolean isUnique(Authority authority) {
		try {
			return this.authorityService.isUnique(authority);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(Long id) {
		ModelAndView mv = new ModelAndView("system/authority/authority_edit");
		if (null != id) {
			Authority authority = this.authorityService.get(id);
			mv.getModelMap().put("authority", authority);
		}
		return mv;
	}
	
	@ResponseBody
	@RequestMapping("/delete")
	public Result delete(@RequestParam(value="id")Long id) {
		try {
			this.authorityService.del(id);
			return new Result(true, "删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败");
		}
	}
	
	@ResponseBody
	@RequestMapping("/authoritys")
	public List<Authority> authoritys(Authority authority) {
		if (null != authority && null != authority.getId()) {
			authority.setParentId(authority.getId());
			authority.setId(null);
		}
		return this.authorityService.list(authority);
	}
	
	@ResponseBody
	@RequestMapping("/authoritysTree")
	public List<Authority> authoritysTree(Authority authority) {
		return this.authorityService.listTree(authority);
	}
	
}
