package com.wolfpire.system.common.springmvc;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.wolfpire.system.common.Constants;



/**
 * 
 * 
 * @author mengyang
 *
 */
public class PrincipalArgumentResolver implements
		HandlerMethodArgumentResolver {

	public boolean supportsParameter(MethodParameter parameter) {

		return parameter.getParameterAnnotation(PrincipalArgument.class) != null;
	}

	@SuppressWarnings("unused")
	public Object resolveArgument(MethodParameter parameter,
			ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
			WebDataBinderFactory binderFactory) throws Exception {
		
		PrincipalArgument param = parameter.getParameterAnnotation(PrincipalArgument.class);
		return webRequest.getAttribute(Constants.SESSION_USER, WebRequest.SCOPE_SESSION);
	}

}
