/**
 * 
 */
package com.wolfpire.system.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * 请求拦截器
 * @author lihd
 *
 */
public class SystemHandlerInterceptor implements HandlerInterceptor {
	
	protected final Log logger = LogFactory.getLog(getClass());

	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.HandlerInterceptor#preHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object)
	 */
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		logger.debug("\r\n----------->Pre-handle");
		long startTime = System.currentTimeMillis();  
        request.setAttribute("startTime", startTime);
        return true;
	}

	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.HandlerInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)
	 */
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		long executeTime = 0L; 
        if (null != request.getAttribute("startTime")) {
        	long startTime = (Long)request.getAttribute("startTime");
        	long endTime = System.currentTimeMillis();
        	executeTime = endTime - startTime;
		}
        if(logger.isWarnEnabled()) {
            logger.warn("\r\n----------->request.getServerName():"+request.getServerName());
            logger.warn("\r\n----------->request.getPathInfo():"+request.getPathInfo());
            logger.warn("\r\n----------->request.getContextPath():"+request.getContextPath());
            logger.warn("\r\n----------->request.getRequestURI():"+request.getRequestURI());
            logger.warn("\r\n----------->request.getRequestURL():"+request.getRequestURL());
            logger.warn("\r\n----------->view:"+ ((modelAndView == null) ? "null" : modelAndView.getViewName()));
            logger.warn("\r\n----------->postTime:"+executeTime);
        }
        if(modelAndView == null) return;
	}

	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.HandlerInterceptor#afterCompletion(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, java.lang.Exception)
	 */
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		logger.debug("\r\n----------->After completion handle");
		long executeTime = 0L; 
        if (null != request.getAttribute("startTime")) {
        	long startTime = (Long)request.getAttribute("startTime");
        	long endTime = System.currentTimeMillis();
        	executeTime = endTime - startTime;
		}
        if(logger.isWarnEnabled()) {
            logger.warn("\r\n----------->completTime:"+executeTime);
        }
	}

}
