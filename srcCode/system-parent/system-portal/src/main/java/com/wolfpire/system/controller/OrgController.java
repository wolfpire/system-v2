/**
 * 
 */
package com.wolfpire.system.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.wolfpire.system.common.AuthorityUtils;
import com.wolfpire.system.common.EasyuiPage;
import com.wolfpire.system.common.EasyuiPagination;
import com.wolfpire.system.common.Page;
import com.wolfpire.system.common.Result;
import com.wolfpire.system.common.springmvc.PrincipalArgument;
import com.wolfpire.system.model.Authority;
import com.wolfpire.system.model.Org;
import com.wolfpire.system.model.User;
import com.wolfpire.system.service.OrgService;

/**
 * 组织模块控制器
 * @author lihd
 *
 */

@Controller
@RequestMapping("/org")
public class OrgController {
	
	@Resource private OrgService orgService;
	
	@RequestMapping("/list")
	public ModelAndView list(@PrincipalArgument User user, @RequestParam Long menuId) {
		//获取对应菜单的操作权限
//		List<Authority> operAuthorities = getAperAuthorities(user, menuId);
		List<Authority> operAuthorities = AuthorityUtils.getAperAuthorities(user, menuId);
		return new ModelAndView("system/org/org_list", "operAuthorities", operAuthorities);
	}
	
	@ResponseBody
	@RequestMapping("/queryList")
	public EasyuiPagination<Org> queryList(EasyuiPage<Org> page, Org org) {
		if (null != org && null != org.getId()) {
			org.setParentId(org.getId());
			org.setId(null);
		}
		Page<Org> orgs = this.orgService.setPageDataList(page, org);
		return new EasyuiPagination<Org>(orgs);
	}
	
	@ResponseBody
	@RequestMapping("/queryChildren")
	public List<Org> queryChildren(Org org) {
		if (null != org && null != org.getId()) {
			org.setParentId(org.getId());
			org.setId(null);
		}
		List<Org> orgs = this.orgService.list(org);
		return orgs;
	}
	
	@ResponseBody
	@RequestMapping("/orgs")
	public List<Org> orgs(Org org) {
		if (null != org && null != org.getId()) {
			org.setParentId(org.getId());
			org.setId(null);
		}
		return this.orgService.list(org);
	}
	
	@RequestMapping(value = "/add")
	public ModelAndView add(HttpSession session, Long parentId) {
		ModelAndView mv = new ModelAndView("system/org/org_add");
		mv.getModelMap().put("parentId", parentId);
		return mv;
	}
	
	@ResponseBody
	@RequestMapping("/save")
	public Result save(Org newOrg) {
		Result rs = this.orgService.isUnique(newOrg);
		if (rs.isSuccess()) {
			this.orgService.saveOrUpdate(newOrg);
			rs.setMessage("保存成功");
		}
		return rs;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(Long id) {
		ModelAndView mv = new ModelAndView("system/org/org_edit");
		if (null != id) {
			Org org = this.orgService.get(id);
			mv.getModelMap().put("org", org);
		}
		return mv;
	}
	
	@ResponseBody
	@RequestMapping("/delete")
	public Result delete(@RequestParam(value="id")Long id) {
		try {
			this.orgService.del(id);
			return new Result(true, "删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败");
		}
	}
}
