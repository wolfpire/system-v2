/**
 * 
 */
package com.wolfpire.system.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.wolfpire.system.common.Constants;
import com.wolfpire.system.exception.LoginException;
import com.wolfpire.system.model.User;

/**
 * 登录拦截器
 * @author lihd
 *
 */
public class LoginInterceptor implements HandlerInterceptor {

	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.HandlerInterceptor#preHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object)
	 */
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		HttpSession session = request.getSession(true);  
        // 从session 里面获取用户名的信息  
        Object obj = session.getAttribute(Constants.SESSION_USER);  
        // 判断如果没有取到用户信息，就跳转到登陆页面，提示用户进行登陆 
        try {
			User user = (User) obj;
			if (null != user.getId()) {
				return true;
			}
			return false;
		} catch (Exception e) {
//			e.printStackTrace();
			System.out.println("====================>>>>>怀疑是非法入侵，请正常登陆操作...");
			throw new LoginException();
		}
	}

	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.HandlerInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)
	 */
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.HandlerInterceptor#afterCompletion(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, java.lang.Exception)
	 */
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub

	}

}
