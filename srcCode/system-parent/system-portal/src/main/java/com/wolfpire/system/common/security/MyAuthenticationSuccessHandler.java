/**
 * 
 */
package com.wolfpire.system.common.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wolfpire.system.common.AddressUtils;
import com.wolfpire.system.common.Constants;
import com.wolfpire.system.common.Result;
import com.wolfpire.system.model.User;

/**
 * @author lihd
 *
 */
public class MyAuthenticationSuccessHandler implements
		AuthenticationSuccessHandler {
	
	/**
	 * 认证成功后的跳转地址
	 */
	private String url;
	
	@SuppressWarnings("unused")
	private final AddressUtils addressUtils = new AddressUtils();
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication auth) throws IOException,
			ServletException {
		// TODO Auto-generated method stub
		User user = (User)auth.getPrincipal();
		request.getSession().setAttribute(Constants.SESSION_USER, user);
		Result result = new Result(true, null, url);
		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("application/json;charset=utf-8");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		
		mapper.writeValue(response.getWriter(), result);
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
