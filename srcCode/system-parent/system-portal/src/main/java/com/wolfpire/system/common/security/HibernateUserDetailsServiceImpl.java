/**
 * 
 */
package com.wolfpire.system.common.security;

import javax.annotation.Resource;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.wolfpire.system.model.User;
import com.wolfpire.system.service.UserService;

/**
 * @author lihd
 *
 */
public class HibernateUserDetailsServiceImpl implements UserDetailsService {
	
	@Resource private UserService userService;
	
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		User user = this.userService.getByAccount(username);
		if (null != user) {
			user = this.userService.getCompleteUser(user.getId());
			return user;
		} else {
			throw new UsernameNotFoundException("没找到账号：'" + username + "'对应的用户信息。");
		}
	}

}
