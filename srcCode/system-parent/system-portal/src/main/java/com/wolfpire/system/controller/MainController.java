package com.wolfpire.system.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.wolfpire.system.common.Constants;
import com.wolfpire.system.common.springmvc.PrincipalArgument;
import com.wolfpire.system.model.Authority;
import com.wolfpire.system.model.User;
import com.wolfpire.system.service.AuthorityService;

@Controller
@RequestMapping("/system")
public class MainController {
	
	@Resource private AuthorityService authorityService;
	
	@RequestMapping("/top")
	public String top(@ModelAttribute(Constants.SESSION_USER) User user) {
		return "system/top";
	}
	
	@RequestMapping("/left")
	public String left(@ModelAttribute(Constants.SESSION_USER) User user) {
		return "system/left";
	}
	
	@RequestMapping("/index")
	public ModelAndView index(@PrincipalArgument User user) {
//		System.out.println(ToStringBuilder.reflectionToString(user));
		ModelAndView mv = new ModelAndView("system/index");
		// 构建菜单导航栏所需的权限菜单
		List<Authority> authorities = this.authorityService.buildMenuTree(user.getUserAuthorities());
		mv.getModel().put("authorities", authorities);
		return mv;
	}
	
}
